/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.settings;

import java.io.Serializable;

/**
 *
 * @author abhishekingle
 */
public class OOBEmailChannelSettings implements Serializable{
    
    private static final long serialVersionUID = 999991307498275974L;

    public OOBEmailChannelSettings(String smtpIp, int port, String userId, String password, boolean ssl, boolean authRequired, String className, int preference, String fromEmail, String fromName, Object reserve1, Object reserve2, Object reserve3, int autofailover, int retrycount, int status, String implementationJAR, int retryduration) {
        this.smtpIp = smtpIp;
        this.port = port;
        this.userId = userId;
        this.password = password;
        this.ssl = ssl;
        this.authRequired = authRequired;
        this.className = className;
        this.preference = preference;
        this.fromEmail = fromEmail;
        this.fromName = fromName;
        this.reserve1 = reserve1;
        this.reserve2 = reserve2;
        this.reserve3 = reserve3;
        this.autofailover = autofailover;
        this.retrycount = retrycount;        
        this.status = status;
        this.implementationJAR = implementationJAR;
        this.retryduration = retryduration;
    }

    public OOBEmailChannelSettings() {
    }

    @Override
    public String toString() {
        return "OOBEmailChannelSettings{" + "smtpIp=" + smtpIp + ", port=" + port + ", userId=" + userId + ", password=" + password + ", ssl=" + ssl + ", authRequired=" + authRequired + ", className=" + className + ", preference=" + preference + ", PRIMARY=" + PRIMARY + ", SECONDARY=" + SECONDARY + ", fromEmail=" + fromEmail + ", fromName=" + fromName + ", reserve1=" + reserve1 + ", reserve2=" + reserve2 + ", reserve3=" + reserve3 + ", status=" + status + ", STATUS_ACTIVE=" + STATUS_ACTIVE + ", STATUS_INACTIVE=" + STATUS_INACTIVE + ", implementationJAR=" + implementationJAR + ", retryduration=" + retryduration + '}';
    }

    public String getSmtpIp() {
        return smtpIp;
    }

    public void setSmtpIp(String smtpIp) {
        this.smtpIp = smtpIp;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isSsl() {
        return ssl;
    }

    public void setSsl(boolean ssl) {
        this.ssl = ssl;
    }

    public boolean isAuthRequired() {
        return authRequired;
    }

    public void setAuthRequired(boolean authRequired) {
        this.authRequired = authRequired;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public int getPreference() {
        return preference;
    }

    public void setPreference(int preference) {
        this.preference = preference;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public Object getReserve1() {
        return reserve1;
    }

    public void setReserve1(Object reserve1) {
        this.reserve1 = reserve1;
    }

    public Object getReserve2() {
        return reserve2;
    }

    public void setReserve2(Object reserve2) {
        this.reserve2 = reserve2;
    }

    public Object getReserve3() {
        return reserve3;
    }

    public void setReserve3(Object reserve3) {
        this.reserve3 = reserve3;
    }    

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getImplementationJAR() {
        return implementationJAR;
    }

    public void setImplementationJAR(String implementationJAR) {
        this.implementationJAR = implementationJAR;
    }

    String smtpIp;

    private int port;

    private String userId;

    private String password;

    private boolean ssl;

    private boolean authRequired;

    private String className;

    int preference;

    final int PRIMARY = 1;

    final int SECONDARY = 2;

    private String fromEmail;

    private String fromName;

    private Object reserve1;

    private Object reserve2;

    private Object reserve3;

    int autofailover;

    public int getAutofailover() {
        return autofailover;
    }

    public void setAutofailover(int autofailover) {
        this.autofailover = autofailover;
    }

    public int getRetrycount() {
        return retrycount;
    }

    public void setRetrycount(int retrycount) {
        this.retrycount = retrycount;
    }

    public int getRetryduration() {
        return retryduration;
    }

    public void setRetryduration(int retryduration) {
        this.retryduration = retryduration;
    }

    int retrycount;
    
    int status;

    int retryduration;

    final int STATUS_ACTIVE = 0;

    final int STATUS_INACTIVE = 1;

    private String implementationJAR;
}
