/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.settings;

import java.io.Serializable;

/**
 *
 * @author abhishekingle
 */
public class OOBMobileChannelSettings implements Serializable{
    
    private static final long serialVersionUID = 999992307498275974L;

    public OOBMobileChannelSettings(int type, String ip, int port, String userid, String password, String className, int preference, boolean logConfirmation, String phoneNumber, Object reserve1, Object reserve2, Object reserve3, int status, int autofailover, int retrycount, int retryduration, int messageLength) {
        this.type = type;
        this.ip = ip;
        this.port = port;
        this.userid = userid;
        this.password = password;
        this.className = className;
        this.preference = preference;
        this.logConfirmation = logConfirmation;
        this.phoneNumber = phoneNumber;
        this.reserve1 = reserve1;
        this.reserve2 = reserve2;
        this.reserve3 = reserve3;
        this.status = status;
        this.autofailover = autofailover;
        this.retrycount = retrycount;
        this.retryduration = retryduration;
        this.messageLength = messageLength;
    }

    public OOBMobileChannelSettings() {
    }

    @Override
    public String toString() {
        return "OOBMobileChannelSettings{" + "type=" + type + ", SMS=" + SMS + ", USSD=" + USSD + ", VOICE=" + VOICE + ", ip=" + ip + ", port=" + port + ", userid=" + userid + ", password=" + password + ", className=" + className + ", preference=" + preference + ", PRIMARY=" + PRIMARY + ", SECONDARY=" + SECONDARY + ", logConfirmation=" + logConfirmation + ", phoneNumber=" + phoneNumber + ", reserve1=" + reserve1 + ", reserve2=" + reserve2 + ", reserve3=" + reserve3  + ", status=" + status + ", STATUS_ACTIVE=" + STATUS_ACTIVE + ", STATUS_INACTIVE=" + STATUS_INACTIVE + "" + +'}';
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public int getPreference() {
        return preference;
    }

    public void setPreference(int preference) {
        this.preference = preference;
    }

    public boolean isLogConfirmation() {
        return logConfirmation;
    }

    public void setLogConfirmation(boolean logConfirmation) {
        this.logConfirmation = logConfirmation;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Object getReserve1() {
        return reserve1;
    }

    public void setReserve1(Object reserve1) {
        this.reserve1 = reserve1;
    }

    public Object getReserve2() {
        return reserve2;
    }

    public void setReserve2(Object reserve2) {
        this.reserve2 = reserve2;
    }

    public Object getReserve3() {
        return reserve3;
    }

    public void setReserve3(Object reserve3) {
        this.reserve3 = reserve3;
    }
   
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getAutofailover() {
        return autofailover;
    }

    public void setAutofailover(int autofailover) {
        this.autofailover = autofailover;
    }

    public int getRetrycount() {
        return retrycount;
    }

    public void setRetrycount(int retrycount) {
        this.retrycount = retrycount;
    }

    public int getRetryduration() {
        return retryduration;
    }

    public void setRetryduration(int retryduration) {
        this.retryduration = retryduration;
    }

    public boolean isEnable2Way() {
        return enable2Way;
    }

    public void setEnable2Way(boolean enable2WaySMS) {
        this.enable2Way = enable2WaySMS;
    }

    public String getTwoWayPhoneNumber() {
        return twoWayPhoneNumber;
    }

    public void setTwoWayPhoneNumber(String twoWayPhoneNumber) {
        this.twoWayPhoneNumber = twoWayPhoneNumber;
    }

    public String getTwoWayServerUrl() {
        return twoWayServerUrl;
    }

    public void setTwoWayServerUrl(String twoWayServerUrl) {
        this.twoWayServerUrl = twoWayServerUrl;
    }

    public String getTwoWayuserid() {
        return twoWayuserid;
    }

    public void setTwoWayuserid(String twoWayuserid) {
        this.twoWayuserid = twoWayuserid;
    }

    public String getTwoWayPassword() {
        return twoWayPassword;
    }

    public void setTwoWayPassword(String twoWayPassword) {
        this.twoWayPassword = twoWayPassword;
    }

    public void setMessageLength(int messageLength) {
        this.messageLength = messageLength;
    }

    public int getMessageLength() {
        return messageLength;
    }

    final int SMS = 1;

    final int USSD = 2;

    final int VOICE = 3;

    final int PRIMARY = 1;

    final int SECONDARY = 2;

    final int STATUS_ACTIVE = 0;

    final int STATUS_INACTIVE = 1;

    int type;

    String ip;

    int port;

    String userid;

    String password;

    String className;

    int preference;

    boolean logConfirmation;

    String phoneNumber;

    private Object reserve1;

    private Object reserve2;

    private Object reserve3;   

    int status;

    int autofailover;

    int retrycount;

    int retryduration;

    int messageLength;

    boolean enable2Way;

    String twoWayPhoneNumber;

    String twoWayServerUrl;

    String twoWayuserid;

    String twoWayPassword;

    int twowaySessionExpiryInMins;
}
