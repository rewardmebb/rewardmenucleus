/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.settings;

import com.mollatech.rewardme.connector.communication.EmailConnectorSettings;
import com.mollatech.rewardme.connector.communication.MobileConnectorSettings;
import com.mollatech.rewardme.connector.communication.RMStatus;
import com.mollatech.rewardme.nucleus.db.RmChannellogs;
import com.mollatech.rewardme.nucleus.db.connector.ChannelLogsUtils;
import com.mollatech.rewardme.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.rewardme.nucleus.db.connector.SettingsUtil;
import com.mollatech.rewardme.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.security.MessageDigest;
import java.util.Date;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author abhishekingle
 */
public class SendNotification {
    
    static final Logger logger = Logger.getLogger(SendNotification.class);

    public static final int SMS = 1;

    public static final int USSD = 2;

    public static final int VOICE = 3;

    public static final int EMAIL = 4;

    public static final int FAX = 5;

    public static final int FACEBOOK = 6;

    public static final int LINKEDIN = 7;

    public static final int TWITTER = 8;

    public static final int ANDROIDPUSH = 18;

    public static final int IPHONEPUSH = 19;

    public static final int ACTIVE = 1;

    public static final int INACTIVE = 0;

    public static final int FAILED = -1;

    public static final int PENDING = 2;

    public static final int SENT = 0;

    public static final String strPENDING = "Message delivery is pending";

    public static final String strFAILED = "Message delivery was failed";

    public RMStatus SendOnMobileByPreference(String number, String message, int type, int preference, int sourceId) {
        SessionFactoryUtil suSettings = new SessionFactoryUtil(SessionFactoryUtil.settings);
        Session sSettings = suSettings.openSession();
        SettingsUtil sUtil = new SettingsUtil(suSettings, sSettings);               
        RMStatus status = new RMStatus();
        String strType = "";
        status.iStatus = -8;
        int retrycount = 0;
        if (type == SMS) {
            status.strStatus = "SMS Server Configuration missing";
            strType = "SMS";
        }
        SettingsManagement sManagement = new SettingsManagement();
        RMStatus notificationAlert = new RMStatus();
        notificationAlert.iStatus = 0;
        notificationAlert.strStatus = "completed";
        Object objec = sManagement.getSettingInner(SettingsManagement.GlobalSettings, SettingsManagement.PREFERENCE_ONE);
        String sid = null;
        Class c;
        SessionFactoryUtil suChannelLogs = new SessionFactoryUtil(SessionFactoryUtil.channellogs);
        Session sChannelLogs = suChannelLogs.openSession();
        ChannelLogsUtils cUtils = new ChannelLogsUtils(suChannelLogs, sChannelLogs);
        try {
            Constructor argsConstructor;
            Method[] allMethods = null;
            Object object = null;
            Object obj = sUtil.getSetting(type, preference);
            if (obj != null) {
                if (obj instanceof OOBMobileChannelSettings) {
                    OOBMobileChannelSettings settings = (OOBMobileChannelSettings) obj;
                    if (settings.status == ACTIVE) {
                        MobileConnectorSettings mobile = new MobileConnectorSettings();
                        mobile.ip = settings.getIp();
                        mobile.className = settings.getClassName();
                        mobile.password = settings.getPassword();
                        mobile.phoneNumber = settings.getPhoneNumber();
                        mobile.port = settings.getPort();
                        mobile.reserve1 = settings.getReserve1();
                        mobile.reserve2 = settings.getReserve2();
                        mobile.reserve3 = settings.getReserve3();
                        mobile.userid = settings.getUserid();
                        mobile.type = settings.getType();
                        try {
                            c = Class.forName(settings.getClassName());
                            argsConstructor = c.getConstructor();
                            object = argsConstructor.newInstance();
                            allMethods = c.getDeclaredMethods();
                        } catch (Exception e) {
                            logger.error("Exception at SendNotification ", e);
                        }
                        for (Method m : allMethods) {
                            String mname = m.getName();
                            if (mname.compareTo("Load") == 0) {
                                m.setAccessible(true);
                                try {
                                    m.invoke(object, mobile);
                                } catch (Exception ex) {
                                    logger.error("Exception at SendNotification ", ex);
                                }
                            }
                        }
                        mainLable:
                        for (int j = settings.getRetrycount(); j >= 0; --j) {
                            retrycount++;
                            for (Method m : allMethods) {
                                String mname = m.getName();
                                if (mname.compareTo("SendMessage") == 0) {
                                    m.setAccessible(true);
                                    try {
                                        sid = (String) m.invoke(object, number, message);
                                    } catch (Exception ex) {
                                        logger.error("Exception at SendNotification ", ex);
                                    }
                                }
                            }
                            if (sid != null) {
                                for (Method m : allMethods) {
                                    String mname = m.getName();
                                    if (mname.compareTo("GetStatus") == 0) {
                                        m.setAccessible(true);
                                        try {
                                            Thread.sleep(15000);
                                            status = (RMStatus) m.invoke(object, sid);
                                            if (status != null) {
                                                if (status.strStatus.compareTo("ringing") == 0) {
                                                    Thread.sleep(15000);
                                                    status = (RMStatus) m.invoke(object, sid);
                                                }
                                            }
                                        } catch (Exception ex) {
                                            logger.error("Exception at SendNotification ", ex);
                                        }
                                    }
                                }
                            } else if (sid == null) {
                                status = new RMStatus();
                                status.iStatus = -5;
                                status.strStatus = "Failed";
                            }
                            if (status != null) {
                                if (status.iStatus == 0 && status.strStatus.equals("completed")) {
                                    RmChannellogs cLogs = new RmChannellogs();
                                    //cLogs.setChannelid(channelId);
                                    cLogs.setType(SMS);
                                    cLogs.setPreference(settings.getPreference());
                                    cLogs.setMsgid(sid);
                                    cLogs.setPhone(number);
                                    cLogs.setMessage(message);
                                    cLogs.setSentUtctime(new Date());
                                    cLogs.setErrorstatus(status.strStatus);
                                    cLogs.setSourceid(sourceId);
                                    cLogs.setStatus(status.iStatus);
                                    cLogs.setRetrycount(retrycount);
                                    cLogs.setLastupdateUtctime(new Date());
                                    cUtils.addChannellogs(settings.getPreference(), SMS, cLogs);
                                    break mainLable;
                                } else if (j == 0) {
                                    if (settings.autofailover == ACTIVE) {
                                        continue;
                                    } else {
                                        break mainLable;
                                    }
                                }
                                RmChannellogs cLogs = new RmChannellogs();                               
                                cLogs.setType(SMS);
                                cLogs.setPreference(settings.getPreference());
                                cLogs.setMsgid(sid);
                                cLogs.setPhone(number);
                                cLogs.setMessage(message);
                                cLogs.setSentUtctime(new Date());
                                cLogs.setErrorstatus(status.strStatus);
                                cLogs.setSourceid(sourceId);
                                cLogs.setStatus(status.iStatus);
                                cLogs.setRetrycount(retrycount);
                                cLogs.setLastupdateUtctime(new Date());
                                cUtils.addChannellogs(settings.getPreference(), SMS, cLogs);
                            } else {
                                RmChannellogs cLogs = new RmChannellogs();                                
                                cLogs.setType(SMS);
                                cLogs.setPreference(settings.getPreference());
                                cLogs.setMsgid(sid);
                                cLogs.setPhone(number);
                                cLogs.setMessage(message);
                                cLogs.setSentUtctime(new Date());
                                cLogs.setErrorstatus(status.strStatus);
                                cLogs.setSourceid(sourceId);
                                cLogs.setStatus(status.iStatus);
                                cLogs.setRetrycount(retrycount);
                                cLogs.setLastupdateUtctime(new Date());
                                cUtils.addChannellogs(settings.getPreference(), SMS, cLogs);
                            }
                            if (settings.autofailover == ACTIVE) {
                                continue;
                            } else {
                                break mainLable;
                            }
                        }
                    }
                }
            }
            return status;
        } catch (Exception ex) {
            logger.error("Exception at SendNotification ", ex);
            return status;
        } finally {
            sSettings.close();
            suSettings.close();
        }
    }

    public RMStatus SendEmail(String emailid, String subject, String message, String[] cc, String[] bcc, String[] files, String[] mimetypes, int sourceId) {
        SessionFactoryUtil suSettings = new SessionFactoryUtil(SessionFactoryUtil.settings);
        Session sSettings = suSettings.openSession();
        SettingsUtil sUtil = new SettingsUtil(suSettings, sSettings);        
        RMStatus notificationAlert = new RMStatus();
        notificationAlert.iStatus = 0;
        notificationAlert.strStatus = "completed";
        RMStatus status = new RMStatus();
        status.iStatus = -8;
        int retrycount = 0;
        status.strStatus = "Email Server Configuration missing";
        String sid = null;
        Class c;
        SessionFactoryUtil suChannelLogs = new SessionFactoryUtil(SessionFactoryUtil.channellogs);
        Session sChannelLogs = suChannelLogs.openSession();
        ChannelLogsUtils cUtils = new ChannelLogsUtils(suChannelLogs, sChannelLogs);
        try {
            Constructor argsConstructor;
            Method[] allMethods = null;
            Object object = null;
            Object[] obj = sUtil.getSettingsByTypes(EMAIL);
            if (obj != null) {
                mainLable:
                for (int i = 0; i < obj.length; i++) {
                    if (obj[i] instanceof OOBEmailChannelSettings) {
                        OOBEmailChannelSettings settings = (OOBEmailChannelSettings) obj[i];
                        if (settings.status == ACTIVE) {
                            EmailConnectorSettings email = new EmailConnectorSettings();
                            email.smtpIp = settings.getSmtpIp();
                            email.password = settings.getPassword();
                            email.fromEmail = settings.getFromEmail();
                            email.fromName = settings.getFromName();
                            email.authRequired = settings.isAuthRequired();
                            email.ssl = settings.isSsl();
                            email.port = settings.getPort();
                            email.reserve1 = settings.getReserve1();
                            email.reserve2 = settings.getReserve2();
                            email.reserve3 = settings.getReserve3();
                            email.userId = settings.getUserId();
                            try {
                                c = Class.forName(settings.getClassName());
                                argsConstructor = c.getConstructor();
                                object = argsConstructor.newInstance();
                                allMethods = c.getDeclaredMethods();
                            } catch (Exception e) {
                                logger.error("Exception at SendNotification ", e);
                            }
                            for (Method m : allMethods) {
                                String mname = m.getName();
                                if (mname.compareTo("Load") == 0) {
                                    m.setAccessible(true);
                                    try {
                                        m.invoke(object, email);
                                    } catch (Exception ex) {
                                        logger.error("Exception at SendNotification ", ex);
                                    }
                                }
                            }
                            for (int j = settings.getRetrycount(); j >= 0; --j) {
                                retrycount++;
                                for (Method m : allMethods) {
                                    String mname = m.getName();
                                    if (mname.compareTo("SendMessage") == 0) {
                                        m.setAccessible(true);
                                        try {
                                            String strStress = null;
                                            try {
                                                sid = (String) m.invoke(object, emailid, subject, message, cc, bcc, files, mimetypes);
                                            } catch (Exception ex) {
                                                logger.error("Exception at SendNotification ", ex);
                                            }
                                        } catch (Exception ex) {
                                            logger.error("Exception at SendNotification ", ex);
                                        }
                                    }
                                }
                                if (sid != null) {
                                    for (Method m : allMethods) {
                                        String mname = m.getName();
                                        if (mname.compareTo("GetStatus") == 0) {
                                            m.setAccessible(true);
                                            try {
                                                Thread.sleep(10);
                                                String strStress = null;
                                                try {
                                                    strStress = LoadSettings.g_sSettings.getProperty("reserved.6");
                                                    if (strStress != null) {
                                                        if (strStress.compareToIgnoreCase("yes") == 0) {
                                                            RMStatus status1 = new RMStatus();
                                                            status1.iStatus = 0;
                                                            status1.strStatus = "completed";
                                                            status = status1;
                                                        } else {
                                                            status = (RMStatus) m.invoke(object, sid);
                                                        }
                                                    } else {
                                                        status = (RMStatus) m.invoke(object, sid);
                                                    }
                                                } catch (Exception ex) {
                                                    logger.error("Exception at SendNotification ", ex);
                                                }
                                            } catch (Exception ex) {
                                                logger.error("Exception at SendNotification ", ex);
                                            }
                                        }
                                    }
                                } else if (sid == null) {
                                    status = new RMStatus();
                                    status.iStatus = -5;
                                    status.strStatus = "Failed";
                                }
                                if (status != null) {
                                    if (status.iStatus == 0 && status.strStatus.equals("completed")) {
                                        RmChannellogs cLogs = new RmChannellogs();                                        
                                        cLogs.setType(EMAIL);
                                        cLogs.setPreference(settings.getPreference());
                                        cLogs.setMsgid(sid);
                                        cLogs.setEmail(emailid);
                                        cLogs.setMessage(message);
                                        cLogs.setSentUtctime(new Date());
                                        cLogs.setErrorstatus(status.strStatus);
                                        cLogs.setSourceid(sourceId);
                                        cLogs.setStatus(status.iStatus);
                                        cLogs.setRetrycount(retrycount);
                                        cLogs.setLastupdateUtctime(new Date());
                                        cUtils.addChannellogs(settings.getPreference(), EMAIL, cLogs);
                                        break mainLable;
                                    } else if (j == 0) {
                                        if (settings.autofailover == ACTIVE) {
                                            continue;
                                        } else {
                                            break mainLable;
                                        }
                                    }
                                    RmChannellogs cLogs = new RmChannellogs();                                    
                                    cLogs.setType(EMAIL);
                                    cLogs.setPreference(settings.getPreference());
                                    cLogs.setMsgid(sid);
                                    cLogs.setEmail(emailid);
                                    cLogs.setMessage(message);
                                    cLogs.setSentUtctime(new Date());
                                    cLogs.setErrorstatus(status.strStatus);
                                    cLogs.setSourceid(sourceId);
                                    cLogs.setStatus(status.iStatus);
                                    cLogs.setRetrycount(retrycount);
                                    cLogs.setLastupdateUtctime(new Date());
                                    cUtils.addChannellogs(settings.getPreference(), EMAIL, cLogs);
                                } else {
                                    RmChannellogs cLogs = new RmChannellogs();                                    
                                    cLogs.setType(EMAIL);
                                    cLogs.setPreference(settings.getPreference());
                                    cLogs.setMsgid(sid);
                                    cLogs.setEmail(emailid);
                                    cLogs.setMessage(message);
                                    cLogs.setSentUtctime(new Date());
                                    cLogs.setErrorstatus(status.strStatus);
                                    cLogs.setSourceid(sourceId);
                                    cLogs.setStatus(status.iStatus);
                                    cLogs.setRetrycount(retrycount);
                                    cLogs.setLastupdateUtctime(new Date());
                                    cUtils.addChannellogs(settings.getPreference(), EMAIL, cLogs);
                                }
                            }
                            if (settings.autofailover == ACTIVE) {
                                continue;
                            } else {
                                break mainLable;
                            }
                        }
                    }
                }
            }
            return status;
        } finally {
            sSettings.close();
            suSettings.close();
        }
    }

    private byte[] SHA1(String message) {
        try {
            MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
            byte[] array = sha1.digest(message.getBytes());
            return array;
        } catch (Exception e) {
            logger.error("Exception at SendNotification ", e);
            return null;
        }
    }

    public String asHex(byte buf[]) {
        StringBuffer strbuf = new StringBuffer(buf.length * 2);
        int i;
        for (i = 0; i < buf.length; i++) {
            if (((int) buf[i] & 0xff) < 0x10) {
                strbuf.append("0");
            }
            strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
        }
        return strbuf.toString();
    }

    public RMStatus SendOnEmailByPreference(String emailid, String subject, String message, String[] cc, String[] bcc, String[] files, String[] mimetypes, int type, int preference, int sourceId) {
        SessionFactoryUtil suSettings = new SessionFactoryUtil(SessionFactoryUtil.settings);
        Session sSettings = suSettings.openSession();
        SettingsUtil sUtil = new SettingsUtil(suSettings, sSettings);       
        RMStatus status = new RMStatus();
        status.iStatus = -8;
        int retrycount = 0;
        status.strStatus = "Email Server Configuration missing";       
        RMStatus notificationAlert = new RMStatus();
        notificationAlert.iStatus = 0;
        notificationAlert.strStatus = "completed";
        String sid = null;
        Class c;
        SessionFactoryUtil suChannelLogs = new SessionFactoryUtil(SessionFactoryUtil.channellogs);
        Session sChannelLogs = suChannelLogs.openSession();
        ChannelLogsUtils cUtils = new ChannelLogsUtils(suChannelLogs, sChannelLogs);
        try {
            Constructor argsConstructor;
            Method[] allMethods = null;
            Object object = null;
            Object obj = sUtil.getSetting(type, preference);
            if (obj != null) {
                if (obj instanceof OOBEmailChannelSettings) {
                    OOBEmailChannelSettings settings = (OOBEmailChannelSettings) obj;
                    if (settings.status == ACTIVE) {
                        EmailConnectorSettings email = new EmailConnectorSettings();
                        email.smtpIp = settings.getSmtpIp();
                        email.password = settings.getPassword();
                        email.fromEmail = settings.getFromEmail();
                        email.fromName = settings.getFromName();
                        email.authRequired = settings.isAuthRequired();
                        email.ssl = settings.isSsl();
                        email.port = settings.getPort();
                        email.reserve1 = settings.getReserve1();
                        email.reserve2 = settings.getReserve2();
                        email.reserve3 = settings.getReserve3();
                        email.userId = settings.getUserId();
                        try {
                            c = Class.forName(settings.getClassName());
                            argsConstructor = c.getConstructor();
                            object = argsConstructor.newInstance();
                            allMethods = c.getDeclaredMethods();
                        } catch (Exception e) {
                            logger.error("Exception at SendNotification ", e);
                        }
                        for (Method m : allMethods) {
                            String mname = m.getName();
                            if (mname.compareTo("Load") == 0) {
                                m.setAccessible(true);
                                try {
                                    m.invoke(object, email);
                                } catch (Exception ex) {
                                    logger.error("Exception at SendNotification ", ex);
                                }
                            }
                        }
                        mainLable:
                        for (int j = settings.getRetrycount(); j >= 0; --j) {
                            retrycount++;
                            for (Method m : allMethods) {
                                String mname = m.getName();
                                if (mname.compareTo("SendMessage") == 0) {
                                    m.setAccessible(true);
                                    try {
                                        sid = (String) m.invoke(object, emailid, subject, message, cc, bcc, files, mimetypes);
                                    } catch (Exception ex) {
                                        logger.error("Exception at SendNotification ", ex);
                                    }
                                }
                            }
                            if (sid != null) {
                                for (Method m : allMethods) {
                                    String mname = m.getName();
                                    if (mname.compareTo("GetStatus") == 0) {
                                        m.setAccessible(true);
                                        try {
                                            Thread.sleep(5000);
                                            status = (RMStatus) m.invoke(object, sid);
                                        } catch (Exception ex) {
                                            logger.error("Exception at SendNotification ", ex);
                                        }
                                    }
                                }
                            } else if (sid == null) {
                                status = new RMStatus();
                                status.iStatus = -5;
                                status.strStatus = "Failed";
                            }
                            if (status != null) {
                                if (status.iStatus == 0 && status.strStatus.equals("completed")) {
                                    RmChannellogs cLogs = new RmChannellogs();                                    
                                    cLogs.setType(EMAIL);
                                    cLogs.setPreference(settings.getPreference());
                                    cLogs.setMsgid(sid);
                                    cLogs.setEmail(emailid);
                                    cLogs.setMessage(message);
                                    cLogs.setSentUtctime(new Date());
                                    cLogs.setErrorstatus(status.strStatus);
                                    cLogs.setSourceid(sourceId);
                                    cLogs.setStatus(status.iStatus);
                                    cLogs.setRetrycount(retrycount);
                                    cLogs.setLastupdateUtctime(new Date());
                                    cUtils.addChannellogs(settings.getPreference(), EMAIL, cLogs);
                                    break mainLable;
                                } else if (j == 0) {
                                    if (settings.autofailover == ACTIVE) {
                                        continue;
                                    } else {
                                        break mainLable;
                                    }
                                }
                                RmChannellogs cLogs = new RmChannellogs();                                
                                cLogs.setType(EMAIL);
                                cLogs.setPreference(settings.getPreference());
                                cLogs.setMsgid(sid);
                                cLogs.setEmail(emailid);
                                cLogs.setMessage(message);
                                cLogs.setSentUtctime(new Date());
                                cLogs.setErrorstatus(status.strStatus);
                                cLogs.setSourceid(sourceId);
                                cLogs.setStatus(status.iStatus);
                                cLogs.setRetrycount(retrycount);
                                cLogs.setLastupdateUtctime(new Date());
                                cUtils.addChannellogs(settings.getPreference(), EMAIL, cLogs);
                            } else {
                                RmChannellogs cLogs = new RmChannellogs();                                
                                cLogs.setType(EMAIL);
                                cLogs.setPreference(settings.getPreference());
                                cLogs.setMsgid(sid);
                                cLogs.setEmail(emailid);
                                cLogs.setMessage(message);
                                cLogs.setSentUtctime(new Date());
                                cLogs.setErrorstatus(status.strStatus);
                                cLogs.setSourceid(sourceId);
                                cLogs.setStatus(status.iStatus);
                                cLogs.setRetrycount(retrycount);
                                cLogs.setLastupdateUtctime(new Date());
                                cUtils.addChannellogs(settings.getPreference(), EMAIL, cLogs);
                            }
                            if (settings.autofailover == ACTIVE) {
                                continue;
                            } else {
                                break mainLable;
                            }
                        }
                    }
                }
            }
            return status;
        } finally {
            sSettings.close();
            suSettings.close();
        }
    }

    public RMStatus SendEmailOperator(String emailid, String subject, String message, String[] cc, String[] bcc, String[] files, String[] mimetypes, int sourceId) {
        SessionFactoryUtil suSettings = new SessionFactoryUtil(SessionFactoryUtil.settings);
        Session sSettings = suSettings.openSession();
        SettingsUtil sUtil = new SettingsUtil(suSettings, sSettings);      
        RMStatus status = new RMStatus();
        status.iStatus = -8;
        int retrycount = 0;
        status.strStatus = "Email Server Configuration missing";        
        String sid = null;
        Class c;
        SessionFactoryUtil suChannelLogs = new SessionFactoryUtil(SessionFactoryUtil.channellogs);
        Session sChannelLogs = suChannelLogs.openSession();
        ChannelLogsUtils cUtils = new ChannelLogsUtils(suChannelLogs, sChannelLogs);
        try {
            Constructor argsConstructor;
            Method[] allMethods = null;
            Object object = null;
            Object[] obj = sUtil.getSettingsByTypes(EMAIL);
            if (obj != null) {
                mainLable:
                for (int i = 0; i < obj.length; i++) {
                    if (obj[i] instanceof OOBEmailChannelSettings) {
                        OOBEmailChannelSettings settings = (OOBEmailChannelSettings) obj[i];
                        if (settings.status == ACTIVE) {
                            EmailConnectorSettings email = new EmailConnectorSettings();
                            email.smtpIp = settings.getSmtpIp();
                            email.password = settings.getPassword();
                            email.fromEmail = settings.getFromEmail();
                            email.fromName = settings.getFromName();
                            email.authRequired = settings.isAuthRequired();
                            email.ssl = settings.isSsl();
                            email.port = settings.getPort();
                            email.reserve1 = settings.getReserve1();
                            email.reserve2 = settings.getReserve2();
                            email.reserve3 = settings.getReserve3();
                            email.userId = settings.getUserId();
                            try {
                                c = Class.forName(settings.getClassName());
                                argsConstructor = c.getConstructor();
                                object = argsConstructor.newInstance();
                                allMethods = c.getDeclaredMethods();
                            } catch (Exception e) {
                                logger.error("Exception at SendNotification ", e);
                            }
                            for (Method m : allMethods) {
                                String mname = m.getName();
                                if (mname.compareTo("Load") == 0) {
                                    m.setAccessible(true);
                                    try {
                                        m.invoke(object, email);
                                    } catch (Exception ex) {
                                        logger.error("Exception at SendNotification ", ex);
                                    }
                                }
                            }
                            for (int j = settings.getRetrycount(); j >= 0; --j) {
                                retrycount++;
                                for (Method m : allMethods) {
                                    String mname = m.getName();
                                    if (mname.compareTo("SendMessage") == 0) {
                                        m.setAccessible(true);
                                        try {
                                            String strStress = null;
                                            try {
                                                strStress = LoadSettings.g_sSettings.getProperty("reserved.6");
                                                if (strStress != null) {
                                                    if (strStress.compareToIgnoreCase("yes") == 0) {
                                                        Date d = new Date();
                                                        byte[] SHA1hash = SHA1(emailid + message + subject + d.toString());
                                                        sid = asHex(SHA1hash);
                                                    }
                                                } else {
                                                    sid = (String) m.invoke(object, emailid, subject, message, cc, bcc, files, mimetypes);
                                                }
                                            } catch (Exception ex) {
                                                logger.error("Exception at SendNotification ", ex);
                                            }
                                        } catch (Exception ex) {
                                            logger.error("Exception at SendNotification ", ex);
                                        }
                                    }
                                }
                                if (sid != null) {
                                    for (Method m : allMethods) {
                                        String mname = m.getName();
                                        if (mname.compareTo("GetStatus") == 0) {
                                            m.setAccessible(true);
                                            try {
                                                Thread.sleep(10);
                                                String strStress = null;
                                                try {
                                                    strStress = LoadSettings.g_sSettings.getProperty("reserved.6");
                                                    if (strStress != null) {
                                                        if (strStress.compareToIgnoreCase("yes") == 0) {
                                                            RMStatus status1 = new RMStatus();
                                                            status1.iStatus = 0;
                                                            status1.strStatus = "completed";
                                                            status = status1;
                                                        }
                                                    } else {
                                                        status = (RMStatus) m.invoke(object, sid);
                                                    }
                                                } catch (Exception ex) {
                                                    logger.error("Exception at SendNotification ", ex);
                                                }
                                            } catch (Exception ex) {
                                                logger.error("Exception at SendNotification ", ex);
                                            }
                                        }
                                    }
                                } else if (sid == null) {
                                    status = new RMStatus();
                                    status.iStatus = -5;
                                    status.strStatus = "Failed";
                                }
                                if (status != null) {
                                    if (status.iStatus == 0 && status.strStatus.equals("completed")) {
                                        RmChannellogs cLogs = new RmChannellogs();
                                        cLogs.setType(EMAIL);
                                        cLogs.setPreference(settings.getPreference());
                                        cLogs.setMsgid(sid);
                                        cLogs.setEmail(emailid);
                                        cLogs.setMessage(message);
                                        cLogs.setSentUtctime(new Date());
                                        cLogs.setErrorstatus(status.strStatus);
                                        cLogs.setSourceid(sourceId);
                                        cLogs.setStatus(status.iStatus);
                                        cLogs.setRetrycount(retrycount);
                                        cLogs.setLastupdateUtctime(new Date());
                                        cUtils.addChannellogs(settings.getPreference(), EMAIL, cLogs);
                                        break mainLable;
                                    } else if (j == 0) {
                                        if (settings.autofailover == ACTIVE) {
                                            continue;
                                        } else {
                                            break mainLable;
                                        }
                                    }
                                    RmChannellogs cLogs = new RmChannellogs();                                    
                                    cLogs.setType(EMAIL);
                                    cLogs.setPreference(settings.getPreference());
                                    cLogs.setMsgid(sid);
                                    cLogs.setEmail(emailid);
                                    cLogs.setMessage(message);
                                    cLogs.setSentUtctime(new Date());
                                    cLogs.setErrorstatus(status.strStatus);
                                    cLogs.setSourceid(sourceId);
                                    cLogs.setStatus(status.iStatus);
                                    cLogs.setRetrycount(retrycount);
                                    cLogs.setLastupdateUtctime(new Date());
                                    cUtils.addChannellogs(settings.getPreference(), EMAIL, cLogs);
                                } else {
                                    RmChannellogs cLogs = new RmChannellogs();                                    
                                    cLogs.setType(EMAIL);
                                    cLogs.setPreference(settings.getPreference());
                                    cLogs.setMsgid(sid);
                                    cLogs.setEmail(emailid);
                                    cLogs.setMessage(message);
                                    cLogs.setSentUtctime(new Date());
                                    cLogs.setErrorstatus(status.strStatus);
                                    cLogs.setSourceid(sourceId);
                                    cLogs.setStatus(status.iStatus);
                                    cLogs.setRetrycount(retrycount);
                                    cLogs.setLastupdateUtctime(new Date());
                                    cUtils.addChannellogs(settings.getPreference(), EMAIL, cLogs);
                                }
                            }
                            if (settings.autofailover == ACTIVE) {
                                continue;
                            } else {
                                break mainLable;
                            }
                        }
                    }
                }
            }
            return status;
        } finally {
            sSettings.close();
            suSettings.close();
        }
    }
    
    
    // email with attachment
    public RMStatus SendEmailWithAttachment(String channelId, String emailid, String subject, String message, String[] cc, String[] bcc, String[] files, String[] mimetypes, int sourceId) {
        SessionFactoryUtil suSettings = new SessionFactoryUtil(SessionFactoryUtil.settings);
        Session sSettings = suSettings.openSession();
        SettingsUtil sUtil = new SettingsUtil(suSettings, sSettings);        
        RMStatus notificationAlert = new RMStatus();
        notificationAlert.iStatus = 0;
        notificationAlert.strStatus = "completed";
        RMStatus status = new RMStatus();
        status.iStatus = -8;
        int retrycount = 0;
        status.strStatus = "Email Server Configuration missing";
        String sid = null;
        Class c;
        SessionFactoryUtil suChannelLogs = new SessionFactoryUtil(SessionFactoryUtil.channellogs);
        Session sChannelLogs = suChannelLogs.openSession();
        ChannelLogsUtils cUtils = new ChannelLogsUtils(suChannelLogs, sChannelLogs);
        try {
            Constructor argsConstructor;
            Method[] allMethods = null;
            Object object = null;
            Object[] obj = sUtil.getSettingsByTypes(EMAIL);
            if (obj != null) {
                mainLable:
                for (int i = 0; i < obj.length; i++) {
                    if (obj[i] instanceof OOBEmailChannelSettings) {
                        OOBEmailChannelSettings settings = (OOBEmailChannelSettings) obj[i];
                        if (settings.status == ACTIVE) {
                            EmailConnectorSettings email = new EmailConnectorSettings();
                            email.smtpIp = settings.getSmtpIp();
                            email.password = settings.getPassword();
                            email.fromEmail = settings.getFromEmail();
                            email.fromName = settings.getFromName();
                            email.authRequired = settings.isAuthRequired();
                            email.ssl = settings.isSsl();
                            email.port = settings.getPort();
                            email.reserve1 = settings.getReserve1();
                            email.reserve2 = settings.getReserve2();
                            email.reserve3 = settings.getReserve3();
                            email.userId = settings.getUserId();
                            try {
                                c = Class.forName(settings.getClassName());
                                argsConstructor = c.getConstructor();
                                object = argsConstructor.newInstance();
                                allMethods = c.getDeclaredMethods();
                            } catch (Exception e) {
                                logger.error("Exception at SendEmailWithAttachment ", e);
                            }
                            for (Method m : allMethods) {
                                String mname = m.getName();
                                if (mname.compareTo("Load") == 0) {
                                    m.setAccessible(true);
                                    try {
                                        m.invoke(object, email);
                                    } catch (Exception ex) {
                                        logger.error("Exception at SendEmailWithAttachment ", ex);
                                    }
                                }
                            }
                            for (int j = settings.getRetrycount(); j >= 0; --j) {
                                retrycount++;
                                for (Method m : allMethods) {
                                    String mname = m.getName();
                                    if (mname.compareTo("SendMessageWithAttachment") == 0) {
                                        m.setAccessible(true);
                                        try {
                                            String strStress = null;
                                            try {
                                                sid = (String) m.invoke(object, emailid, subject, message, cc, bcc, files, mimetypes);
                                            } catch (Exception ex) {
                                                logger.error("Exception at SendEmailWithAttachment ", ex);
                                            }
                                        } catch (Exception ex) {
                                            logger.error("Exception at SendEmailWithAttachment ", ex);
                                        }
                                    }
                                }
                                if (sid != null) {
                                    for (Method m : allMethods) {
                                        String mname = m.getName();
                                        if (mname.compareTo("GetStatus") == 0) {
                                            m.setAccessible(true);
                                            try {
                                                Thread.sleep(10);
                                                String strStress = null;
                                                try {
                                                    strStress = LoadSettings.g_sSettings.getProperty("reserved.6");
                                                    if (strStress != null) {
                                                        if (strStress.compareToIgnoreCase("yes") == 0) {
                                                            RMStatus status1 = new RMStatus();
                                                            status1.iStatus = 0;
                                                            status1.strStatus = "completed";
                                                            status = status1;
                                                        } else {
                                                            status = (RMStatus) m.invoke(object, sid);
                                                        }
                                                    } else {
                                                        status = (RMStatus) m.invoke(object, sid);
                                                    }
                                                } catch (Exception ex) {
                                                    logger.error("Exception at SendEmailWithAttachment ", ex);
                                                }
                                            } catch (Exception ex) {
                                                logger.error("Exception at SendEmailWithAttachment ", ex);
                                            }
                                        }
                                    }
                                } else if (sid == null) {
                                    status = new RMStatus();
                                    status.iStatus = -5;
                                    status.strStatus = "Failed";
                                }
                                if (status != null) {
                                    if (status.iStatus == 0 && status.strStatus.equals("completed")) {
                                        RmChannellogs cLogs = new RmChannellogs();                                       
                                        cLogs.setType(EMAIL);
                                        cLogs.setPreference(settings.getPreference());
                                        cLogs.setMsgid(sid);
                                        cLogs.setEmail(emailid);
                                        cLogs.setMessage(message);
                                        cLogs.setSentUtctime(new Date());
                                        cLogs.setErrorstatus(status.strStatus);
                                        cLogs.setSourceid(sourceId);
                                        cLogs.setStatus(status.iStatus);
                                        cLogs.setRetrycount(retrycount);
                                        cLogs.setLastupdateUtctime(new Date());
                                        cUtils.addChannellogs(settings.getPreference(), EMAIL, cLogs);
                                        break mainLable;
                                    } else if (j == 0) {
                                        if (settings.autofailover == ACTIVE) {
                                            continue;
                                        } else {
                                            break mainLable;
                                        }
                                    }
                                    RmChannellogs cLogs = new RmChannellogs();                                    
                                    cLogs.setType(EMAIL);
                                    cLogs.setPreference(settings.getPreference());
                                    cLogs.setMsgid(sid);
                                    cLogs.setEmail(emailid);
                                    cLogs.setMessage(message);
                                    cLogs.setSentUtctime(new Date());
                                    cLogs.setErrorstatus(status.strStatus);
                                    cLogs.setSourceid(sourceId);
                                    cLogs.setStatus(status.iStatus);
                                    cLogs.setRetrycount(retrycount);
                                    cLogs.setLastupdateUtctime(new Date());
                                    cUtils.addChannellogs(settings.getPreference(), EMAIL, cLogs);
                                } else {
                                    RmChannellogs cLogs = new RmChannellogs();                                    
                                    cLogs.setType(EMAIL);
                                    cLogs.setPreference(settings.getPreference());
                                    cLogs.setMsgid(sid);
                                    cLogs.setEmail(emailid);
                                    cLogs.setMessage(message);
                                    cLogs.setSentUtctime(new Date());
                                    cLogs.setErrorstatus(status.strStatus);
                                    cLogs.setSourceid(sourceId);
                                    cLogs.setStatus(status.iStatus);
                                    cLogs.setRetrycount(retrycount);
                                    cLogs.setLastupdateUtctime(new Date());
                                    cUtils.addChannellogs(settings.getPreference(), EMAIL, cLogs);
                                }
                            }
                            if (settings.autofailover == ACTIVE) {
                                continue;
                            } else {
                                break mainLable;
                            }
                        }
                    }
                }
            }
            return status;
        } finally {
            sSettings.close();
            suSettings.close();
        }
    }
}
