/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.settings;

/**
 *
 * @author abhishekingle
 */
public class PasswordPolicySetting {
    
    public int passwordLength;

    public int passwordExpiryTime;

    public int issuingLimitDuration;

    public int enforcementDormancytime;

    public int invalidAttempts;

    public int oldPasswordCannotbeReused;

    public int passwordTye;

    public boolean changePasswordAfterFirstlogin;

    public boolean allowEPINforResetandReissue;

    public boolean allowCRQAforResetandReissue;

    public boolean allowRepetationOfChars;

    public boolean allowCommonPassword;

    public int passwordGenerations;

    public String[] arrAllowedPassword;

    public boolean passwordAlert;

    public int passwordAlertVia;

    public int passwordAlertAttempts;

    public int passwordAlertTemplateID;
    
}
