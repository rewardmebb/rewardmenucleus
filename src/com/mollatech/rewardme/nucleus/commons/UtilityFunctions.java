/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.commons;

import com.google.gson.Gson;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Random;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.naming.Context;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import org.bouncycastle.util.encoders.Base64;

/**
 *
 * @author abhishekingle
 */
public class UtilityFunctions {
    
    private static final int BUFFER_SIZE = 4096;

    public static DirContext context;

    public String convertToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; i++) {
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if ((0 <= halfbyte) && (halfbyte <= 9)) {
                    buf.append((char) ('0' + halfbyte));
                } else {
                    buf.append((char) ('a' + (halfbyte - 10)));
                }
                halfbyte = data[i] & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }

    public static String Bas64SHA1(String message) {
        try {
            MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
            byte[] array = sha1.digest(message.getBytes());
            return new String(Base64.encode(array));
        } catch (Exception e) {
            return null;
        }
    }

    public String HexSHA1(String message) {
        try {
            MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
            byte[] array = sha1.digest(message.getBytes());
            return convertToHex(array);
        } catch (Exception e) {
            return null;
        }
    }

    public void copyFile(File sourceFile, File destinationFile) {
        try {
            FileInputStream fileInputStream = new FileInputStream(sourceFile);
            FileOutputStream fileOutputStream = new FileOutputStream(destinationFile);
            int bufferSize;
            byte[] bufffer = new byte[512];
            while ((bufferSize = fileInputStream.read(bufffer)) > 0) {
                fileOutputStream.write(bufffer, 0, bufferSize);
            }
            fileInputStream.close();
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int delete(File file) {
        try {
            if (file.isDirectory()) {
                if (file.list().length == 0) {
                    file.delete();
                } else {
                    String files[] = file.list();
                    for (String temp : files) {
                        File fileDelete = new File(file, temp);
                        delete(fileDelete);
                    }
                    if (file.list().length == 0) {
                        file.delete();
                    }
                }
            } else {
                file.delete();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return -1;
    }

    public static Object deserializeFromObject(ByteArrayInputStream binaryObject) {
        ObjectInputStream ins;
        try {
            ins = new ObjectInputStream(binaryObject);
            Object object = (Object) ins.readObject();
            ins.close();
            return object;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String MaskData(String data) {
        String set = data.substring(data.length() - 4, data.length());
        String retData = "";
        for (int i = 0; i < data.length(); i++) {
            if (i < data.length() - 4) {
                retData = "*" + retData;
            }
        }
        retData = retData + set;
        return retData;
    }

    public static boolean isValidEmail(String emailAddress) {
        return emailAddress.contains(" ") == false && emailAddress.matches(".+@.+\\.[a-z]+");
    }

    public static boolean isValidPhoneNumber(String stringNumber) {
        if (stringNumber.equals("")) {
            return false;
        }
        if (stringNumber.startsWith("+")) {
            stringNumber = stringNumber.replaceAll(" ", "");
            char[] charNumber = stringNumber.toCharArray();
            for (int i = 1; i < charNumber.length; i++) {
                if (!Character.isDigit(charNumber[i])) {
                    return false;
                }
            }
            return true;
        }
        stringNumber = stringNumber.replaceAll(" ", "");
        char[] charNumber = stringNumber.toCharArray();
        for (int i = 0; i < charNumber.length; i++) {
            if (!Character.isDigit(charNumber[i])) {
                return false;
            }
        }
        return true;
    }

    public static byte[] SHA1(String message) {
        try {
            MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
            byte[] array = sha1.digest(message.getBytes());
            return array;
        } catch (Exception e) {
            return null;
        }
    }

    public String writeTofile(String filename, String data) {
        try {
            Writer output = null;
            File file = new File(filename);
            output = new BufferedWriter(new FileWriter(file));
            output.write(data);
            output.close();
            return file.getAbsolutePath();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static String ConvertGeotoBase32(double latitude, double longitude) {
        try {
            String encodedetails = null;
            String finalencodedetails = null;
            String result = null;
            String fin = null;
            String test1 = String.valueOf(latitude);
            String test2 = String.valueOf(longitude);
            test1 = test1.substring(0, test1.indexOf(".") + 3);
            test2 = test2.substring(0, test2.indexOf(".") + 3);
            if (test1.charAt(0) == '-' && test2.charAt(0) == '-') {
                test2 = test2.substring(1, test2.length());
                result = test1.concat(test2);
                fin = result.replace(".", "");
                BigDecimal big = new BigDecimal(fin);
                BigInteger theInt = big.unscaledValue();
                String str = "" + theInt;
                encodedetails = new String(Base32.encode(str.getBytes()));
                encodedetails = "5".concat(encodedetails);
            } else if (test2.charAt(0) == '-') {
                test2 = test2.substring(1, test2.length());
                result = test1.concat(test2);
                result = "-".concat(result);
                fin = result.replace(".", "");
                BigDecimal big = new BigDecimal(fin);
                BigInteger theInt = big.unscaledValue();
                String str = "" + theInt;
                encodedetails = new String(Base32.encode(str.getBytes()));
                encodedetails = "7".concat(encodedetails);
            } else if (test1.charAt(0) == '-' && test2.charAt(0) != '-') {
                result = test1.concat(test2);
                fin = result.replace(".", "");
                BigDecimal big = new BigDecimal(fin);
                BigInteger theInt = big.unscaledValue();
                String str = "" + theInt;
                encodedetails = new String(Base32.encode(str.getBytes()));
                encodedetails = "8".concat(encodedetails);
            } else {
                result = test1.concat(test2);
                fin = new String(result.replace(".", ""));
                BigDecimal big = new BigDecimal(fin);
                BigInteger theInt = big.unscaledValue();
                String str = "" + theInt;
                encodedetails = new String(Base32.encode(str.getBytes()));
                encodedetails = "6".concat(encodedetails);
            }
            if (test1.length() == 4) {
                if (test1.charAt(0) == '0') {
                    finalencodedetails = "8".concat(encodedetails);
                } else {
                    finalencodedetails = "4".concat(encodedetails);
                }
            } else if (test1.length() == 5) {
                if (test1.charAt(0) == '-' && test1.charAt(1) == '0') {
                    finalencodedetails = "9".concat(encodedetails);
                } else {
                    finalencodedetails = "5".concat(encodedetails);
                }
            } else if (test1.length() == 6) {
                finalencodedetails = "6".concat(encodedetails);
            } else if (test1.length() == 7) {
                finalencodedetails = "7".concat(encodedetails);
            }
            return finalencodedetails;
        } catch (Exception e) {
            return null;
        }
    }

    public String[] DecodeGEOCode(String encodeValue) {
        try {
            int Encodedvalue = 0;
            String en = null;
            String Encodestr = null;
            String encodeval = null;
            int length = 0;
            String Longitude = null;
            String Latitude = null;
            String Lon = null;
            String Lat = null;
            String[] longilatitude = new String[2];
            if (encodeValue.charAt(1) == '5') {
                if (encodeValue.charAt(0) == '9') {
                    encodeval = encodeValue.substring(2, encodeValue.length());
                    Encodestr = new String(Base32.decode(encodeval));
                    String Ln = Encodestr.substring(0, 1) + "0" + Encodestr.substring(1, Encodestr.length());
                    Lon = Ln.substring(0, 5 - 1);
                    Lat = Ln.substring(Lon.length(), Ln.length());
                    Longitude = Lon.substring(0, Lon.length() - 2) + "." + Lon.substring(Lon.length() - 2, Lon.length());
                    Latitude = "-".concat(Lat.substring(0, Lat.length() - 2) + "." + Lat.substring(Lat.length() - 2, Lat.length()));
                } else {
                    encodeval = encodeValue.substring(2, encodeValue.length());
                    length = Integer.parseInt(encodeValue.substring(0, 1));
                    Encodestr = new String(Base32.decode(encodeval));
                    Lon = Encodestr.substring(0, length - 1);
                    Lat = Encodestr.substring(Lon.length(), Encodestr.length());
                    Longitude = Lon.substring(0, Lon.length() - 2) + "." + Lon.substring(Lon.length() - 2, Lon.length());
                    Latitude = "-".concat(Lat.substring(0, Lat.length() - 2) + "." + Lat.substring(Lat.length() - 2, Lat.length()));
                }
            } else if (encodeValue.charAt(1) == '6') {
                if (encodeValue.charAt(0) == '8') {
                    encodeval = encodeValue.substring(2, encodeValue.length());
                    Encodestr = new String(Base32.decode(encodeval));
                    String Ln = "0".concat(Encodestr);
                    Lon = Ln.substring(0, 4 - 1);
                    Lat = Ln.substring(Lon.length(), Ln.length());
                    Longitude = Lon.substring(0, Lon.length() - 2) + "." + Lon.substring(Lon.length() - 2, Lon.length());
                    Latitude = Lat.substring(0, Lat.length() - 2) + "." + Lat.substring(Lat.length() - 2, Lat.length());
                } else {
                    encodeval = encodeValue.substring(2, encodeValue.length());
                    length = Integer.parseInt(encodeValue.substring(0, 1));
                    Encodestr = new String(Base32.decode(encodeval));
                    Lon = Encodestr.substring(0, length - 1);
                    Lat = Encodestr.substring(Lon.length(), Encodestr.length());
                    Longitude = Lon.substring(0, Lon.length() - 2) + "." + Lon.substring(Lon.length() - 2, Lon.length());
                    Latitude = Lat.substring(0, Lat.length() - 2) + "." + Lat.substring(Lat.length() - 2, Lat.length());
                }
            } else if (encodeValue.charAt(1) == '7') {
                if (encodeValue.charAt(0) == '8') {
                    encodeval = encodeValue.substring(2, encodeValue.length());
                    Encodestr = new String(Base32.decode(encodeval));
                    String Ln = Encodestr.substring(0, 1) + "0" + Encodestr.substring(1, Encodestr.length());
                    Lon = Ln.substring(1, 5 - 1);
                    Lat = Ln.substring(Lon.length() + 1, Ln.length());
                    Longitude = Lon.substring(0, Lon.length() - 2) + "." + Lon.substring(Lon.length() - 2, Lon.length());
                    Latitude = "-".concat(Lat.substring(0, Lat.length() - 2) + "." + Lat.substring(Lat.length() - 2, Lat.length()));
                } else {
                    encodeval = encodeValue.substring(2, encodeValue.length());
                    length = Integer.parseInt(encodeValue.substring(0, 1));
                    Encodestr = new String(Base32.decode(encodeval));
                    Lon = Encodestr.substring(1, length);
                    Lat = Encodestr.substring(Lon.length() + 1, Encodestr.length());
                    Longitude = Lon.substring(0, Lon.length() - 2) + "." + Lon.substring(Lon.length() - 2, Lon.length());
                    Latitude = "-".concat(Lat.substring(0, Lat.length() - 2) + "." + Lat.substring(Lat.length() - 2, Lat.length()));
                }
            } else if (encodeValue.charAt(1) == '8') {
                if (encodeValue.charAt(0) == '9') {
                    encodeval = encodeValue.substring(2, encodeValue.length());
                    Encodestr = new String(Base32.decode(encodeval));
                    String Ln = Encodestr.substring(0, 1) + "0" + Encodestr.substring(1, Encodestr.length());
                    Lon = Ln.substring(0, 5 - 1);
                    Lat = Ln.substring(Lon.length(), Ln.length());
                    Longitude = Lon.substring(0, Lon.length() - 2) + "." + Lon.substring(Lon.length() - 2, Lon.length());
                    Latitude = Lat.substring(0, Lat.length() - 2) + "." + Lat.substring(Lat.length() - 2, Lat.length());
                } else {
                    encodeval = encodeValue.substring(2, encodeValue.length());
                    length = Integer.parseInt(encodeValue.substring(0, 1));
                    Encodestr = new String(Base32.decode(encodeval));
                    Lon = Encodestr.substring(0, length - 1);
                    Lat = Encodestr.substring(Lon.length(), Encodestr.length());
                    Longitude = Lon.substring(0, Lon.length() - 2) + "." + Lon.substring(Lon.length() - 2, Lon.length());
                    Latitude = Lat.substring(0, Lat.length() - 2) + "." + Lat.substring(Lat.length() - 2, Lat.length());
                }
            }
            longilatitude[0] = Longitude;
            longilatitude[1] = Latitude;
            return longilatitude;
        } catch (Exception e) {
            return null;
        }
    }

    public static int calculatePercentage(int value, int total) {
        int percentage = (value * 100) / total;
        return percentage;
    }

    public static int getRandomNumber(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }

    public static void main(String[] args) {
        String reg = Bas64SHA1("18728281");
        Date d = new Date(1401460868L);
    }

    public DirContext createConnection(String ip, String port) {
        Hashtable hashtable = new Hashtable();
        hashtable.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        hashtable.put(Context.PROVIDER_URL, "ldap://" + ip + ":" + port);
        hashtable.put(Context.SECURITY_AUTHENTICATION, "simple");
        try {
            context = new InitialDirContext(hashtable);
            return context;
        } catch (Exception e) {
            return null;
        }
    }

    public static void unzip(String zipFilePath, String destDirectory) throws IOException {
        File destDir = new File(destDirectory);
        if (!destDir.exists()) {
            destDir.mkdir();
        }
        ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFilePath));
        ZipEntry entry = zipIn.getNextEntry();
        while (entry != null) {
            String filePath = destDirectory + File.separator + entry.getName();
            if (!entry.isDirectory()) {
                extractFile(zipIn, filePath);
            } else {
                File dir = new File(filePath);
                dir.mkdir();
            }
            zipIn.closeEntry();
            entry = zipIn.getNextEntry();
        }
        zipIn.close();
    }

    private static void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
        byte[] bytesIn = new byte[BUFFER_SIZE];
        int read = 0;
        while ((read = zipIn.read(bytesIn)) != -1) {
            bos.write(bytesIn, 0, read);
        }
        bos.close();
    }

    public static String getTMReqDate(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
        return format.format(date);
    }

    public static String asHex(byte buf[]) {
        StringBuffer strbuf = new StringBuffer(buf.length * 2);
        int i;

        for (i = 0; i < buf.length; i++) {
            if (((int) buf[i] & 0xff) < 0x10) {
                strbuf.append("0");
            }

            strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
        }

        return strbuf.toString();
    }

    public Object copyValues(Object old, Object New, Class clazz) {
        Gson gson = new Gson();
        String tmp = gson.toJson(old);
        New = gson.fromJson(tmp, clazz);
        return New;
    }
}
