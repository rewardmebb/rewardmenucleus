/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.commons;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author abhishekingle
 */
public class TaxCalculation {
    
    public static Map calculateTax(String taxDetails, Double totalPaymentAmount) {
        Map<String, String> taxMap = new LinkedHashMap<String, String>();
        double gstTax = 0;
        double calculatedGST = 0;
        String showGST = gstTax + ":" + calculatedGST;
        taxMap.put("GST Tax", showGST);
        if (taxDetails != null && totalPaymentAmount != null) {
            try {
                
                gstTax = Float.parseFloat(taxDetails);                
                if (gstTax != 0) {
                    calculatedGST = (totalPaymentAmount * gstTax) / 100;
                    showGST = gstTax + ":" + calculatedGST;
                    taxMap.put("GST Tax", showGST);
                }                
                return taxMap;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return taxMap;
    }
}
