package com.mollatech.rewardme.nucleus.commons;

import com.mollatech.service.nucleus.crypto.LoadSettings;
import java.net.*;
import java.io.*;
import java.text.ParseException;
import java.util.Date;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

public class GetLocationFromAddress {

    static final Logger logger = Logger.getLogger(GetLocationFromAddress.class);
    
    public String getGeoData(String address) throws ParseException {
        if(logger.getLevel() == null){
            logger.setLevel(Level.ALL);
        }
        StringBuilder content = new StringBuilder();
        // wrapped them all in one try/catch statement.
        logger.info("GetLocationFromAddress called at "+new Date());
        try {
            String apiURL = LoadSettings.g_sSettings.getProperty("geo.location.boundary.api");
            String apiKey = LoadSettings.g_sSettings.getProperty("geo.location.api.key");
            apiURL = apiURL+"?address="+address+"&key="+apiKey;
            logger.info("apiURL "+apiURL);
            logger.info("apiKey "+apiKey);
            // create a url object
            URL url = new URL(apiURL);
            // create a urlconnection object
            URLConnection urlConnection = url.openConnection();
            // wrap the urlconnection in a bufferedreader
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line;
            // read from the urlconnection via the bufferedreader
            while ((line = bufferedReader.readLine()) != null) {
                content.append(line + "\n");
            }
            bufferedReader.close();
             
        } catch (Exception e) {
            e.printStackTrace();
        }
       // JSONObject jsonData = new JSONObject(content.toString());
        String bounds = null;
        try {
            JSONObject jsonobj = new JSONObject(content.toString());
            JSONArray jsonarray = jsonobj.getJSONArray("results");
            //System.out.println("length == " + jsonarray.length());
            if (jsonarray.length() > 0) {
                for (int i = 0; i < jsonarray.length(); i++) {
                    JSONObject jsonObj = jsonarray.getJSONObject(i);
                    JSONObject geometry = jsonObj.getJSONObject("geometry");
                    bounds = geometry.getString("bounds");
                    //System.out.println("geo = " + bounds);
                    
                }
            } else {
                logger.info("bounds "+bounds);
                return bounds;
                
            }

        } catch (Exception e) {
            e.printStackTrace();
            return bounds;
        }
        logger.info("bounds "+bounds);
        return bounds;
    }
}