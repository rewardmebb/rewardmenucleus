/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.commons;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.util.Enumeration;
import java.util.Random;
/**
 *
 * @author abhishekingle
 */
public class CommonUtility {
    
    public static void delete(File file) throws IOException {
        if (file.isDirectory()) {
            if (file.list().length == 0) {
                file.delete();
            } else {
                String files[] = file.list();
                for (String temp : files) {
                    File fileDelete = new File(file, temp);
                    delete(fileDelete);
                }
                if (file.list().length == 0) {
                    file.delete();
                }
            }
        } else {
            file.delete();
        }
    }

    public static String asHex(byte buf[]) {
        StringBuffer strbuf = new StringBuffer(buf.length * 2);
        int i;
        for (i = 0; i < buf.length; i++) {
            if (((int) buf[i] & 0xff) < 0x10) {
                strbuf.append("0");
            }
            strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
        }
        return strbuf.toString();
    }

    public static byte[] SHA1(String message) {
        try {
            MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
            byte[] array = sha1.digest(message.getBytes());
            return array;
        } catch (Exception e) {
            return null;
        }
    }

    public static String getUniqueId(String SessionId, String url, String data, String threadId) {
        String tempId = SessionId + ":" + url + ":" + data + ":" + threadId + ":" + System.currentTimeMillis() + ":" + randomString(20);
        tempId = asHex(tempId.getBytes());
        tempId = randomStringFromString(64, tempId);
        return tempId;
    }

    public static String randomString(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        }
        return sb.toString();
    }

    public static String randomStringFromString(int len, String val) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            sb.append(val.charAt(rnd.nextInt(val.length())));
        }
        return sb.toString();
    }

//    public static byte[] generateJKS(byte[] certData, WSSecurity security) throws Exception {
//        KeyStore keyObj = KeyStore.getInstance("PKCS12");
//        java.security.cert.Certificate c = null;
//        String alias = security.clientAlias;
//        InputStream in = new ByteArrayInputStream(security.keystoreData);
//        keyObj.load(in, security.keystorePassword.toCharArray());
//        if (keyObj.aliases().hasMoreElements()) {
//            Enumeration aliases = keyObj.aliases();
//            while (aliases.hasMoreElements()) {
//                alias = (String) aliases.nextElement();
//                c = keyObj.getCertificate(alias);
//            }
//        }
//        CertificateFactory cf = CertificateFactory.getInstance("X.509");
//        InputStream incert = new ByteArrayInputStream(certData);
//        java.security.cert.Certificate certiObj = cf.generateCertificate(incert);
//        KeyStore jks = KeyStore.getInstance("JKS");
//        jks.load(null, security.keystorePassword.toCharArray());
//        Certificate cert[] = new Certificate[1];
//        cert[0] = c;
//        Key key = keyObj.getKey(alias, security.keystorePassword.toCharArray());
//        jks.setKeyEntry(security.clientAlias, key, security.clientCertPassword.toCharArray(), cert);
//        jks.setCertificateEntry(security.serverAlias, certiObj);
//        File jksFile = File.createTempFile("temp", ".jks");
//        String jksFilePath = jksFile.getCanonicalPath();
//        FileOutputStream out1 = new FileOutputStream(jksFilePath);
//        jks.store(out1, security.keystorePassword.toCharArray());
//        out1.flush();
//        out1.close();
//        File pjks = new File(jksFilePath);
//        byte[] jksData = new byte[(int) pjks.length()];
//        FileInputStream inputStream = new FileInputStream(pjks);
//        inputStream.read(jksData);
//        inputStream.close();
//        jksFile.delete();
//        return jksData;
//    }

    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZqwertyuiopasdfghjklzxcvbnm";

    static Random rnd = new Random();
}
