/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.commons;

/**
 *
 * @author abhishekingle
 */
public class GlobalStatus {
    
    public static final int DELETED = -99;
    public static final int SUCCESS = 0;

    public static final int SUSPEND = -1;
    public static final int ACTIVE = 1;

    public static final int SENDTO_CHECKER = 2;
    public static final int CANCLE_BYCHECKER = -2;

    public static final int APPROVED = 3;
    public static final int REJECTED = -3;

    public static final int UPDATED = 4;
    public static final int PENDING = -4;

    public static final int PAID = 5;
    public static final int UNPAID = -5;

    public static final int ADDTOCREDIT = 6;
    public static final int TERMINATED = 7;

    public static final int CHANGE_PASSWORD_STATUS = 8;
    public static final int LOCKED = -8;

    public static final int DEFAULT = 9;

    public static final int FIRSTTIMELOGIN = -10;
    public static final int FIRSTTIMELOGINDONE = 10;

    public static final int UPDATEASITIS = -11;

    public static final int PROCESSED = 12;
    public static final int NOT_PROCESSED = -12;
    public static final int START_PROCESS = 13;
    public static final int STOP_PROCESS = 14;
    
    public static final int RATE = 1;
    public static final int APRATE = 2;
    public static final int FLATPRICE = 3;
    public static final int APIThrottling = 4;
    public static final int PROMOCODE = 5;
    public static final int TIERINGPRICE = 6;
    public static final int ALERTS = 7;
    public static final int FEATURE = 8;
   
    
}
