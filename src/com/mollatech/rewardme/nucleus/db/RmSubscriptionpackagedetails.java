package com.mollatech.rewardme.nucleus.db;
// Generated Jan 11, 2018 6:53:19 PM by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * RmSubscriptionpackagedetails generated by hbm2java
 */
public class RmSubscriptionpackagedetails  implements java.io.Serializable {


     private Integer subscriptionId;
     private String packageDuration;
     private String packageName;
     private Integer ownerId;
     private Date creationDate;
     private String paymentMode;
     private Float planAmount;
     private Integer status;
     private String tax;
     private Date updationDate;
     private String creditDeductionConfiguration;
     private Float mainCredits;
     private String packageDescription;
     private String recurrenceBillingPlanId;
     private Date expiryDate;

    public RmSubscriptionpackagedetails() {
    }

    public RmSubscriptionpackagedetails(String packageDuration, String packageName, Integer ownerId, Date creationDate, String paymentMode, Float planAmount, Integer status, String tax, Date updationDate, String creditDeductionConfiguration, Float mainCredits, String packageDescription, String recurrenceBillingPlanId, Date expiryDate) {
       this.packageDuration = packageDuration;
       this.packageName = packageName;
       this.ownerId = ownerId;
       this.creationDate = creationDate;
       this.paymentMode = paymentMode;
       this.planAmount = planAmount;
       this.status = status;
       this.tax = tax;
       this.updationDate = updationDate;
       this.creditDeductionConfiguration = creditDeductionConfiguration;
       this.mainCredits = mainCredits;
       this.packageDescription = packageDescription;
       this.recurrenceBillingPlanId = recurrenceBillingPlanId;
       this.expiryDate = expiryDate;
    }
   
    public Integer getSubscriptionId() {
        return this.subscriptionId;
    }
    
    public void setSubscriptionId(Integer subscriptionId) {
        this.subscriptionId = subscriptionId;
    }
    public String getPackageDuration() {
        return this.packageDuration;
    }
    
    public void setPackageDuration(String packageDuration) {
        this.packageDuration = packageDuration;
    }
    public String getPackageName() {
        return this.packageName;
    }
    
    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
    public Integer getOwnerId() {
        return this.ownerId;
    }
    
    public void setOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
    }
    public Date getCreationDate() {
        return this.creationDate;
    }
    
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
    public String getPaymentMode() {
        return this.paymentMode;
    }
    
    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }
    public Float getPlanAmount() {
        return this.planAmount;
    }
    
    public void setPlanAmount(Float planAmount) {
        this.planAmount = planAmount;
    }
    public Integer getStatus() {
        return this.status;
    }
    
    public void setStatus(Integer status) {
        this.status = status;
    }
    public String getTax() {
        return this.tax;
    }
    
    public void setTax(String tax) {
        this.tax = tax;
    }
    public Date getUpdationDate() {
        return this.updationDate;
    }
    
    public void setUpdationDate(Date updationDate) {
        this.updationDate = updationDate;
    }
    public String getCreditDeductionConfiguration() {
        return this.creditDeductionConfiguration;
    }
    
    public void setCreditDeductionConfiguration(String creditDeductionConfiguration) {
        this.creditDeductionConfiguration = creditDeductionConfiguration;
    }
    public Float getMainCredits() {
        return this.mainCredits;
    }
    
    public void setMainCredits(Float mainCredits) {
        this.mainCredits = mainCredits;
    }
    public String getPackageDescription() {
        return this.packageDescription;
    }
    
    public void setPackageDescription(String packageDescription) {
        this.packageDescription = packageDescription;
    }
    public String getRecurrenceBillingPlanId() {
        return this.recurrenceBillingPlanId;
    }
    
    public void setRecurrenceBillingPlanId(String recurrenceBillingPlanId) {
        this.recurrenceBillingPlanId = recurrenceBillingPlanId;
    }
    public Date getExpiryDate() {
        return this.expiryDate;
    }
    
    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }




}


