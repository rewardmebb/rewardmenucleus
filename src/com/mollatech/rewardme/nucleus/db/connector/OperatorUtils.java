/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector;

import com.mollatech.rewardme.nucleus.commons.GlobalStatus;
import com.mollatech.rewardme.nucleus.db.RmOperators;
import com.mollatech.rewardme.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.rewardme.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.rewardme.nucleus.settings.PasswordPolicySetting;
import com.mollatech.service.nucleus.crypto.AxiomProtect;
import java.util.Date;
import java.util.List;
import java.util.Random;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author abhishekingle
 */
public class OperatorUtils {
    
    private SessionFactoryUtil m_su;

    private Session m_session;

    static final Logger logger = Logger.getLogger(OperatorUtils.class);

    public int ChangeLoginStatus(String operatorid, int loginFlag) {
        try {
            RmOperators oObj = new RmOperators();
            Criteria criteria = m_session.createCriteria(RmOperators.class);
            criteria.add(Restrictions.eq("operatorid", operatorid));            
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return -2;
            } else {
                oObj = (RmOperators) list.get(0);
                oObj.setLoginflag(loginFlag);
                Transaction tx = null;
                try {
                    tx = m_session.beginTransaction();
                    m_session.save(oObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    logger.error("Exception at OperatorUtils ", e);
                }
            }
        } catch (Exception e) {
            logger.error("Exception at OperatorUtils ", e);
        }
        return -2;
    }

    public OperatorUtils(SessionFactoryUtil su, Session session) {
        m_su = su;
        m_session = session;
    }

    public int addOperator(String operatorid, String name, String phone, String emailid, String passsword, int roleid, int status, int currentAttempts, Date createdOn, Date lastAccessOn) {
        RmOperators oObj = new RmOperators();
        oObj.setOperatorid(operatorid);        
        oObj.setName(name);
        oObj.setPhone(phone);
        oObj.setEmailid(emailid);
        String securePassword = AxiomProtect.ProtectData(passsword);
        oObj.setPasssword(securePassword);
        oObj.setRoleid(roleid);
        oObj.setCurrentAttempts(currentAttempts);
        oObj.setCreatedOn(createdOn);
        oObj.setLastAccessOn(lastAccessOn);
        oObj.setStatus(status);
        oObj.setPasswordupdatedOn(new Date());
        oObj.setChangePassword(GlobalStatus.CHANGE_PASSWORD_STATUS);
        Transaction tx = null;
        try {
            tx = m_session.beginTransaction();
            m_session.save(oObj);
            tx.commit();
            return 0;
        } catch (Exception e) {
            logger.error("Exception at OperatorUtils ", e);
        }
        return -1;
    }

    public RmOperators getOperator(String operatorId) {
        try {
            Criteria criteria = m_session.createCriteria(RmOperators.class);
            criteria.add(Restrictions.eq("operatorid", operatorId));            
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmOperators oObj = (RmOperators) list.get(0);
                String password = oObj.getPasssword();
                oObj.setPasssword(AxiomProtect.AccessData(password));
                return oObj;
            }
        } catch (Exception e) {
            logger.error("Exception at OperatorUtils ", e);
        }
        return null;
    }

    public int changeOperatorStatus(String operatorid, int status) {
        try {
            RmOperators oObj = new RmOperators();
            Criteria criteria = m_session.createCriteria(RmOperators.class);
            criteria.add(Restrictions.eq("operatorid", operatorid));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return -2;
            } else {
                oObj = (RmOperators) list.get(0);
                Transaction tx = null;
                try {
                    oObj.setStatus(status);
                    oObj.setLastAccessOn(new Date());
                    tx = m_session.beginTransaction();
                    m_session.save(oObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    logger.error("Exception at OperatorUtils ", e);
                }
            }
        } catch (Exception e) {
            logger.error("Exception at OperatorUtils ", e);
        }
        return -2;
    }

    public int updateLastAccessOn(String operatorid, Date lastAccessOnDate) {
        try {
            RmOperators oObj = new RmOperators();
            Criteria criteria = m_session.createCriteria(RmOperators.class);
            criteria.add(Restrictions.eq("operatorid", operatorid));            
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return -2;
            } else if (lastAccessOnDate != null) {
                oObj = (RmOperators) list.get(0);
                oObj.setLastAccessOn(lastAccessOnDate);
                Transaction tx = null;
                try {
                    tx = m_session.beginTransaction();
                    m_session.save(oObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    logger.error("Exception at OperatorUtils ", e);
                }
            } else {
                return -1;
            }
        } catch (Exception e) {
            logger.error("Exception at OperatorUtils ", e);
        }
        return -2;
    }

    public RmOperators[] getAllOperators() {
        try {
            Criteria criteria = m_session.createCriteria(RmOperators.class);            
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmOperators[] Operators = new RmOperators[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    Operators[i] = (RmOperators) list.get(i);
                    String password = Operators[i].getPasssword();
                    Operators[i].setPasssword(AxiomProtect.AccessData(password));
                }
                return Operators;
            }
        } catch (Exception e) {
            logger.error("Exception at OperatorUtils ", e);
        }
        return null;
    }

    public int getCount() {
        try {
            Criteria criteria = m_session.createCriteria(RmOperators.class);
            Integer totalResult = ((Number) criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
            return totalResult;
        } catch (Exception e) {
            logger.error("Exception at OperatorUtils ", e);
            return -2;
        }
    }

    public RmOperators[] getAllRmOperators() {
        try {
            Criteria criteria = m_session.createCriteria(RmOperators.class);
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmOperators[] Operators = new RmOperators[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    Operators[i] = (RmOperators) list.get(i);
                    String password = Operators[i].getPasssword();
                    Operators[i].setPasssword(AxiomProtect.AccessData(password));
                }
                return Operators;
            }
        } catch (Exception e) {
            logger.error("Exception at OperatorUtils ", e);
        }
        return null;
    }

    public int deleteOperators(String Operatorid, String channelId) {
        try {
            Criteria criteria = m_session.createCriteria(RmOperators.class);
            criteria.add(Restrictions.eq("operatorid", Operatorid));            
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return -1;
            } else {
                RmOperators oObj = (RmOperators) list.get(0);
                Transaction tx = null;
                try {
                    tx = m_session.beginTransaction();
                    m_session.delete(oObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    logger.error("Exception at OperatorUtils ", e);
                }
            }
        } catch (Exception e) {
            logger.error("Exception at OperatorUtils ", e);
        }
        return -2;
    }

    public int changeRmOperators(String operatorId, RmOperators operators) {
        try {
            RmOperators oObj = new RmOperators();
            Criteria criteria = m_session.createCriteria(RmOperators.class);
            criteria.add(Restrictions.eq("operatorid", operatorId));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return -2;
            } else if (operators != null) {
                oObj = (RmOperators) list.get(0);                
                oObj.setCurrentAttempts(operators.getCurrentAttempts());
                oObj.setEmailid(operators.getEmailid());
                oObj.setLastAccessOn(operators.getLastAccessOn());
                oObj.setName(operators.getName());
                String securePassword = AxiomProtect.ProtectData(operators.getPasssword());
                oObj.setPasssword(securePassword);
                oObj.setPhone(operators.getPhone());
                oObj.setRoleid(operators.getRoleid());
                oObj.setStatus(operators.getStatus());
                Transaction tx = null;
                try {
                    tx = m_session.beginTransaction();
                    m_session.save(oObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    logger.error("Exception at OperatorUtils ", e);
                }
            } else {
                return -1;
            }
        } catch (Exception e) {
            logger.error("Exception at OperatorUtils ", e);
        }
        return -2;
    }

    public RmOperators VerifyCredential(String name, String password) {
        try {
            Criteria criteria = m_session.createCriteria(RmOperators.class);            
            criteria.add(Restrictions.eq("name", name));
            String securePassword = AxiomProtect.ProtectData(password);
            criteria.add(Restrictions.eq("passsword", securePassword));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                criteria = m_session.createCriteria(RmOperators.class);                
                criteria.add(Restrictions.eq("name", name));
                List list1 = criteria.list();
                if (list1 == null || list1.isEmpty() == true) {
                } else {
                    RmOperators oObj1 = (RmOperators) list1.get(0);
                    Transaction tx = null;
                    try {
                        SettingsManagement sMngt = new SettingsManagement();
                        Object obj = sMngt.getSettingInner(SettingsManagement.PASSWORD_POLICY_SETTING, SettingsManagement.PREFERENCE_ONE);
                        if (obj != null) {
                            if (obj instanceof PasswordPolicySetting) {
                                PasswordPolicySetting passObj = (PasswordPolicySetting) obj;
                                tx = m_session.beginTransaction();
                                int iAttempts = oObj1.getCurrentAttempts();
                                iAttempts = iAttempts + 1;
                                if (iAttempts > passObj.invalidAttempts) {
                                    oObj1.setCurrentAttempts(iAttempts);
                                    oObj1.setStatus(-1);
                                } else {
                                    oObj1.setCurrentAttempts(iAttempts);
                                }
                                m_session.save(oObj1);
                                tx.commit();
                                return null;
                            }
                        } else {
                            tx = m_session.beginTransaction();
                            int iAttempts = oObj1.getCurrentAttempts();
                            iAttempts = iAttempts + 1;
                            if (iAttempts > 4) {
                                oObj1.setCurrentAttempts(iAttempts);
                                oObj1.setStatus(-1);
                                oObj1.setLoginflag(OperatorsManagement.LOGIN_ACTIVE);
                            } else {
                                oObj1.setCurrentAttempts(iAttempts);
                            }
                            m_session.save(oObj1);
                            tx.commit();
                            return null;
                        }
                    } catch (Exception e) {
                        logger.error("Exception at OperatorUtils ", e);
                    }
                }
                return null;
            } else {
                RmOperators oObj1 = (RmOperators) list.get(0);
                Transaction tx = null;
                try {
                    tx = m_session.beginTransaction();
                    oObj1.setCurrentAttempts(0);
                    m_session.save(oObj1);
                    tx.commit();
                } catch (Exception e) {
                    logger.error("Exception at OperatorUtils ", e);
                }
                RmOperators oObj = (RmOperators) list.get(0);
                String pass = oObj.getPasssword();
                oObj.setPasssword(AxiomProtect.AccessData(pass));
                return oObj;
            }
        } catch (Exception e) {
            logger.error("Exception at OperatorUtils ", e);
        }
        return null;
    }

    public RmOperators GetOperatorsByname(String name) {
        try {
            Criteria criteria = m_session.createCriteria(RmOperators.class);            
            criteria.add(Restrictions.eq("name", name));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmOperators oObj = (RmOperators) list.get(0);
                String password = oObj.getPasssword();
                oObj.setPasssword(AxiomProtect.AccessData(password));
                return oObj;
            }
        } catch (Exception e) {
            logger.error("Exception at OperatorUtils ", e);
        }
        return null;
    }

    public int getOperatorCount() {
        try {
            Criteria criteria = m_session.createCriteria(RmOperators.class);            
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return 0;
            } else {
                int count = list.size();
                return count;
            }
        } catch (Exception e) {
            logger.error("Exception at OperatorUtils ", e);
        }
        return 0;
    }

    public RmOperators checkOperatorEmail(String emailID) {
        try {
            Criteria criteria = m_session.createCriteria(RmOperators.class);
            criteria.add(Restrictions.eq("emailid", emailID));            
            RmOperators Oper = null;
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                Oper = (RmOperators) list.get(0);
                return Oper;
            }
        } catch (Exception e) {
            logger.error("Exception at OperatorUtils ", e);
        }
        return null;
    }

    public RmOperators checkOperatorName(String name) {
        try {
            Criteria criteria = m_session.createCriteria(RmOperators.class);
            criteria.add(Restrictions.eq("name", name));            
            List list = criteria.list();
            RmOperators oper = null;
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                oper = (RmOperators) list.get(0);
                return oper;
            }
        } catch (Exception e) {
            logger.error("Exception at OperatorUtils ", e);
        }
        return null;
    }

    public RmOperators checkOperatorPhone(String phone) {
        try {
            Criteria criteria = m_session.createCriteria(RmOperators.class);
            criteria.add(Restrictions.eq("phone", phone));            
            RmOperators oper = new RmOperators();
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                oper = (RmOperators) list.get(0);
                return oper;
            }
        } catch (Exception e) {
            logger.error("Exception at OperatorUtils ", e);
        }
        return null;
    }

    public RmOperators GetOperatorsById(String operatorId) {
        try {
            Criteria criteria = m_session.createCriteria(RmOperators.class);            
            criteria.add(Restrictions.eq("operatorid", operatorId));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmOperators oObj = (RmOperators) list.get(0);
                String password = oObj.getPasssword();
                oObj.setPasssword(AxiomProtect.AccessData(password));
                return oObj;
            }
        } catch (Exception e) {
            logger.error("Exception at OperatorUtils ", e);
        }
        return null;
    }

    public int changeRole(String operatorId, int roleId) {
        try {
            RmOperators oObj = new RmOperators();
            Criteria criteria = m_session.createCriteria(RmOperators.class);
            criteria.add(Restrictions.eq("operatorid", operatorId));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return -2;
            } else {
                oObj = (RmOperators) list.get(0);
                oObj.setRoleid(roleId);
                Transaction tx = null;
                try {
                    tx = m_session.beginTransaction();
                    m_session.save(oObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    logger.error("Exception at OperatorUtils ", e);
                }
            }
        } catch (Exception e) {
            logger.error("Exception at OperatorUtils ", e);
        }
        return -2;
    }

    public int changePassword(String operatorid, String password) {
        try {
            RmOperators oObj = new RmOperators();
            Criteria criteria = m_session.createCriteria(RmOperators.class);
            criteria.add(Restrictions.eq("operatorid", operatorid));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return -2;
            } else {
                oObj = (RmOperators) list.get(0);
                oObj.setPasssword(AxiomProtect.ProtectData(password));
                oObj.setLastAccessOn(new Date());
                oObj.setPasswordupdatedOn(new Date());
                oObj.setCurrentAttempts(0);
                Transaction tx = null;
                try {
                    tx = m_session.beginTransaction();
                    m_session.save(oObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    logger.error("Exception at OperatorUtils ", e);
                }
            }
        } catch (Exception e) {
            logger.error("Exception at OperatorUtils ", e);
        }
        return -2;
    }

    public int checkOperatorInChannel(int type, String value) {
        try {
            Criteria criteria = m_session.createCriteria(RmOperators.class);            
            if (type == 1) {
                criteria.add(Restrictions.eq("name", value));
            } else if (type == 2) {
                criteria.add(Restrictions.eq("emailid", value));
            } else if (type == 3) {
                criteria.add(Restrictions.eq("phone", value));
            }
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return 0;
            } else {
                return -1;
            }
        } catch (Exception e) {
            logger.error("Exception at OperatorUtils ", e);
        }
        return -1;
    }

    public int UnlockOperatorPassword(String operatorid) {
        try {
            RmOperators oObj = new RmOperators();
            Criteria criteria = m_session.createCriteria(RmOperators.class);            
            criteria.add(Restrictions.eq("operatorid", operatorid));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return -2;
            } else {
                oObj = (RmOperators) list.get(0);
                Transaction tx = null;
                try {
                    oObj.setStatus(1);
                    oObj.setCurrentAttempts(0);
                    oObj.setLastAccessOn(new Date());
                    tx = m_session.beginTransaction();
                    m_session.save(oObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    logger.error("Exception at OperatorUtils ", e);
                }
            }
        } catch (Exception e) {
            logger.error("Exception at OperatorUtils ", e);
        }
        return -2;
    }

    public RmOperators GetByName(String name) {
        try {
            Criteria criteria = m_session.createCriteria(RmOperators.class);            
            criteria.add(Restrictions.eq("name", name));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmOperators oObj = (RmOperators) list.get(0);
                String password = oObj.getPasssword();
                oObj.setPasssword(AxiomProtect.AccessData(password));
                return oObj;
            }
        } catch (Exception e) {
            logger.error("Exception at OperatorUtils ", e);
        }
        return null;
    }

    public RmOperators GetAdminOperators(int roleid) {
        try {
            Criteria criteria = m_session.createCriteria(RmOperators.class);            
            criteria.add(Restrictions.eq("roleid", roleid));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmOperators Operators = new RmOperators();
                Operators = (RmOperators) list.get(0);
                String password = Operators.getPasssword();
                Operators.setPasssword(AxiomProtect.AccessData(password));
                return Operators;
            }
        } catch (Exception e) {
            logger.error("Exception at OperatorUtils ", e);
        }
        return null;
    }

    public int changeOperatorsPassword(String operatorId, RmOperators operators) {
        try {
            RmOperators oObj = new RmOperators();
            Criteria criteria = m_session.createCriteria(RmOperators.class);
            criteria.add(Restrictions.eq("operatorid", operatorId));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return -2;
            } else if (operators != null) {
                oObj = (RmOperators) list.get(0);                
                oObj.setCurrentAttempts(operators.getCurrentAttempts());
                oObj.setEmailid(operators.getEmailid());
                oObj.setLastAccessOn(operators.getLastAccessOn());
                oObj.setName(operators.getName());
                String securePassword = AxiomProtect.ProtectData(operators.getPasssword());
                oObj.setPasssword(securePassword);
                oObj.setPhone(operators.getPhone());
                oObj.setRoleid(operators.getRoleid());
                oObj.setStatus(operators.getStatus());
                oObj.setPasswordupdatedOn(new Date());
                oObj.setChangePassword(GlobalStatus.ACTIVE);
                Transaction tx = null;
                try {
                    tx = m_session.beginTransaction();
                    m_session.save(oObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    logger.error("Exception at OperatorUtils ", e);
                }
            } else {
                return -1;
            }
        } catch (Exception e) {
            logger.error("Exception at OperatorUtils ", e);
        }
        return -2;
    }

    public RmOperators[] getNewAllOperators() {
        try {
            Criteria criteria = m_session.createCriteria(RmOperators.class);            
            criteria.add(Restrictions.ne("status", GlobalStatus.DELETED));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmOperators[] Operators = new RmOperators[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    Operators[i] = (RmOperators) list.get(i);
                    String password = Operators[i].getPasssword();
                    Operators[i].setPasssword(AxiomProtect.AccessData(password));
                }
                return Operators;
            }
        } catch (Exception e) {
            logger.error("Exception at OperatorUtils ", e);
        }
        return null;
    }

    public RmOperators[] GetByRoleID(int roleId) {
        try {
            Criteria criteria = m_session.createCriteria(RmOperators.class);            
            criteria.add(Restrictions.eq("roleid", roleId));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmOperators[] operatyorList = new RmOperators[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    operatyorList[i] = (RmOperators) list.get(i);
                }
                return operatyorList;
            }
        } catch (Exception e) {
            logger.error("Exception at OperatorUtils ", e);
        }
        return null;
    }

    public RmOperators initializeResetPassword(String name, String email, String randomString, Date expiryTime) {
        try {
            Criteria criteria = m_session.createCriteria(RmOperators.class);            
            criteria.add(Restrictions.eq("name", name));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmOperators oObj1 = (RmOperators) list.get(0);
                if (expiryTime != null) {
                    oObj1.setRandomString(randomString + "," + randomString(8));
                    oObj1.setLinkExpiry(expiryTime);
                    Transaction tx = null;
                    try {
                        tx = m_session.beginTransaction();
                        oObj1.setCurrentAttempts(0);
                        m_session.save(oObj1);
                        tx.commit();
                    } catch (Exception e) {
                        logger.error("Exception at OperatorUtils ", e);
                    }
                }
                return oObj1;
            }
        } catch (Exception e) {
            logger.error("Exception at OperatorUtils ", e);
        }
        return null;
    }

    public RmOperators resetPassword(String name, String email, String randomString, String password) {
        try {
            Criteria criteria = m_session.createCriteria(RmOperators.class);            
            criteria.add(Restrictions.eq("name", name));
            criteria.add(Restrictions.eq("randomString", randomString));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmOperators oObj1 = (RmOperators) list.get(0);
                if (oObj1.getLinkExpiry().before(new Date())) {
                    return null;
                }
                oObj1.setPasssword(AxiomProtect.ProtectData(password));
                oObj1.setChangePassword(GlobalStatus.CHANGE_PASSWORD_STATUS);
                oObj1.setStatus(GlobalStatus.ACTIVE);
                oObj1.setCurrentAttempts(0);
                Transaction tx = null;
                try {
                    tx = m_session.beginTransaction();
                    oObj1.setCurrentAttempts(0);
                    m_session.save(oObj1);
                    tx.commit();
                } catch (Exception e) {
                    logger.error("Exception at OperatorUtils ", e);
                }
                return oObj1;
            }
        } catch (Exception e) {
            logger.error("Exception at OperatorUtils ", e);
        }
        return null;
    }

    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZqwertyuiopasdfghjklzxcvbnm";

    static Random rnd = new Random();

    String randomString(int len) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        }
        return sb.toString();
    }
    
    public int changeOperators(String operatorId, RmOperators operators) {
        try {
            RmOperators oObj = new RmOperators();
            Criteria criteria = m_session.createCriteria(RmOperators.class);
            criteria.add(Restrictions.eq("operatorid", operatorId));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return -2;
            } else if (operators != null) {
                oObj = (RmOperators) list.get(0);                
                oObj.setCurrentAttempts(operators.getCurrentAttempts());
                oObj.setEmailid(operators.getEmailid());
                oObj.setLastAccessOn(operators.getLastAccessOn());
                oObj.setName(operators.getName());
                String securePassword = AxiomProtect.ProtectData(operators.getPasssword());
                oObj.setPasssword(securePassword);
                oObj.setPhone(operators.getPhone());
                oObj.setRoleid(operators.getRoleid());
                oObj.setStatus(operators.getStatus());
                Transaction tx = null;
                try {
                    tx = m_session.beginTransaction();
                    m_session.save(oObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    logger.error("Exception at OperatorUtils ", e);
                }
            } else {
                return -1;
            }
        } catch (Exception e) {
            logger.error("Exception at OperatorUtils ", e);
        }
        return -2;
    }
}
