/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector;

import com.mollatech.rewardme.nucleus.commons.GlobalStatus;
import com.mollatech.rewardme.nucleus.db.RmCampaigndetails;
import java.io.Serializable;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author abhishekingle
 */
public class CampaignUtils {
    
    private SessionFactoryUtil m_su;

    private Session m_session;

    static final Logger logger = Logger.getLogger(CampaignUtils.class);

    public CampaignUtils(SessionFactoryUtil su, Session session) {
        m_su = su;
        m_session = session;
    }

    public int addCampaignDetails(RmCampaigndetails campaignObj) {
        Transaction tx = null;
        try {
            tx = m_session.beginTransaction();
            Serializable s = m_session.save(campaignObj);
            tx.commit();
            return (Integer) s;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception at CampaignUtils ", e);
            return -1;
        }
    }        
    
    public RmCampaigndetails getCampaignById(int campaignid) {
        RmCampaigndetails pdetails;
        try {
            Criteria criteria = m_session.createCriteria(RmCampaigndetails.class);
            criteria.add(Restrictions.eq("campaignId", campaignid));
            List list = criteria.list();
            if (list.isEmpty() == true) {
                return null;
            } else {
                pdetails = (RmCampaigndetails) list.get(0);
                return pdetails;
            }
        } catch (Exception e) {
            logger.error("Exception at CampaignUtils ", e);
        }
        return null;
    }
    
    public RmCampaigndetails getCampaignByUniqueId(String campaignUniqueId) {
        RmCampaigndetails pdetails;
        try {
            Criteria criteria = m_session.createCriteria(RmCampaigndetails.class);
            criteria.add(Restrictions.eq("campaignUniqueId", campaignUniqueId));
            List list = criteria.list();
            if (list.isEmpty() == true) {
                return null;
            } else {
                pdetails = (RmCampaigndetails) list.get(0);
                return pdetails;
            }
        } catch (Exception e) {
            logger.error("Exception at CampaignUtils ", e);
        }
        return null;
    }
    
    public int updateDetails(RmCampaigndetails campaignDetails) {
        try {
            Transaction tx = m_session.beginTransaction();
            m_session.update(campaignDetails);
            tx.commit();
            return 0;
        } catch (Exception ex) {
            logger.error("Exception at CampaignUtils ", ex);
            return -1;
        }
    }        
    
    public RmCampaigndetails[] getAllCampaignDetails() {
        RmCampaigndetails[] pObj = null;
        Criteria criteria = m_session.createCriteria(RmCampaigndetails.class);        
        criteria.add(Restrictions.ne("status", GlobalStatus.DELETED));
        List list = criteria.list();
        if (list.isEmpty() != true) {
            try {
                pObj = new RmCampaigndetails[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    pObj[i] = (RmCampaigndetails) list.get(i);
                }
                return pObj;
            } catch (Exception e) {
                logger.error("Exception at CampaignUtils ", e);
                return null;
            }
        }
        return null;
    }
    
    public RmCampaigndetails[] getCampaignDetailsByOwnerId(int ownerId) {
        RmCampaigndetails[] pObj = null;
        Criteria criteria = m_session.createCriteria(RmCampaigndetails.class);        
        //criteria.add(Restrictions.ne("status", GlobalStatus.DELETED));
        criteria.add(Restrictions.eq("ownerId", ownerId));
        List list = criteria.list();
        if (list.isEmpty() != true) {
            try {
                pObj = new RmCampaigndetails[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    pObj[i] = (RmCampaigndetails) list.get(i);
                }
                return pObj;
            } catch (Exception e) {
                logger.error("Exception at CampaignUtils ", e);
                return null;
            }
        }
        return null;
    }
    
    public RmCampaigndetails[] getAllCampaignBySocialMediaUniqueId(String socialMediaUniqueId) {
        RmCampaigndetails[] pObj = null;
        Criteria criteria = m_session.createCriteria(RmCampaigndetails.class);                
        criteria.add(Restrictions.eq("twitterSocialSetting", socialMediaUniqueId));
        criteria.add(Restrictions.eq("status", GlobalStatus.START_PROCESS));        
        List list = criteria.list();
        if (list.isEmpty() != true) {
            try {
                pObj = new RmCampaigndetails[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    pObj[i] = (RmCampaigndetails) list.get(i);
                }
                return pObj;
            } catch (Exception e) {
                logger.error("Exception at CampaignUtils ", e);
                return null;
            }
        }
        return null;
    }
}
