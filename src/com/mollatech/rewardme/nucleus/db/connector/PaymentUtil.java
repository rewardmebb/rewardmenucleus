/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector;

import com.mollatech.rewardme.nucleus.db.RmPaymentdetails;
import java.io.Serializable;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author abhishekingle
 */
public class PaymentUtil {
    
    private SessionFactoryUtil m_su;

    private Session m_session;

    static final Logger logger = Logger.getLogger(PaymentUtil.class);

    public PaymentUtil(SessionFactoryUtil su, Session session) {
        m_su = su;
        m_session = session;
    }

    public RmPaymentdetails getPaymentDetailsbySubscribe(int subscriptionId, int ownerId) {
        RmPaymentdetails pdetails;
        try {
            Criteria criteria = m_session.createCriteria(RmPaymentdetails.class);
            criteria.add(Restrictions.eq("subscriptionId", subscriptionId));
            criteria.add(Restrictions.eq("ownerId", ownerId));
            List list = criteria.list();
            if (list.isEmpty() == true) {
                return null;
            } else {
                pdetails = (RmPaymentdetails) list.get(0);
                return pdetails;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception at PaymentUtil ", e);
        }
        return null;
    }

    public int addPaymentDetails(RmPaymentdetails payObj) {
        Transaction tx = null;
        try {
            tx = m_session.beginTransaction();
            Serializable s = m_session.save(payObj);
            tx.commit();
            return (Integer) s;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception at PaymentUtil ", e);
            return -1;
        }
    }

    public List<RmPaymentdetails> getSgPaymentdetailsById(int ownerId) {
        try {
            Criteria criteria = m_session.createCriteria(RmPaymentdetails.class);
            criteria.add(Restrictions.eq("ownerId", ownerId));
            List<RmPaymentdetails> list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                return list;
            }
        } catch (Exception e) {
            logger.error("Exception at PaymentUtil ", e);
        }
        return null;
    }

    public List<RmPaymentdetails> getPaymentDetailByInvoiceId(String invoiceNo) {
        try {
            Criteria criteria = m_session.createCriteria(RmPaymentdetails.class);
            if (invoiceNo != null) {
                criteria.add(Restrictions.eq("invoiceNo", invoiceNo));
            }
            List<RmPaymentdetails> list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                return list;
            }
        } catch (Exception e) {
            logger.error("Exception at PaymentUtil ", e);
        }
        return null;
    }

    public int updateDetails(RmPaymentdetails paymentObj) {
        try {
            Transaction tx = m_session.beginTransaction();
            m_session.update(paymentObj);
            tx.commit();
            return 0;
        } catch (Exception ex) {
            logger.error("Exception at PaymentUtil ", ex);
            return -1;
        }
    }

    public RmPaymentdetails getPaymentDetailsbyPaymentId(int paymentId) {
        RmPaymentdetails pdetails;
        try {
            Criteria criteria = m_session.createCriteria(RmPaymentdetails.class);
            criteria.add(Restrictions.eq("paymentId", paymentId));
            List list = criteria.list();
            if (list.isEmpty() == true) {
                return null;
            } else {
                pdetails = (RmPaymentdetails) list.get(0);
                return pdetails;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception at PaymentUtil ", e);
        }
        return null;
    }
}
