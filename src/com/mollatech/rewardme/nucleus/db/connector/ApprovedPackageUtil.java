/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector;

import com.mollatech.rewardme.nucleus.commons.GlobalStatus;
import com.mollatech.rewardme.nucleus.db.RmApprovedpackagedetails;
import com.mollatech.rewardme.nucleus.db.RmUnapprovalpackagedetails;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author abhishekingle
 */
public class ApprovedPackageUtil {
    
    private SessionFactoryUtil m_su;

    private Session m_session;

    static final Logger logger = Logger.getLogger(ApprovedPackageUtil.class);

    public ApprovedPackageUtil(SessionFactoryUtil su, Session session) {
        m_su = su;
        m_session = session;
    }

    public RmApprovedpackagedetails[] listpackage() {
        RmApprovedpackagedetails[] pdetails = null;
        try {
            Criteria criteria = m_session.createCriteria(RmApprovedpackagedetails.class);
            List list = criteria.list();
            if (list.isEmpty() == true) {
                return null;
            } else {
                pdetails = new RmApprovedpackagedetails[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    pdetails[i] = (RmApprovedpackagedetails) list.get(i);
                }
                return pdetails;
            }
        } catch (Exception e) {
            logger.error("Exception at ApprovedPackageUtil ", e);
        }
        return null;
    }

    public int addPackage(RmApprovedpackagedetails bucketObj) {
        Transaction tx = null;
        try {
            tx = m_session.beginTransaction();
            Serializable s = m_session.save(bucketObj);
            tx.commit();
            return (Integer) s;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception at ApprovedPackageUtil ", e);
            return -1;
        }
    }

    public int editReqPackage(RmApprovedpackagedetails bucketObj) {
        Transaction tx = null;
        try {
            tx = m_session.beginTransaction();
            m_session.update(bucketObj);
            tx.commit();
            return (Integer) 0;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception at ApprovedPackageUtil ", e);
            return -1;
        }
    }

    public RmApprovedpackagedetails getReqPackageByName(String packageName) {
        try {
            Criteria criteria = m_session.createCriteria(RmApprovedpackagedetails.class);            
            criteria.add(Restrictions.eq("packageName", packageName));
            criteria.add(Restrictions.ne("status", GlobalStatus.DELETED));
            List list = criteria.list();
            if (list != null && !list.isEmpty()) {
                RmApprovedpackagedetails packageObj = (RmApprovedpackagedetails) list.get(0);
                return packageObj;
            } else {
                return null;
            }
        } catch (Exception ex) {
            logger.error("Exception at ApprovedPackageUtil ", ex);
            return null;
        }
    }

    public RmApprovedpackagedetails[] listBucketRequestsbystatus(int status, int secStatus) {
        try {
            Criteria criteria = m_session.createCriteria(RmApprovedpackagedetails.class);
            Disjunction or = Restrictions.disjunction();
            or.add(Restrictions.eq("status", status));
            or.add(Restrictions.eq("status", secStatus));
            criteria.add(or);
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmApprovedpackagedetails[] packageList = new RmApprovedpackagedetails[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    packageList[i] = (RmApprovedpackagedetails) list.get(i);
                }
                return packageList;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception at ApprovedPackageUtil ", e);
        }
        return null;
    }

    public RmApprovedpackagedetails[] getBucketRequestsbystatus(int status) {
        try {
            Criteria criteria = m_session.createCriteria(RmApprovedpackagedetails.class);
            criteria.add(Restrictions.eq("status", status));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmApprovedpackagedetails[] SgAdPackageList = new RmApprovedpackagedetails[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    SgAdPackageList[i] = (RmApprovedpackagedetails) list.get(i);
                }
                return SgAdPackageList;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception at ApprovedPackageUtil ", e);
        }
        return null;
    }
    
    public RmApprovedpackagedetails getBucketRequestsbyStripePlanId(String planId) {
        try {
            Criteria criteria = m_session.createCriteria(RmApprovedpackagedetails.class);
            criteria.add(Restrictions.eq("recurrenceBillingPlanId", planId));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmApprovedpackagedetails SgBucketRequestList = new RmApprovedpackagedetails();
                for (int i = 0; i < list.size(); i++) {
                    SgBucketRequestList = (RmApprovedpackagedetails) list.get(0);
                }
                return SgBucketRequestList;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception at ApprovedPackageUtil ", e);
        }
        return null;
    }

    public int changeRequestStatus(String packageName, int status) {
        try {
            Criteria criteria = m_session.createCriteria(RmApprovedpackagedetails.class);
            criteria.add(Restrictions.eq("packageName", packageName));
            List list = criteria.list();
            Criteria crt = m_session.createCriteria(RmUnapprovalpackagedetails.class);
            crt.add(Restrictions.eq("packageName", packageName));            
            List listPack = crt.list();
            if (list == null || list.isEmpty() == true || listPack == null || listPack.isEmpty()) {
                return -2;
            } else {
                RmApprovedpackagedetails rObj = (RmApprovedpackagedetails) list.get(0);
                RmUnapprovalpackagedetails packObj = (RmUnapprovalpackagedetails) listPack.get(0);
                if (status == rObj.getStatus() || rObj.getMakerflag() == GlobalStatus.ACTIVE) {
                    return 3;
                }
                Transaction tx = null;
                try {
                    rObj.setStatus(status);
                    packObj.setStatus(status);
                    tx = m_session.beginTransaction();
                    m_session.save(rObj);
                    m_session.save(packObj);
                    if (!tx.wasCommitted()) {
                        tx.commit();
                    }
                    return 0;
                } catch (Exception e) {
                    e.printStackTrace();
                    tx.rollback();
                }
                return -1;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception at ApprovedPackageUtil ", e);
        }
        return -1;
    }

    public int rejectPackage(String packageName, int status, String reason) {
        try {
            Criteria criteria = m_session.createCriteria(RmApprovedpackagedetails.class);
            criteria.add(Restrictions.eq("packageName", packageName));            
            List list = criteria.list();
            Criteria crt = m_session.createCriteria(RmUnapprovalpackagedetails.class);
            crt.add(Restrictions.eq("packageName", packageName));            
            List listPack = crt.list();
            if (list == null || list.isEmpty() == true || listPack == null || listPack.isEmpty()) {
                return -2;
            } else {
                RmApprovedpackagedetails rObj = (RmApprovedpackagedetails) list.get(0);
                RmUnapprovalpackagedetails packObj = (RmUnapprovalpackagedetails) listPack.get(0);
                if (status == rObj.getStatus()) {
                    return 3;
                }
                Transaction tx = null;
                try {
                    rObj.setStatus(status);
                    rObj.setMakerflag(GlobalStatus.SUSPEND);
                    rObj.setUpdationDate(new Date());
                    packObj.setStatus(status);
                    packObj.setReasonForRejection(reason);
                    packObj.setUpdationDate(new Date());
                    tx = m_session.beginTransaction();
                    m_session.save(rObj);
                    m_session.save(packObj);
                    if (!tx.wasCommitted()) {
                        tx.commit();
                    }
                    return 0;
                } catch (Exception e) {
                    e.printStackTrace();
                    tx.rollback();
                }
                return -1;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception at ApprovedPackageUtil ", e);
        }
        return -1;
    }

    public int updateDetails(RmApprovedpackagedetails bucketObj) {
        Transaction tx = null;
        try {
            tx = m_session.beginTransaction();
            m_session.update(bucketObj);
            tx.commit();
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            tx.rollback();
        }
        return -1;
    }

    public RmApprovedpackagedetails getPackageByDefault() {
        try {
            Criteria criteria = m_session.createCriteria(RmApprovedpackagedetails.class);            
            criteria.add(Restrictions.eq("markAsDefault", GlobalStatus.DEFAULT));
            criteria.add(Restrictions.ne("status", GlobalStatus.DELETED));
            List list = criteria.list();
            if (list != null && !list.isEmpty()) {
                RmApprovedpackagedetails packageObj = (RmApprovedpackagedetails) list.get(0);
                return packageObj;
            } else {
                return null;
            }
        } catch (Exception ex) {
            logger.error("Exception at ApprovedPackageUtil ", ex);
            return null;
        }
    }
}
