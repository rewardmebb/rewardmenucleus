/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector;

import com.mollatech.rewardme.nucleus.db.RmRoles;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author abhishekingle
 */
public class RolesUtils {
 
    private SessionFactoryUtil m_su;

    private Session m_session;

    static final Logger logger = Logger.getLogger(RolesUtils.class);

    public RolesUtils(SessionFactoryUtil su, Session session) {
        m_su = su;
        m_session = session;
    }

    public int addRole(String name, Date createdOn, Date lastAccessOn) {
        RmRoles rObj = new RmRoles();        
        rObj.setName(name);
        rObj.setCreatedOn(createdOn);
        rObj.setLastAccessOn(lastAccessOn);
        Transaction tx = null;
        try {
            tx = m_session.beginTransaction();
            m_session.save(rObj);
            tx.commit();
            return 0;
        } catch (Exception e) {
            logger.error("Exception at ResourceUtil ", e);
        }
        return -1;
    }

    public RmRoles getRole(int roleId) {
        try {
            Criteria criteria = m_session.createCriteria(RmRoles.class);
            criteria.add(Restrictions.eq("roleid", roleId));            
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmRoles rObj = (RmRoles) list.get(0);
                return rObj;
            }
        } catch (Exception e) {
            logger.error("Exception at ResourceUtil ", e);
        }
        return null;
    }

    public int changeRole(int roleId, RmRoles role) {
        try {
            RmRoles rObj = new RmRoles();
            Criteria criteria = m_session.createCriteria(RmRoles.class);            
            criteria.add(Restrictions.eq("roleid", roleId));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return -2;
            } else if (role != null) {
                rObj = (RmRoles) list.get(0);
                rObj = role;
                Transaction tx = null;
                try {
                    tx = m_session.beginTransaction();
                    m_session.save(rObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    logger.error("Exception at ResourceUtil ", e);
                }
            } else {
                return -1;
            }
        } catch (Exception e) {
            logger.error("Exception at ResourceUtil ", e);
        }
        return -2;
    }

    public RmRoles[] getRoles() {
        try {
            Criteria criteria = m_session.createCriteria(RmRoles.class);            
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmRoles[] objectList = new RmRoles[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    objectList[i] = (RmRoles) list.get(i);
                }
                return objectList;
            }
        } catch (Exception e) {
            logger.error("Exception at ResourceUtil ", e);
        }
        return null;
    }

    public int getCount() {
        try {
            Criteria criteria = m_session.createCriteria(RmRoles.class);
            Integer totalResult = ((Number) criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
            return totalResult;
        } catch (Exception e) {
            logger.error("Exception at ResourceUtil ", e);
            return -2;
        }
    }
    
    public RmRoles getRoleById(String channelId, int roleID) {
        try {
            Criteria criteria = m_session.createCriteria(RmRoles.class);            
            criteria.add(Restrictions.eq("roleid", roleID));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmRoles rObj = (RmRoles) list.get(0);
                return rObj;
            }
        } catch (Exception e) {
            logger.error("Exception at ResourceUtil ", e);
        }
        return null;
    }

    public int updateLastAccessOn(int roleId, Date lastAccessOnDate) {
        try {
            RmRoles rObj = new RmRoles();
            Criteria criteria = m_session.createCriteria(RmRoles.class);
            criteria.add(Restrictions.eq("roleid", roleId));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return -2;
            } else if (lastAccessOnDate != null) {
                rObj = (RmRoles) list.get(0);
                rObj.setLastAccessOn(lastAccessOnDate);
                Transaction tx = null;
                try {
                    tx = m_session.beginTransaction();
                    m_session.save(rObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    logger.error("Exception at ResourceUtil ", e);
                }
            } else {
                return -1;
            }
        } catch (Exception e) {
            logger.error("Exception at ResourceUtil ", e);
        }
        return -2;
    }

    public RmRoles getRoleByName(String name) {
        try {
            Criteria criteria = m_session.createCriteria(RmRoles.class);
            criteria.add(Restrictions.eq("name", name));            
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmRoles rObj = (RmRoles) list.get(0);
                return rObj;
            }
        } catch (Exception e) {
            logger.error("Exception at ResourceUtil ", e);
        }
        return null;
    }
}
