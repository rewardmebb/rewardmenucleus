/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector;

import com.mollatech.rewardme.nucleus.db.RmCreditinfo;
import java.io.Serializable;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author abhishekingle
 */
public class CreditUtils {
    
    private SessionFactoryUtil m_su;

    private Session m_session;

    static final Logger logger = Logger.getLogger(CreditUtils.class);
    
    int count =0;
    
    public CreditUtils(SessionFactoryUtil su, Session session) {
        m_su = su;
        m_session = session;
    }
            
    public int addDetails(RmCreditinfo creditInfo) {
        if (count >= 5) {
            return -1;
        }
        Transaction tx = null;
        try {
            tx = m_session.beginTransaction();
            Serializable s = m_session.save(creditInfo);
            tx.commit();
            return (Integer) s;
        }catch (org.hibernate.exception.ConstraintViolationException e) {
            count++;
            e.printStackTrace();
            return addDetails(creditInfo);
        } catch (Exception e) {
            logger.error("Exception at CreditUtils ", e);
            e.printStackTrace();
            return -1;
        }
    }

    public RmCreditinfo getDetailsByOwnerId(int ownerId) {        
        RmCreditinfo gdetails = null;
        try {
            Criteria criteria = m_session.createCriteria(RmCreditinfo.class);
            criteria.add(Restrictions.eq("ownerId", ownerId));
            List list = criteria.list();
            if (list != null && !list.isEmpty()) {
                gdetails = (RmCreditinfo) list.get(0);
                return gdetails;
            } else {
                return null;
            }
        } catch (Exception e) {
            logger.error("Exception at CreditUtils ", e);
        }
        return null;
    }

    public int updateDetails(RmCreditinfo creditInfo) {
        
            Transaction tx = null;
            try {
                tx = m_session.beginTransaction();
                m_session.update(creditInfo);                
                tx.commit();
                return 0;
            } catch (HibernateException exception) {
                exception.printStackTrace();
                updateDetails(creditInfo);
            } catch (Exception e) {
                e.printStackTrace();
                logger.error("Exception at CreditUtils ", e);
            }
            return -1;        
    }
}
