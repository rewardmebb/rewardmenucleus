package com.mollatech.rewardme.nucleus.db.connector;

import com.mollatech.service.nucleus.crypto.AES;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import java.io.File;
import java.util.Properties;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class SessionFactory {

    static final Logger logger = Logger.getLogger(SessionFactory.class);

    public static Properties stat_Properties = null;

    public SessionFactory() {
        logger.setLevel(Level.ALL);
        Properties properties = new Properties();
        properties.setProperty("solr.log", LoadSettings.g_strPath.replace("rewardme-settings", "") + "RewardMeLogs" + File.separator);
        properties.setProperty("log4j.appender.file", "org.apache.log4j.DailyRollingFileAppender");
        properties.setProperty("log4j.appender.file.File", "${solr.log}RewardMe.log");
        properties.setProperty("log4j.appender.file.DatePattern", "'.'yyyy-MM-dd");
        properties.setProperty("log4j.appender.file.layout.ConversionPattern", "[%-5p] %d{dd-MMM-yy hh-mm-ss} %c %M - %m%n");
        properties.setProperty("log4j.appender.file.layout", "org.apache.log4j.PatternLayout");
        properties.setProperty("log4j.rootLogger", LoadSettings.g_sSettings.getProperty("logging.level") + ", file");
        PropertyConfigurator.configure(properties);
        try {
            if (stat_Properties == null) {
                Properties p = new Properties();
                AES aesObj = new AES();
                String host = LoadSettings.g_sSettings.getProperty("db.host");
                host = aesObj.PINDecrypt(host, AES.getSignature());
                String port = LoadSettings.g_sSettings.getProperty("db.port");
                port = aesObj.PINDecrypt(port, AES.getSignature());
                String database = LoadSettings.g_sSettings.getProperty("db.database");
                database = aesObj.PINDecrypt(database, AES.getSignature());
                String username = LoadSettings.g_sSettings.getProperty("db.username");
                username = aesObj.PINDecrypt(username, AES.getSignature());

                String passwd = LoadSettings.g_sSettings.getProperty("db.password");
                passwd = aesObj.PINDecrypt(passwd, AES.getSignature());
                String dbType = LoadSettings.g_sSettings.getProperty("db.type");
                dbType = aesObj.PINDecrypt(dbType, AES.getSignature());
                if (passwd == null) {
                    passwd = "";
                }
                if (dbType.equalsIgnoreCase("mysql")) {
                    //p.put("hibernate.connection.datasource", "java:/comp/env/jdbc/mainDB");
                    p.put("hibernate.connection.driver_class", "com.mysql.jdbc.Driver");
                    p.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
                    database = "rewardme";
                    host = "localhost";
                    p.put("hibernate.connection.url", "jdbc:mysql://"
                            + host + ":"
                            + port + "/"
                            + database
                            + "?autoReconnect=true");
                } else if (dbType.equalsIgnoreCase("oracle")) {
                    p.put("hibernate.connection.url", "jdbc:oracle:thin:@" + host + ":" + port + ":" + database);
                    p.put("hibernate.connection.datasource", "java:/comp/env/jdbc/mainDB");
                    p.put("hibernate.connection.driver_class", "oracle.jdbc.OracleDriver");
                    p.put("hibernate.dialect", "org.hibernate.dialect.OracleDialect");
                }
                p.put("hibernate.connection.username", username);
                p.put("hibernate.connection.password", passwd);
//                p.put("hibernate.connection.provider_class", "org.hibernate.connection.C3P0ConnectionProvider");
//                p.put("hibernate.c3p0.min_size", "5");
//                p.put("hibernate.c3p0.max_size", "10");
//                p.put("hibernate.c3p0.timeout", "100");
//                p.put("hibernate.c3p0.idle_test_period", "1000");
//                p.put("hibernate.c3p0.max_statements", "100");
//                p.put("hibernate.c3p0.validate", "true");
//                p.put("hibernate.bytecode.use_reflection_optimizer", "false");

                p.put("hibernate.hbm2ddl.auto", "update");
                p.put("hibernate.transaction.factory_class", "org.hibernate.transaction.JDBCTransactionFactory");
                p.put("hibernate.current_session_context_class", "thread");
                p.put("hibernate.show_sql", "false");
                p.put("hibernate.connection.autocommit", "true");

                stat_Properties = p;
            }
        } catch (Exception e) {
            logger.error("Exception at SessionFactory ", e);
        }
    }
}
