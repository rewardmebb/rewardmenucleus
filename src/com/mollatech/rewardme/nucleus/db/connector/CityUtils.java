/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector;

import com.mollatech.rewardme.nucleus.db.RmCities;
import com.mollatech.rewardme.nucleus.db.RmStates;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author abhishekingle
 */
public class CityUtils {
    
    private SessionFactoryUtil m_su;

    private Session m_session;

    static final Logger logger = Logger.getLogger(CityUtils.class);

    public CityUtils(SessionFactoryUtil su, Session session) {
        m_su = su;
        m_session = session;
    }
    
    public RmCities getCityById(int cityId) {
        RmCities pdetails;
        try {
            Criteria criteria = m_session.createCriteria(RmCities.class);
            criteria.add(Restrictions.eq("id", cityId));
            List list = criteria.list();
            if (list.isEmpty() == true) {
                return null;
            } else {
                pdetails = (RmCities) list.get(0);
                return pdetails;
            }
        } catch (Exception e) {
            logger.error("Exception at StateUtils ", e);
        }
        return null;
    }
    
}
