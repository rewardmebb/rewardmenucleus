/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.mollatech.rewardme.nucleus.db.RmChannellogs;
import com.mollatech.service.nucleus.crypto.AxiomProtect;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author abhishekingle
 */
public class ChannelLogsUtils {
 
    private final String[] aREPORTHEADERCOL = { "Sr.No", "Channel Id", "Message Id", "Type", "Delivered To", "Status", "Sent On", "Update On" };

    private String filePath;

    private Document document = null;

    private SessionFactoryUtil m_su;

    private Session m_session;

    static final Logger logger = Logger.getLogger(ChannelLogsUtils.class);

    public ChannelLogsUtils(SessionFactoryUtil su, Session session) {
        m_su = su;
        m_session = session;
    }

    public int addChannellogs(int preference, int type, RmChannellogs cLogs) {
        RmChannellogs cObj = new RmChannellogs();
        if (cLogs != null) {            
            cObj.setEmail(cLogs.getEmail());
            cObj.setErrorstatus(cLogs.getErrorstatus());
            cObj.setLastupdateUtctime(cLogs.getLastupdateUtctime());
            String message = AxiomProtect.ProtectData(cLogs.getMessage());
            cObj.setMessage(message);
            cObj.setMsgid(cLogs.getMsgid());
            cObj.setType(type);
            cObj.setPreference(preference);
            cObj.setPhone(cLogs.getPhone());
            cObj.setSentUtctime(cLogs.getSentUtctime());
            cObj.setSourceid(cLogs.getSourceid());
            cObj.setSocial(cLogs.getSocial());
            cObj.setStatus(cLogs.getStatus());
            cObj.setCost(cLogs.getCost());
            cObj.setGoogleregisterid(cLogs.getGoogleregisterid());
            cObj.setRetrycount(cLogs.getRetrycount());
            Transaction tx = null;
            try {
                tx = m_session.beginTransaction();
                m_session.save(cObj);
                tx.commit();
                return cObj.getChannellogsId().intValue();
            } catch (Exception e) {
                logger.error("Exception at ChannelLogsUtils ", e);
            }
        }
        return -1;
    }

    public int getCount() {
        try {
            Criteria criteria = m_session.createCriteria(RmChannellogs.class);
            Integer totalResult = ((Number) criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
            return totalResult;
        } catch (Exception ex) {
            logger.error("Exception at ChannelLogsUtils ", ex);
            return -2;
        }
    }

    public RmChannellogs[] getAllLogs(String channelId) {
        try {
            Criteria criteria = m_session.createCriteria(RmChannellogs.class);            
            criteria.addOrder(Order.desc("sentUtctime"));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmChannellogs[] logList = new RmChannellogs[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    logList[i] = (RmChannellogs) list.get(i);
                    logList[i].setMessage(AxiomProtect.AccessData(logList[i].getMessage()));
                }
                return logList;
            }
        } catch (Exception e) {
            logger.error("Exception at ChannelLogsUtils ", e);
        }
        return null;
    }

    public RmChannellogs[] getAllLogs(String channelId, int type, Date start, Date end) {
        try {
            Criteria criteria = m_session.createCriteria(RmChannellogs.class);            
            if (type != -1) {
                criteria.add(Restrictions.eq("type", type));
            }
            start.setHours(0);
            start.setMinutes(0);
            start.setSeconds(0);
            end.setHours(23);
            end.setMinutes(59);
            end.setSeconds(59);
            criteria.add(Restrictions.between("sentUtctime", start, end));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmChannellogs[] logList = new RmChannellogs[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    logList[i] = (RmChannellogs) list.get(i);
                    logList[i].setMessage(AxiomProtect.AccessData(logList[i].getMessage()));
                }
                return logList;
            }
        } catch (Exception e) {
            logger.error("Exception at ChannelLogsUtils ", e);
        }
        return null;
    }

    public PrintWriter createCSV(String channelName, String sdate, String edate) {
        try {
            FileWriter fw = null;
            PrintWriter out = null;
            File f = generateTempFile(".csv");
            filePath = f.getAbsolutePath();
            fw = new FileWriter(filePath);
            Font f1 = new Font();
            f1.setSize(15);
            out = new PrintWriter(fw);
            out.print("                                            Report Generated for  " + channelName + " from " + sdate + "  to " + edate + ",\n");
            out.println();
            for (int a = 0; a < 1; ++a) {
                for (int j = 0; j < aREPORTHEADERCOL.length; ++j) {
                    out.print(aREPORTHEADERCOL[j] + ",");
                }
            }
            out.println();
            return out;
        } catch (IOException ex) {
            logger.error("Exception at ChannelLogsUtils ", ex);
        }
        return null;
    }

    public String generateReport(int iReportType, RmChannellogs[] requesttrack, String channelName, String sdate, String edate) throws Exception {
        filePath = "";
        try {
            PdfPTable table = null;
            PrintWriter out = null;
            if (iReportType == 0) {
                table = createPdf(channelName, sdate, edate);
            } else if (iReportType == 1) {
                out = createCSV(channelName, sdate, edate);
            }
            Font f1 = new Font();
            f1.setSize(8);
            if (requesttrack != null) {
                for (int i = 0; i < requesttrack.length; i++) {
                    if (iReportType == 0) {
                        PdfPCell reportId = new PdfPCell(new Phrase(String.valueOf((i + 1)), f1));
                        table.addCell(reportId);
//                        PdfPCell channelid = new PdfPCell(new Phrase(String.valueOf(requesttrack[i].getChannelid()), f1));
//                        table.addCell(channelid);
                        PdfPCell APname = new PdfPCell(new Phrase(String.valueOf(requesttrack[i].getMsgid()), f1));
                        table.addCell(APname);
                        String type = "";
                        String deliveredTo = "";
                        String Status = "SENT";
                        if (requesttrack[i].getType() == 1) {
                            type = "SMS";
                            deliveredTo = requesttrack[i].getPhone();
                        } else {
                            type = "EMAIL";
                            deliveredTo = requesttrack[i].getEmail();
                        }
                        if (requesttrack[i].getStatus() != 0) {
                            Status = "NOT SENT";
                        }
                        PdfPCell grpid = new PdfPCell(new Phrase(String.valueOf(type), f1));
                        table.addCell(grpid);
                        PdfPCell resid = new PdfPCell(new Phrase(String.valueOf(deliveredTo), f1));
                        table.addCell(resid);
                        PdfPCell status = new PdfPCell(new Phrase(String.valueOf(Status), f1));
                        table.addCell(status);
                        PdfPCell pid = new PdfPCell(new Phrase(String.valueOf(requesttrack[i].getSentUtctime()), f1));
                        table.addCell(pid);
                        PdfPCell status1 = new PdfPCell(new Phrase(String.valueOf(requesttrack[i].getLastupdateUtctime()), f1));
                        table.addCell(status1);
                    } else if (iReportType == 1) {
                        String partnername;
                        out.print("\n");
                        out.print(String.valueOf((i + 1)) + ",");
                        //out.print(requesttrack[i].getChannelid() + ",");
                        out.print(requesttrack[i].getMsgid() + ",");
                        String type = "";
                        String deliveredTo = "";
                        String Status = "SENT";
                        if (requesttrack[i].getType() == 1) {
                            type = "SMS";
                            deliveredTo = requesttrack[i].getPhone();
                        } else {
                            type = "EMAIL";
                            deliveredTo = requesttrack[i].getEmail();
                        }
                        if (requesttrack[i].getStatus() != 0) {
                            Status = "NOT SENT";
                        }
                        out.print(type + ",");
                        out.print(deliveredTo + ",");
                        out.print(Status + ",");
                        out.print(String.valueOf(requesttrack[i].getSentUtctime()) + ",");
                        out.print(String.valueOf(requesttrack[i].getLastupdateUtctime()) + ",");
                        out.println();
                        out.flush();
                    }
                }
            } else if (requesttrack == null) {
                if (iReportType == 0) {
                    PdfPCell cell = new PdfPCell(new Phrase("No Records Found"));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(13);
                    table.addCell(cell);
                } else if (iReportType == 1) {
                    out.println();
                    out.print("                                                No Record Found");
                    out.println();
                    out.flush();
                }
            }
            if (iReportType == 0) {
                Date date1 = new Date();
                SimpleDateFormat sd1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                String da1 = sd1.format(date1);
                PdfPCell cell1 = new PdfPCell(new Phrase("Date " + da1));
                cell1.setBackgroundColor(BaseColor.GRAY);
                cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell1.setColspan(16);
                table.addCell(cell1);
                table.setSkipLastFooter(true);
                String author = LoadSettings.g_sSettings.getProperty("report.author");
                String title = LoadSettings.g_sSettings.getProperty("report.title");
                document.addAuthor(author);
                document.addTitle(title);
                document.add(table);
                document.close();
            } else if (iReportType == 1) {
                Date date1 = new Date();
                SimpleDateFormat sd1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                String da1 = sd1.format(date1);
                out.print("                                                               Date  :" + da1);
                out.println();
                out.flush();
            } else {
                Date date1 = new Date();
                SimpleDateFormat sd1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                String da1 = sd1.format(date1);
                out.print("                                                               Date  :" + da1);
                out.println();
                out.flush();
            }
            if (out != null) {
                out.close();
            }
        } catch (Exception e) {
            logger.error("Exception at ChannelLogsUtils ", e);
        }
        if (document != null) {
            if (document.isOpen()) {
                document.close();
            }
        }
        return filePath;
    }

    private File generateTempFile(String strExtension) throws IOException {
        return File.createTempFile("serviceguard", strExtension);
    }

    private PdfPTable createPdf(String channelName, String sdate, String edate) throws DocumentException, FileNotFoundException, IOException {
        document = new Document(PageSize.A4.rotate());
        File file = generateTempFile(".pdf");
        filePath = generateTempFile(".pdf").getAbsolutePath();
        FileOutputStream fout = new FileOutputStream(filePath);
        PdfWriter.getInstance(document, fout);
        document.open();
        PdfPTable table = new PdfPTable(new float[] { 3, 3, 3, 3, 3, 3, 3, 3 });
        table.setWidthPercentage(100f);
        table.getDefaultCell().setUseAscender(true);
        table.getDefaultCell().setUseDescender(true);
        Font f = new Font();
        f.setSize(10);
        f.setColor(BaseColor.BLACK);
        Date date = new Date();
        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String da = sd.format(date);
        PdfPCell cell = new PdfPCell(new Phrase("Report Generated for  channel " + channelName + " from  " + sdate + " to " + edate, f));
        cell.setBackgroundColor(BaseColor.BLUE);
        cell.setBackgroundColor(BaseColor.GRAY);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setColspan(16);
        table.addCell(cell);
        Font f1 = new Font();
        f1.setSize(5);
        table.getDefaultCell().setBackgroundColor(BaseColor.LIGHT_GRAY);
        for (int i = 0; i < 2; ++i) {
            if (i == 0) {
                for (int j = 0; j < aREPORTHEADERCOL.length; ++j) {
                    cell.setColspan(2);
                    table.addCell(aREPORTHEADERCOL[j]);
                }
            } else {
                for (int j = 0; j < aREPORTHEADERCOL.length; ++j) {
                    cell.setColspan(2);
                    table.addCell("");
                }
            }
        }
        table.getDefaultCell().setBackgroundColor(null);
        table.setHeaderRows(3);
        table.setFooterRows(1);
        return table;
    }
}
