/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector;

import com.mollatech.rewardme.nucleus.commons.GlobalStatus;
import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
import com.mollatech.rewardme.nucleus.db.RmSocialmediadetails;
import java.io.Serializable;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author abhishekingle
 */
public class SocialMediaUtils {
    
    private SessionFactoryUtil m_su;

    private Session m_session;

    static final Logger logger = Logger.getLogger(SocialMediaUtils.class);

    public SocialMediaUtils(SessionFactoryUtil su, Session session) {
        m_su = su;
        m_session = session;
    }

    public int addSocialMediaDetails(RmSocialmediadetails socialMediaObj) {
        Transaction tx = null;
        try {
            tx = m_session.beginTransaction();
            Serializable s = m_session.save(socialMediaObj);
            tx.commit();
            return (Integer) s;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception at SocialMediaUtils ", e);
            return -1;
        }
    }
            
    public RmSocialmediadetails getSocailMediaDetailsbyId(int socialMediaid) {
        RmSocialmediadetails pdetails;
        try {
            Criteria criteria = m_session.createCriteria(RmSocialmediadetails.class);
            criteria.add(Restrictions.eq("socialmediaId", socialMediaid));
            List list = criteria.list();
            if (list.isEmpty() == true) {
                return null;
            } else {
                pdetails = (RmSocialmediadetails) list.get(0);
                return pdetails;
            }
        } catch (Exception e) {
            logger.error("Exception at SocialMediaUtils ", e);
        }
        return null;
    }
    
    public RmSocialmediadetails[] getSocailMediaDetailsbyOwnerId(int ownerId) {
        RmSocialmediadetails[] pdetails;
        try {
            Criteria criteria = m_session.createCriteria(RmSocialmediadetails.class);            
            criteria.add(Restrictions.eq("ownerId", ownerId));
            List list = criteria.list();
            if (list.isEmpty() == true) {
                return null;
            } else {
                pdetails = new RmSocialmediadetails[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    pdetails[i] = (RmSocialmediadetails) list.get(i);
                }
                return pdetails;
            }
        } catch (Exception e) {
            logger.error("Exception at SocialMediaUtils ", e);
        }
        return null;
    }
    
    public RmSocialmediadetails getSocialMediaDetailsByHandlerName(String twitterHandlerName) {
        RmSocialmediadetails pdetails;
        try {
            Criteria criteria = m_session.createCriteria(RmSocialmediadetails.class);            
            criteria.add(Restrictions.eq("twitterHandlerName", twitterHandlerName));
            List list = criteria.list();
            if (list.isEmpty() == true) {
                return null;
            } else {
                pdetails = (RmSocialmediadetails) list.get(0);
                return pdetails;
            }
        } catch (Exception e) {
            logger.error("Exception at SocialMediaUtils ", e);
        }
        return null;
    }
    
    public RmSocialmediadetails getSocialMediaDetailsByUniqueId(String uniqueId) {
        RmSocialmediadetails pdetails;
        try {
            Criteria criteria = m_session.createCriteria(RmSocialmediadetails.class);            
            criteria.add(Restrictions.eq("uniqueSocialMediaId", uniqueId));
            List list = criteria.list();
            if (list.isEmpty() == true) {
                return null;
            } else {
                pdetails = (RmSocialmediadetails) list.get(0);
                return pdetails;
            }
        } catch (Exception e) {
            logger.error("Exception at SocialMediaUtils ", e);
        }
        return null;
    }
    
    public int updateDetails(RmSocialmediadetails socialMediaDetails) {
        try {
            Transaction tx = m_session.beginTransaction();
            m_session.update(socialMediaDetails);
            tx.commit();
            return 0;
        } catch (Exception ex) {
            logger.error("Exception at SocialMediaUtils ", ex);
            return -1;
        }
    }
            
    public RmSocialmediadetails[] getAllSocailMediaDetails() {
        RmSocialmediadetails[] pObj = null;
        Criteria criteria = m_session.createCriteria(RmSocialmediadetails.class);        
        criteria.add(Restrictions.ne("status", GlobalStatus.DELETED));
        List list = criteria.list();
        if (list.isEmpty() != true) {
            try {
                pObj = new RmSocialmediadetails[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    pObj[i] = (RmSocialmediadetails) list.get(i);
                }
                return pObj;
            } catch (Exception e) {
                logger.error("Exception at SocialMediaUtils ", e);
                return null;
            }
        }
        return null;
    }
}
