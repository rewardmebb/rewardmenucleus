/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector;

import com.mollatech.rewardme.nucleus.db.RmSettings;
import com.mollatech.service.nucleus.crypto.AxiomProtect;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author abhishekingle
 */
public class SettingsUtil {
    
    private SessionFactoryUtil m_su;

    private Session m_session;

    public static final int SMS = 1;
  
    public static final int Email = 2;
   
    public SettingsUtil(SessionFactoryUtil su, Session session) {
        m_su = su;
        m_session = session;
    }

    static final Logger logger = Logger.getLogger(SettingsUtil.class);

    public int addSetting( int iType, int iPreference, Object objSetting) {
        RmSettings sObj = new RmSettings();        
        sObj.setType(iType);
        sObj.setPreference(iPreference);
        sObj.setCreatedOn(new Date());
        byte[] settings = AxiomProtect.ProtectDataBytes(serializeToObject(objSetting));
        sObj.setSettingentry(settings);
        sObj.setLastUpdateOn(new Date());
        Transaction tx = null;
        try {
            tx = m_session.beginTransaction();
            m_session.save(sObj);
            tx.commit();
            return 0;
        } catch (Exception e) {
            logger.error("Exception at SessionUtil ", e);
        }
        return -1;
    }

    public Object getSetting(int iType, int iPreference) {
        Object object = new Object();
        try {
            Criteria criteria = m_session.createCriteria(RmSettings.class);            
            criteria.add(Restrictions.eq("type", iType));
            criteria.add(Restrictions.eq("preference", iPreference));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmSettings sObj = (RmSettings) list.get(0);
                byte[] settings = sObj.getSettingentry();
                ByteArrayInputStream bais = new ByteArrayInputStream(AxiomProtect.AccessDataBytes(settings));
                object = deserializeFromObject(bais);
                return object;
            }
        } catch (Exception e) {
            logger.error("Exception at SessionUtil ", e);
        }
        return null;
    }

   

    public int changeSetting(int iType, int iPreference, Object objOldSetting, Object objNewSetting) {
        try {
            RmSettings sObj = new RmSettings();
            Criteria criteria = m_session.createCriteria(RmSettings.class);            
            criteria.add(Restrictions.eq("type", iType));
            criteria.add(Restrictions.eq("preference", iPreference));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return -2;
            } else if (objNewSetting != null) {
                sObj = (RmSettings) list.get(0);
                byte[] settings = AxiomProtect.ProtectDataBytes(serializeToObject(objNewSetting));
                sObj.setSettingentry(settings);
                Transaction tx = null;
                try {
                    tx = m_session.beginTransaction();
                    m_session.save(sObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    logger.error("Exception at SessionUtil ", e);
                }
            } else {
                return -1;
            }
        } catch (Exception e) {
            logger.error("Exception at SessionUtil ", e);
        }
        return -2;
    }
    
    public Object[] getSettings() {
        Object object = null;
        try {
            Criteria criteria = m_session.createCriteria(RmSettings.class);
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                Object[] objectList = new Object[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    RmSettings sObj = (RmSettings) list.get(i);
                    byte[] settings = sObj.getSettingentry();
                    ByteArrayInputStream bais = new ByteArrayInputStream(AxiomProtect.AccessDataBytes(settings));
                    object = deserializeFromObject(bais);
                    objectList[i] = object;
                }
                return objectList;
            }
        } catch (Exception e) {
            logger.error("Exception at SessionUtil ", e);
        }
        return null;
    }

    public int getCount() {
        Object object = null;
        try {
            Criteria criteria = m_session.createCriteria(RmSettings.class);
            Integer totalResult = ((Number) criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
            return totalResult;
        } catch (Exception e) {
            logger.error("Exception at SessionUtil ", e);
            return -2;
        }
    }

    public static byte[] serializeToObject(Object objectToSerialize) {
        ObjectOutputStream oos = null;
        byte[] data = null;
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(bos);
            oos.writeObject(objectToSerialize);
            oos.flush();
            oos.close();
            bos.close();
            data = bos.toByteArray();
            return data;
        } catch (IOException ex) {
            logger.error("Exception at SessionUtil ", ex);
        } finally {
            try {
                oos.close();
            } catch (IOException ex) {
                logger.error("Exception at SessionUtil ", ex);
            }
        }
        return data;
    }

    public static Object deserializeFromObject(ByteArrayInputStream binaryObject) {
        ObjectInputStream ins;
        try {
            ins = new ObjectInputStream(binaryObject);
            Object object = (Object) ins.readObject();
            ins.close();
            return object;
        } catch (Exception e) {
            logger.error("Exception at SessionUtil ", e);
        }
        return null;
    }

    public Object[] getSettingsByTypes(int iType) {
        Object object = new Object();
        try {
            Criteria criteria = m_session.createCriteria(RmSettings.class);            
            criteria.add(Restrictions.eq("type", iType));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                Object[] objectList = new Object[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    RmSettings sObj = (RmSettings) list.get(i);
                    byte[] settings = sObj.getSettingentry();
                    ByteArrayInputStream bais = new ByteArrayInputStream(AxiomProtect.AccessDataBytes(settings));
                    object = deserializeFromObject(bais);
                    objectList[i] = object;
                }
                return objectList;
            }
        } catch (Exception e) {
            logger.error("Exception at SessionUtil ", e);
        }
        return null;
    }
}
