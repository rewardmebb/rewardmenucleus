/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector.management;

import com.mollatech.rewardme.nucleus.db.RmSubscriptionpackagedetails;
import com.mollatech.rewardme.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.rewardme.nucleus.db.connector.SessionUtil;
import com.mollatech.rewardme.nucleus.db.connector.SubscriptionUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author abhishekingle
 */
public class SubscriptionManagement {
    
    static final Logger logger = Logger.getLogger(SubscriptionManagement.class);

    public static final int ACTIVE_STATUS = 1;    
    
    public int CreateSubscriptionDetails(String sessionId, RmSubscriptionpackagedetails bucketObj) {
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.subscriptionPackageDetails);
        Session pSession = puSession.openSession();
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        SubscriptionUtils prtUtil = new SubscriptionUtils(puSession, pSession);
        int result = -1;
        try {
            if (res == ACTIVE_STATUS) {
                int update = sManagement.UpdateSession(sessionId);
                result = prtUtil.addSubscriptionDetails(bucketObj);
                return result;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Exception at SubscriptionManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
            sSession.close();
            suSession.close();
        }
        return result;
    }
    
    public int updateDetails(RmSubscriptionpackagedetails subObj) {
        SessionFactoryUtil suUser = new SessionFactoryUtil(SessionFactoryUtil.subscriptionPackageDetails);
        Session sUser = suUser.openSession();
        try {
            SubscriptionUtils oUtil = new SubscriptionUtils(suUser, sUser);
            int result = oUtil.updateDetails(subObj);
            return result;
        } catch (Exception ex) {
            logger.error("Exception at SubscriptionManagement ", ex);
            return -2;
        } finally {
            sUser.close();
            suUser.close();
        }
    }
    
     public RmSubscriptionpackagedetails getSubscriptionbyOwnerId(int ownerId) {
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.subscriptionPackageDetails);
        Session pSession = puSession.openSession();
        SubscriptionUtils partUtil = new SubscriptionUtils(puSession, pSession);
        int result = -1;
        RmSubscriptionpackagedetails pdetails = null;
        try {
            pdetails = partUtil.getSubscriptionbyOwnerId(ownerId);
            return pdetails;
        } catch (Exception ex) {
            logger.error("Exception at SubscriptionManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
        }
        return null;
    }
    
    public RmSubscriptionpackagedetails getSubscriptionbySubscriptionId(int subscriptionId) {
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.subscriptionPackageDetails);
        Session pSession = puSession.openSession();
        SubscriptionUtils partUtil = new SubscriptionUtils(puSession, pSession);
        int result = -1;
        RmSubscriptionpackagedetails pdetails = null;
        try {
            pdetails = partUtil.getSubscriptionbySubscriptionId(subscriptionId);
            return pdetails;
        } catch (Exception ex) {
            logger.error("Exception at SubscriptionManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
        }
        return null;
    } 
     
    public RmSubscriptionpackagedetails[] listOfPackageSubscripedbyOwnerId(int ownerId) {
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.subscriptionPackageDetails);
        Session pSession = puSession.openSession();
        SubscriptionUtils partUtil = new SubscriptionUtils(puSession, pSession);
        int result = -1;
        RmSubscriptionpackagedetails[] pdetails = null;
        try {
            pdetails = partUtil.listOfPackageSubscripedbyOwnerId(ownerId);
            return pdetails;
        } catch (Exception ex) {
            logger.error("Exception at SubscriptionManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
        }
        return null;
    }
}
