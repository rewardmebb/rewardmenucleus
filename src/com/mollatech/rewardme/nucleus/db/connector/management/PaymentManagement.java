/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector.management;

import com.mollatech.rewardme.nucleus.db.RmPaymentdetails;
import com.mollatech.rewardme.nucleus.db.connector.PaymentUtil;
import com.mollatech.rewardme.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.rewardme.nucleus.db.connector.SessionUtil;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author abhishekingle
 */
public class PaymentManagement {
    
    static final Logger logger = Logger.getLogger(PaymentManagement.class);

    public static final int ACTIVE_STATUS = 1;

    public static final int SUSPEND_STATUS = 0;

    public RmPaymentdetails getPaymentDetailsbyBrandOwnerAndSubscriptionID(int subId, int ownerId) {
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.paymentDetails);
        Session pSession = puSession.openSession();
        PaymentUtil partUtil = new PaymentUtil(puSession, pSession);
        RmPaymentdetails pdetails = null;
        try {
            pdetails = partUtil.getPaymentDetailsbySubscribe(subId, ownerId);
            return pdetails;
        } catch (Exception ex) {
            logger.error("Exception at PaymentManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
        }
        return null;
    }

    public int createPaymentDetails(String sessionId, RmPaymentdetails payObj) {
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.paymentDetails);
        Session pSession = puSession.openSession();
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        PaymentUtil prtUtil = new PaymentUtil(puSession, pSession);
        int result = -1;
        try {
            if (res == ACTIVE_STATUS) {
                int update = sManagement.UpdateSession(sessionId);
                result = prtUtil.addPaymentDetails(payObj);
                return result;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Exception at PaymentManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
            sSession.close();
            suSession.close();
        }
        return result;
    }
}
