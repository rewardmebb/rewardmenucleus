/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector.management;

import com.mollatech.rewardme.nucleus.db.RmRemoteaccess;
import com.mollatech.rewardme.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.rewardme.nucleus.db.connector.SessionFactoryUtil;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author abhishekingle
 */
public class RemoteAccessManagement {
    
    static final Logger logger = Logger.getLogger(RemoteAccessManagement.class);

    final int ACTIVE_STATUS = 1;

    public int enableRemoteAccess(String sessionid, boolean bEnabled) {
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionid);
        SessionFactoryUtil remoteAccess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session remote = remoteAccess.openSession();
        try {
            if (res == ACTIVE_STATUS) {
                RemoteAccessUtils Util = new RemoteAccessUtils(remoteAccess, remote);
                int result = Util.enableRemoteAccess(bEnabled);
                return result;
            } else {
                return -1;
            }
        } finally {
            remoteAccess.close();
            remote.close();
        }
    }

    public RmRemoteaccess[] getRemoteAccess() {
        SessionManagement sManagement = new SessionManagement();
        SessionFactoryUtil remoteAccess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session remote = remoteAccess.openSession();
        try {
            RemoteAccessUtils Util = new RemoteAccessUtils(remoteAccess, remote);
            RmRemoteaccess[] result = Util.getRemoteAccess();
            return result;
        } finally {
            remoteAccess.close();
            remote.close();
        }
    }

    public int getCount() {
        SessionManagement sManagement = new SessionManagement();
        SessionFactoryUtil remoteAccess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session remote = remoteAccess.openSession();
        try {
            RemoteAccessUtils Util = new RemoteAccessUtils(remoteAccess, remote);
            return Util.getCount();
        } catch (Exception ex) {
            logger.error("Exception at PasswordTrailManagement ", ex);
            return -2;
        } finally {
            remoteAccess.close();
            remote.close();
        }
    }

    public int SetRemoteAccessCredentials(String sessionid, String login, String password) {
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionid);
        SessionFactoryUtil remoteAccess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session remote = remoteAccess.openSession();
        try {
            if (res == ACTIVE_STATUS) {
                RemoteAccessUtils Util = new RemoteAccessUtils(remoteAccess, remote);
                int result = Util.SetRemoteAccessCredentials(login, password);
                return result;
            } else {
                return -1;
            }
        } finally {
            remoteAccess.close();
            remote.close();
        }
    }

    public String[] GetRemoteAccessCredentials(String sessionid) {
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionid);
        SessionFactoryUtil remoteAccess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session remote = remoteAccess.openSession();
        try {
            if (res == ACTIVE_STATUS) {
                RemoteAccessUtils Util = new RemoteAccessUtils(remoteAccess, remote);
                String arr[] = Util.GetRemoteAccessCredentials();
                return arr;
            } else {
                return null;
            }
        } finally {
            remoteAccess.close();
            remote.close();
        }
    }

    public boolean IsRemoteAccessEnabled() {
        SessionFactoryUtil remoteAccess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session remote = remoteAccess.openSession();
        try {
            RemoteAccessUtils raUtil = new RemoteAccessUtils(remoteAccess, remote);
            boolean result = raUtil.IsRemoteAccessEnabled();
            return result;
        } finally {
            remoteAccess.close();
            remote.close();
        }
    }

    public int SetRemoteAccessCredentialsInternal(String login, String password) {
        SessionFactoryUtil remoteAccess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session remote = remoteAccess.openSession();
        try {
            RemoteAccessUtils Util = new RemoteAccessUtils(remoteAccess, remote);
            int result = Util.SetRemoteAccessCredentials(login, password);
            return result;
        } finally {
            remote.close();
            remoteAccess.close();
        }
    }
}
