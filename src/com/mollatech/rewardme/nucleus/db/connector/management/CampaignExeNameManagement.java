/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector.management;

import com.mollatech.rewardme.nucleus.commons.GlobalStatus;
import com.mollatech.rewardme.nucleus.db.RmCampaignExecutionName;
import com.mollatech.rewardme.nucleus.db.connector.CampaignExeNameUtils;
import com.mollatech.rewardme.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.rewardme.nucleus.db.connector.SessionUtil;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author abhishekingle
 */
public class CampaignExeNameManagement {
    
    static final Logger logger = Logger.getLogger(CampaignManagement.class);    
    
    public int CreateCampaignExeName(String sessionId, RmCampaignExecutionName campaignObj) {
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.campaignExecutionName);
        Session pSession = puSession.openSession();
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        CampaignExeNameUtils prtUtil = new CampaignExeNameUtils(puSession, pSession);
        int result = -1;
        try {
            if (res == GlobalStatus.ACTIVE) {
                int update = sManagement.UpdateSession(sessionId);
                result = prtUtil.addCampaignExeNameDetails(campaignObj);
                return result;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Exception at CampaignExeNameManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
            sSession.close();
            suSession.close();
        }
        return result;
    }
   
    
    public RmCampaignExecutionName[] getCampaignExeNameByOwnerId(int ownerId) {
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.campaignExecutionName);
        Session pSession = puSession.openSession();
        CampaignExeNameUtils partUtil = new CampaignExeNameUtils(puSession, pSession);        
        RmCampaignExecutionName[] pdetails = null;
        try {
            pdetails = partUtil.getCampaignDetailsByOwnerId(ownerId);
            return pdetails;
        } catch (Exception ex) {
            logger.error("Exception at CampaignExeNameManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
        }
        return null;
    }
    
    public RmCampaignExecutionName getCampaignDetailsByExename(int ownerId,String exeName) {
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.campaignExecutionName);
        Session pSession = puSession.openSession();
        CampaignExeNameUtils partUtil = new CampaignExeNameUtils(puSession, pSession);        
        RmCampaignExecutionName pdetails = null;
        try {
            pdetails = partUtil.getCampaignDetailsByExename(ownerId,exeName);
            return pdetails;
        } catch (Exception ex) {
            logger.error("Exception at CampaignExeNameManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
        }
        return null;
    }
    
    public int updateDetails(RmCampaignExecutionName campaignDetails) {
        SessionFactoryUtil guSession = new SessionFactoryUtil(SessionFactoryUtil.campaignDetails);
        Session gSession = guSession.openSession();
        CampaignExeNameUtils campaignUtil = new CampaignExeNameUtils(guSession, gSession);
        int result = -1;
        try {
            result = campaignUtil.updateDetails(campaignDetails);
            return result;
        } catch (Exception ex) {
            logger.error("Exception at CampaignExeNameManagement ", ex);
        } finally {
            gSession.close();
            guSession.close();
        }
        return result;
    }
    
    public RmCampaignExecutionName[] getTxDetails(Integer ownerId, String startDate, String endDate, String stime, String etime) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.campaignExecutionName);
        Session sSession = suSession.openSession();
        try {
            RmCampaignExecutionName[] sgTranscationdetailses = new CampaignExeNameUtils(suSession, sSession).getTxDetails(ownerId, startDate, endDate, stime, etime);
            return sgTranscationdetailses;
        } catch (Exception ex) {
            logger.error("Exception at CampaignExeNameManagement ", ex);
        } finally {
            sSession.close();
            suSession.close();
        }
        return null;
    }
}
