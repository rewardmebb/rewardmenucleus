/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector.management;

import com.mollatech.rewardme.nucleus.commons.GlobalStatus;
import com.mollatech.rewardme.nucleus.db.RmSocialmediadetails;
import com.mollatech.rewardme.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.rewardme.nucleus.db.connector.SessionUtil;
import com.mollatech.rewardme.nucleus.db.connector.SocialMediaUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author abhishekingle
 */
public class SocialMediaManagement {
    
    static final Logger logger = Logger.getLogger(SocialMediaManagement.class);

    public static final int ACTIVE_STATUS = 1;    
    
    public int CreateSocialMediaDetails(String sessionId, RmSocialmediadetails socialMediaObj) {
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.socialMediaDetails);
        Session pSession = puSession.openSession();
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        SocialMediaUtils prtUtil = new SocialMediaUtils(puSession, pSession);
        int result = -1;
        try {
            if (res == ACTIVE_STATUS) {
                int update = sManagement.UpdateSession(sessionId);
                result = prtUtil.addSocialMediaDetails(socialMediaObj);
                return result;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Exception at SocialMediaManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
            sSession.close();
            suSession.close();
        }
        return result;
    }
            
    public RmSocialmediadetails getSocialMeidaDetailsById(int socialMediaid) {
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.socialMediaDetails);
        Session pSession = puSession.openSession();
        SocialMediaUtils partUtil = new SocialMediaUtils(puSession, pSession);
        int result = -1;
        RmSocialmediadetails pdetails = null;
        try {
            pdetails = partUtil.getSocailMediaDetailsbyId(socialMediaid);
            return pdetails;
        } catch (Exception ex) {
            logger.error("Exception at SocialMediaManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
        }
        return null;
    }
    
    public RmSocialmediadetails[] getSocialMediaDetailsByOwnerId(int ownerid) {
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.socialMediaDetails);
        Session pSession = puSession.openSession();
        SocialMediaUtils partUtil = new SocialMediaUtils(puSession, pSession);
        int result = -1;
        RmSocialmediadetails[] pdetails = null;
        try {
            pdetails = partUtil.getSocailMediaDetailsbyOwnerId(ownerid);
            return pdetails;
        } catch (Exception ex) {
            logger.error("Exception at SocialMediaManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
        }
        return null;
    }
    
    public RmSocialmediadetails getSocialMediaDetailsByHandlerName(String twitterHandlerName) {
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.socialMediaDetails);
        Session pSession = puSession.openSession();
        SocialMediaUtils partUtil = new SocialMediaUtils(puSession, pSession);
        int result = -1;
        RmSocialmediadetails pdetails = null;
        try {
            pdetails = partUtil.getSocialMediaDetailsByHandlerName(twitterHandlerName);
            return pdetails;
        } catch (Exception ex) {
            logger.error("Exception at SocialMediaManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
        }
        return null;
    }
    
    public RmSocialmediadetails getSocialMediaDetailsByUniqueId(String uniqueId) {
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.socialMediaDetails);
        Session pSession = puSession.openSession();
        SocialMediaUtils partUtil = new SocialMediaUtils(puSession, pSession);
        int result = -1;
        RmSocialmediadetails pdetails = null;
        try {
            pdetails = partUtil.getSocialMediaDetailsByUniqueId(uniqueId);
            return pdetails;
        } catch (Exception ex) {
            logger.error("Exception at SocialMediaManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
        }
        return null;
    }
    
    public int updateDetails(RmSocialmediadetails socialMedia) {
        SessionFactoryUtil guSession = new SessionFactoryUtil(SessionFactoryUtil.socialMediaDetails);
        Session gSession = guSession.openSession();
        SocialMediaUtils otptrail = new SocialMediaUtils(guSession, gSession);
        int result = -1;
        try {
            result = otptrail.updateDetails(socialMedia);
            return result;
        } catch (Exception ex) {
            logger.error("Exception at SocialMediaManagement ", ex);
        } finally {
            gSession.close();
            guSession.close();
        }
        return result;
    }        
    
     public RmSocialmediadetails[] getAllSocialMediaDetails(String sessionId) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.brandOwnerDetails);
        Session pSession = puSession.openSession();
        SocialMediaUtils pUtil = new SocialMediaUtils(puSession, pSession);       
        RmSocialmediadetails[] groups = null;
        try {
            if (res == GlobalStatus.ACTIVE) {
                int update = sManagement.UpdateSession(sessionId);
                groups = pUtil.getAllSocailMediaDetails();
                return groups;
            } else {
                return null;
            }
        } catch (Exception ex) {
            logger.error("Exception at SocialMediaManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
            sSession.close();
            suSession.close();
        }
        return null;
    }
}
