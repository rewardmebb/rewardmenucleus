/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector.management;

import com.mollatech.rewardme.nucleus.db.RmApprovedpackagedetails;
import com.mollatech.rewardme.nucleus.db.RmSessions;
import com.mollatech.rewardme.nucleus.db.connector.ApprovedPackageUtil;
import com.mollatech.rewardme.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.rewardme.nucleus.db.connector.SessionUtil;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author abhishekingle
 */
public class ApprovedPackageManagement {
    
    static final Logger logger = Logger.getLogger(ApprovedPackageManagement.class);

    public static final int ACTIVE_STATUS = 1;

    public static final int SUSPEND_STATUS = 0;

    public static final int REQUEST_ACTIVE_STATUS = 1;

    public static final int REQUEST_SUSPENDED_STATUS = 0;

    public RmApprovedpackagedetails[] listpackage(String sessionId) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.approvedPackageDetails);
        Session pSession = puSession.openSession();
        ApprovedPackageUtil pUtil = new ApprovedPackageUtil(puSession, pSession);
        RmApprovedpackagedetails[] listPackage = null;
        try {
            if (res == ACTIVE_STATUS) {
                int update = sManagement.UpdateSession(sessionId);
                listPackage = pUtil.listpackage();
                return listPackage;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Exception at ApprovedPackageManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
            sSession.close();
            suSession.close();
        }
        return null;
    }

    public int CreateReqPackage(String sessionId, RmApprovedpackagedetails bucketObj) {
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.approvedPackageDetails);
        Session pSession = puSession.openSession();
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        ApprovedPackageUtil prtUtil = new ApprovedPackageUtil(puSession, pSession);
        int result = -1;
        try {
            if (res == ACTIVE_STATUS) {
                int update = sManagement.UpdateSession(sessionId);
                result = prtUtil.addPackage(bucketObj);
                return result;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Exception at ApprovedPackageManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
            sSession.close();
            suSession.close();
        }
        return result;
    }

    public int editReqPackage(String sessionId, RmApprovedpackagedetails bucketObj) {
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.approvedPackageDetails);
        Session pSession = puSession.openSession();
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        ApprovedPackageUtil prtUtil = new ApprovedPackageUtil(puSession, pSession);
        int result = -1;
        try {
            if (res == ACTIVE_STATUS) {
                sManagement.UpdateSession(sessionId);
                result = prtUtil.editReqPackage(bucketObj);
                return result;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Exception at ApprovedPackageManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
            sSession.close();
            suSession.close();
        }
        return result;
    }

    public RmApprovedpackagedetails getReqPackageByName(String sessionId, String packageName) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        RmSessions sessions = sUtil.getSession(sessionId);
        SessionFactoryUtil suAccessPoint = new SessionFactoryUtil(SessionFactoryUtil.approvedPackageDetails);
        Session sAccessPoint = suAccessPoint.openSession();
        try {
            if (res == ACTIVE_STATUS) {
                int update = sManagement.UpdateSession(sessionId);
                ApprovedPackageUtil util = new ApprovedPackageUtil(suAccessPoint, sAccessPoint);
                RmApprovedpackagedetails details = util.getReqPackageByName(packageName);
                return details;
            } else {
                return null;
            }
        } catch (Exception ex) {
            logger.error("Exception at ApprovedPackageManagement ", ex);
            return null;
        } finally {
            sAccessPoint.close();
            suAccessPoint.close();
            sSession.close();
            suSession.close();
        }
    }

    public RmApprovedpackagedetails[] listPackageRequestsbystatus(String sessionId, int status, int secStatus) {
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        SessionFactoryUtil guSession = new SessionFactoryUtil(SessionFactoryUtil.approvedPackageDetails);
        Session gSession = guSession.openSession();
        ApprovedPackageUtil utilObj = new ApprovedPackageUtil(guSession, gSession);
        RmApprovedpackagedetails[] result = null;
        try {
            if (res == ACTIVE_STATUS) {
                int update = sManagement.UpdateSession(sessionId);
                result = utilObj.listBucketRequestsbystatus(status, secStatus);
                return result;
            } else {
                return null;
            }
        } catch (Exception ex) {
            logger.error("Exception at ApprovedPackageManagement ", ex);
        } finally {
            gSession.close();
            guSession.close();
        }
        return result;
    }

    public RmApprovedpackagedetails[] getPackageRequestsbyStatus(String sessionId, int status) {
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        SessionFactoryUtil guSession = new SessionFactoryUtil(SessionFactoryUtil.approvedPackageDetails);
        Session gSession = guSession.openSession();
        ApprovedPackageUtil utilObj = new ApprovedPackageUtil(guSession, gSession);
        RmApprovedpackagedetails[] result = null;
        try {            
                result = utilObj.getBucketRequestsbystatus(status);
                return result;            
        } catch (Exception ex) {
            logger.error("Exception at ApprovedPackageManagement ", ex);
        } finally {
            gSession.close();
            guSession.close();
        }
        return result;
    }

    public RmApprovedpackagedetails getBucketRequestsbyStripePlanId(String planId) {        
        SessionFactoryUtil guSession = new SessionFactoryUtil(SessionFactoryUtil.approvedPackageDetails);
        Session gSession = guSession.openSession();
        ApprovedPackageUtil utilObj = new ApprovedPackageUtil(guSession, gSession);
        RmApprovedpackagedetails result = null;
        try {            
                result = utilObj.getBucketRequestsbyStripePlanId(planId);
                return result;            
        } catch (Exception ex) {
            logger.error("Exception at ApprovedPackageManagement ", ex);
        } finally {
            gSession.close();
            guSession.close();
        }
        return result;
    }
    
    public int ChangeRequestStatus(String sessionId, String packageName, int iStatus) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionFactoryUtil suUser = new SessionFactoryUtil(SessionFactoryUtil.approvedPackageDetails);
        Session sUser = suUser.openSession();
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        try {
            if (res == ACTIVE_STATUS) {
                sManagement.UpdateSession(sessionId);
                ApprovedPackageUtil oUtil = new ApprovedPackageUtil(suUser, sUser);
                int result = oUtil.changeRequestStatus(packageName, iStatus);
                return result;
            } else {
                return -2;
            }
        } catch (Exception ex) {
            logger.error("Exception at ApprovedPackageManagement ", ex);
            return -2;
        } finally {
            sSession.close();
            suSession.close();
            sUser.close();
            suUser.close();
        }
    }

    public int RejectPackageRequest(String sessionId, String packageName, int iStatus, String reason) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionFactoryUtil suUser = new SessionFactoryUtil(SessionFactoryUtil.approvedPackageDetails);
        Session sUser = suUser.openSession();
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        try {
            if (res == ACTIVE_STATUS) {
                sManagement.UpdateSession(sessionId);
                ApprovedPackageUtil oUtil = new ApprovedPackageUtil(suUser, sUser);
                int result = oUtil.rejectPackage(packageName, iStatus, reason);
                return result;
            } else {
                return -2;
            }
        } catch (Exception ex) {
            logger.error("Exception at ApprovedPackageManagement ", ex);
            return -2;
        } finally {
            sSession.close();
            suSession.close();
            sUser.close();
            suUser.close();
        }
    }

    public int updateDetails(RmApprovedpackagedetails bucketObj) {
        SessionFactoryUtil suUser = new SessionFactoryUtil(SessionFactoryUtil.approvedPackageDetails);
        Session sUser = suUser.openSession();
        try {
            ApprovedPackageUtil oUtil = new ApprovedPackageUtil(suUser, sUser);
            int result = oUtil.updateDetails(bucketObj);
            return result;
        } catch (Exception ex) {
            logger.error("Exception at ApprovedPackageManagement ", ex);
            return -2;
        } finally {
            sUser.close();
            suUser.close();
        }
    }

    public RmApprovedpackagedetails getRequestPackageByDefault(String sessionId) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        RmSessions sessions = sUtil.getSession(sessionId);
        SessionFactoryUtil suAccessPoint = new SessionFactoryUtil(SessionFactoryUtil.approvedPackageDetails);
        Session sAccessPoint = suAccessPoint.openSession();
        try {
            if (res == ACTIVE_STATUS) {
                int update = sManagement.UpdateSession(sessionId);
                ApprovedPackageUtil util = new ApprovedPackageUtil(suAccessPoint, sAccessPoint);
                RmApprovedpackagedetails details = util.getPackageByDefault();
                return details;
            } else {
                return null;
            }
        } catch (Exception ex) {
            logger.error("Exception at ApprovedPackageManagement ", ex);
            return null;
        } finally {
            sAccessPoint.close();
            suAccessPoint.close();
            sSession.close();
            suSession.close();
        }
    }
}
