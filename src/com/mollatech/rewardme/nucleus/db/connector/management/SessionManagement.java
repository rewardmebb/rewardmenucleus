/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector.management;

import com.mollatech.rewardme.nucleus.db.RmRemoteaccess;
import com.mollatech.rewardme.nucleus.db.RmSessions;
import com.mollatech.rewardme.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.rewardme.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.rewardme.nucleus.db.connector.SessionUtil;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import java.security.MessageDigest;
import java.util.Calendar;
import java.util.Date;
import org.apache.log4j.Logger;
import org.bouncycastle.util.encoders.Base64;
import org.hibernate.Session;

/**
 *
 * @author abhishekingle
 */
public class SessionManagement {
    
    int status = 1;

    static final Logger logger = Logger.getLogger(SessionManagement.class);

    private byte[] SHA1(String message) {
        try {
            MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
            byte[] array = sha1.digest(message.getBytes());
            return array;
        } catch (Exception e) {
            logger.error("Exception at SessionManagement ", e);
            return null;
        }
    }

    public String OpenSession(String loginid, String password, String jsessionid) {
        Date d = new Date();
        String channel = loginid + d + jsessionid;
        byte[] SHA1hash = SHA1(channel);
        String sessionId = new String(Base64.encode(SHA1hash));
        SessionFactoryUtil suRemoteAccess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session sRemoteAccess = suRemoteAccess.openSession();
        RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAccess, sRemoteAccess);
        RmRemoteaccess remoteaccess = rUtil.getRemoteaccess();
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        try {
            int result = sUtil.addSession(sessionId, loginid, status, new Date(), remoteaccess.getExpiryInMins(), new Date());
            if (result == 0) {
                return sessionId;
            } else {
                return null;
            }
        }catch(Exception e){
            logger.error("Exception at SessionManagement ", e);
            return null;
        }finally {
            sSession.close();
            suSession.close();
            sRemoteAccess.close();
            suRemoteAccess.close();
        }
    }

    public int CloseSession(String sessionid) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        try {
            SessionUtil sUtil = new SessionUtil(suSession, sSession);
            RmSessions sessions = sUtil.getSession(sessionid);
            if (sessions != null) {
                if (sessions.getStatus() == 1) {
                    Date date = new Date();
                    int result = sUtil.updateStatus(sessionid, 0);
                    return result;
                }
                return -1;
            }
            return -2;
        } finally {
            sSession.close();
            suSession.close();
        }
    }

    public RmSessions[] getSession() {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        try {
            SessionUtil sUtil = new SessionUtil(suSession, sSession);
            RmSessions[] sessions = sUtil.getSessions();
            return sessions;
        } finally {
            sSession.close();
            suSession.close();
        }
    }

    public int getCount() {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        try {
            SessionUtil sUtil = new SessionUtil(suSession, sSession);
            return sUtil.getCount();
        } catch (Exception ex) {
            logger.error("Exception at ResourceManagement ", ex);
            return -2;
        } finally {
            sSession.close();
            suSession.close();
        }
    }

    public int GetSessionStatus(String sessionid) {
        if (sessionid == null) {
            return 1;
        }
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        RmSessions sessions = sUtil.getSession(sessionid);
        try {
            String sessionFlag = LoadSettings.g_sSettings.getProperty("check.session");
            if (sessions != null) {
                if (sessions.getStatus() == status) {
                    Date date = new Date();
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(sessions.getLastUpdateOn());
                    cal.add(Calendar.MINUTE, sessions.getExpiryInMins());
                    date = cal.getTime();
                    Date d = new Date();
                    if (d.getTime() >= date.getTime()) {
                        int result = -1;
                        if (sessionFlag.equals("-1")) {
                            result = sUtil.updateStatus(sessionid, 1);
                            return 1;
                        } else {
                            result = sUtil.updateStatus(sessionid, -1);
                            return result;
                        }
                    } else if (sessionFlag.equals("-1")) {
                        return 1;
                    }
                }
                return 1;
//                return sessions.getStatus();
            }
            return 1;
        } finally {
            sSession.close();
            suSession.close();
        }
    }

    public int UpdateSession(String sessionId) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        try {
            int result = sUtil.updateSession(sessionId);
            return result;
        } finally {
            sSession.close();
            suSession.close();
        }
    }

    public RmSessions getSessionById(String sessionId) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        try {
            RmSessions sessions = sUtil.getValidSession(sessionId);
            return sessions;
        } finally {
            sSession.close();
            suSession.close();
        }
    }
}
