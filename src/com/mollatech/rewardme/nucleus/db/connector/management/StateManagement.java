/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector.management;

import com.mollatech.rewardme.nucleus.db.RmCountries;
import com.mollatech.rewardme.nucleus.db.RmStates;
import com.mollatech.rewardme.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.rewardme.nucleus.db.connector.StateUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author abhishekingle
 */
public class StateManagement {
    
    static final Logger logger = Logger.getLogger(StateManagement.class);
    
    public RmStates getStateDetailsById(int stateId) {
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.state);
        Session pSession = puSession.openSession();
        StateUtils partUtil = new StateUtils(puSession, pSession);
        int result = -1;
        RmStates pdetails = null;
        try {
            pdetails = partUtil.getStateById(stateId);
            return pdetails;
        } catch (Exception ex) {
            logger.error("Exception at StateManagement", ex);
        } finally {
            pSession.close();
            puSession.close();
        }
        return null;
    }
}
