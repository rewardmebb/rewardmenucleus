/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector.management;

import com.mollatech.rewardme.nucleus.db.RmCountries;
import com.mollatech.rewardme.nucleus.db.connector.CountryUtils;
import com.mollatech.rewardme.nucleus.db.connector.SessionFactoryUtil;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author abhishekingle
 */
public class CountryManagement {
    
    static final Logger logger = Logger.getLogger(CountryManagement.class);
    
    public RmCountries getCountryDetailsById(int countryId) {
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.country);
        Session pSession = puSession.openSession();
        CountryUtils partUtil = new CountryUtils(puSession, pSession);       
        RmCountries pdetails = null;
        try {
            pdetails = partUtil.getCountryById(countryId);
            return pdetails;
        } catch (Exception ex) {
            logger.error("Exception at CountryManagement", ex);
        } finally {
            pSession.close();
            puSession.close();
        }
        return null;
    }
}
