/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector.management;

import com.mollatech.rewardme.nucleus.db.RmSessions;
import com.mollatech.rewardme.nucleus.db.RmUnapprovalpackagedetails;
import com.mollatech.rewardme.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.rewardme.nucleus.db.connector.SessionUtil;
import com.mollatech.rewardme.nucleus.db.connector.UnapprovalPackageUtil;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author abhishekingle
 */
public class UnapprovalPackageManagement {
    
    static final Logger logger = Logger.getLogger(UnapprovalPackageManagement.class);

    public static final int ACTIVE_STATUS = 1;

    public static final int SUSPEND_STATUS = 0;

    public static final int APPROVED_STATUS = 9;

    public static final int UPDATED_REQUEST_STATUS = 2;

    public static final int REJECTED_STATUS = -1;

    public static final int REQUEST_ACTIVE_STATUS = 1;

    public static final int REQUEST_SUSPENDED_STATUS = 0;
 
    
    public int CreateUnApprovalPackage(String sessionId, RmUnapprovalpackagedetails bucketObj) {
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.unApprovalPackageDetails);
        Session pSession = puSession.openSession();
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        UnapprovalPackageUtil prtUtil = new UnapprovalPackageUtil(puSession, pSession);
        int result = -1;
        try {
            if (res == ACTIVE_STATUS) {
                int update = sManagement.UpdateSession(sessionId);
                result = prtUtil.addUnApprovalPackage(bucketObj);
                return result;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Exception at UnapprovalPackageManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
            sSession.close();
            suSession.close();
        }
        return result;
    }

    public RmUnapprovalpackagedetails[] listpackage(String sessionId) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.unApprovalPackageDetails);
        Session pSession = puSession.openSession();
        UnapprovalPackageUtil pUtil = new UnapprovalPackageUtil(puSession, pSession);
        RmUnapprovalpackagedetails[] listPackage = null;
        try {
            if (res == ACTIVE_STATUS) {
                int update = sManagement.UpdateSession(sessionId);
                listPackage = pUtil.listUnapprovalPackage();
                return listPackage;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Exception at UnapprovalPackageManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
            sSession.close();
            suSession.close();
        }
        return null;
    }

    public RmUnapprovalpackagedetails getPackageDetails(String sessionId,int packageId) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.unApprovalPackageDetails);
        Session pSession = puSession.openSession();
        UnapprovalPackageUtil partUtil = new UnapprovalPackageUtil(puSession, pSession);
        int result = -1;
        RmUnapprovalpackagedetails pdetails = null;
        try {
            if (res == ACTIVE_STATUS) {
                int update = sManagement.UpdateSession(sessionId);
                pdetails = partUtil.getUnApprovalPackageById(packageId);
            }
            return pdetails;
        } catch (Exception ex) {
            logger.error("Exception at UnapprovalPackageManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
            sSession.close();
            suSession.close();
        }
        return null;
    }

    public int editPackage(String sessionId, RmUnapprovalpackagedetails details) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.unApprovalPackageDetails);
        Session pSession = puSession.openSession();
        UnapprovalPackageUtil partUtil = new UnapprovalPackageUtil(puSession, pSession);
        try {
            if (res == ACTIVE_STATUS) {
                int update = sManagement.UpdateSession(sessionId);
                res = partUtil.editUnApprovalPackage(details);
                return res;
            } else {
                return res;
            }
        } finally {
            pSession.close();
            puSession.close();
            sSession.close();
            suSession.close();
        }
    }

    public int ChangeRequestStatus(String sessionId,int userId, int iStatus) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionFactoryUtil suUser = new SessionFactoryUtil(SessionFactoryUtil.unApprovalPackageDetails);
        Session sUser = suUser.openSession();
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        try {
            if (res == ACTIVE_STATUS) {
                sManagement.UpdateSession(sessionId);
                UnapprovalPackageUtil oUtil = new UnapprovalPackageUtil(suUser, sUser);
                int result = oUtil.changeUnApprovalRequestStatus(userId, iStatus);
                return result;
            } else {
                return -2;
            }
        } catch (Exception ex) {
            logger.error("Exception at UnapprovalPackageManagement ", ex);
            return -2;
        } finally {
            sSession.close();
            suSession.close();
            sUser.close();
            suUser.close();
        }
    }

    public int ChangePackageStatus(String sessionId, int userId, int iStatus) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionFactoryUtil suUser = new SessionFactoryUtil(SessionFactoryUtil.unApprovalPackageDetails);
        Session sUser = suUser.openSession();
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        try {
            if (res == ACTIVE_STATUS) {
                sManagement.UpdateSession(sessionId);
                UnapprovalPackageUtil oUtil = new UnapprovalPackageUtil(suUser, sUser);
                int result = oUtil.changeUnApprovalPackageStatus(userId, iStatus);
                return result;
            } else {
                return -2;
            }
        } catch (Exception ex) {
            logger.error("Exception at UnapprovalPackageManagement ", ex);
            return -2;
        } finally {
            sSession.close();
            suSession.close();
            sUser.close();
            suUser.close();
        }
    }

    public RmUnapprovalpackagedetails[] listPackageRequestsbystatus(String sessionId, int status) {
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        SessionFactoryUtil guSession = new SessionFactoryUtil(SessionFactoryUtil.unApprovalPackageDetails);
        Session gSession = guSession.openSession();
        UnapprovalPackageUtil utilObj = new UnapprovalPackageUtil(guSession, gSession);
        RmUnapprovalpackagedetails[] result = null;
        try {
            if (res == ACTIVE_STATUS) {
                int update = sManagement.UpdateSession(sessionId);
                result = utilObj.listUnApprovalPackageByStatus(status);
                return result;
            } else {
                return null;
            }
        } catch (Exception ex) {
            logger.error("Exception at UnapprovalPackageManagement ", ex);
        } finally {
            gSession.close();
            guSession.close();
        }
        return result;
    }

    public RmUnapprovalpackagedetails getPackageByName(String sessionId, String packageName) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        RmSessions sessions = sUtil.getSession(sessionId);
        SessionFactoryUtil suAccessPoint = new SessionFactoryUtil(SessionFactoryUtil.unApprovalPackageDetails);
        Session sAccessPoint = suAccessPoint.openSession();
        try {
            if (res == ACTIVE_STATUS) {
                int update = sManagement.UpdateSession(sessionId);
                UnapprovalPackageUtil util = new UnapprovalPackageUtil(suAccessPoint, sAccessPoint);
                RmUnapprovalpackagedetails details = util.getUnApprovalPackageByName(packageName);
                return details;
            } else {
                return null;
            }
        } catch (Exception ex) {
            logger.error("Exception at UnapprovalPackageManagement ", ex);
            return null;
        } finally {
            sAccessPoint.close();
            suAccessPoint.close();
            sSession.close();
            suSession.close();
        }
    }

    public RmUnapprovalpackagedetails getPackageById(String sessionId, int Id) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        RmSessions sessions = sUtil.getSession(sessionId);
        SessionFactoryUtil suAccessPoint = new SessionFactoryUtil(SessionFactoryUtil.unApprovalPackageDetails);
        Session sAccessPoint = suAccessPoint.openSession();
        try {
            if (res == ACTIVE_STATUS) {
                int update = sManagement.UpdateSession(sessionId);
                UnapprovalPackageUtil util = new UnapprovalPackageUtil(suAccessPoint, sAccessPoint);
                RmUnapprovalpackagedetails details = util.getUnApprovalPackageById(Id);
                return details;
            } else {
                return null;
            }
        } catch (Exception ex) {
            logger.error("Exception at UnapprovalPackageManagement ", ex);
            return null;
        } finally {
            sAccessPoint.close();
            suAccessPoint.close();
            sSession.close();
            suSession.close();
        }
    }

    public RmUnapprovalpackagedetails getPackageByDefault(String sessionId) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        RmSessions sessions = sUtil.getSession(sessionId);
        SessionFactoryUtil suAccessPoint = new SessionFactoryUtil(SessionFactoryUtil.unApprovalPackageDetails);
        Session sAccessPoint = suAccessPoint.openSession();
        try {
            if (res == ACTIVE_STATUS) {
                int update = sManagement.UpdateSession(sessionId);
                UnapprovalPackageUtil util = new UnapprovalPackageUtil(suAccessPoint, sAccessPoint);
                RmUnapprovalpackagedetails details = util.getUnApprovalPackageByDefault();
                return details;
            } else {
                return null;
            }
        } catch (Exception ex) {
            logger.error("Exception at UnapprovalPackageManagement ", ex);
            return null;
        } finally {
            sAccessPoint.close();
            suAccessPoint.close();
            sSession.close();
            suSession.close();
        }
    }

    public int updateDetails(RmUnapprovalpackagedetails bucketObj) {
        SessionFactoryUtil suUser = new SessionFactoryUtil(SessionFactoryUtil.unApprovalPackageDetails);
        Session sUser = suUser.openSession();
        try {
            UnapprovalPackageUtil oUtil = new UnapprovalPackageUtil(suUser, sUser);
            int result = oUtil.updateUnApprovalDetails(bucketObj);
            return result;
        } catch (Exception ex) {
            logger.error("Exception at UnapprovalPackageManagement ", ex);
            return -2;
        } finally {
            sUser.close();
            suUser.close();
        }
    }

    public RmUnapprovalpackagedetails listpackageStatus(String sessionId, int bucketId) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.unApprovalPackageDetails);
        Session pSession = puSession.openSession();
        UnapprovalPackageUtil pUtil = new UnapprovalPackageUtil(puSession, pSession);
        RmUnapprovalpackagedetails listPackage = null;
        try {
            if (res == ACTIVE_STATUS) {
                int update = sManagement.UpdateSession(sessionId);
                listPackage = pUtil.unApprovalPackageStatus(bucketId);
                return listPackage;
            } else {
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Exception at UnapprovalPackageManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
            sSession.close();
            suSession.close();
        }
        return null;
    }
}
