/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector.management;

import com.mollatech.rewardme.connector.communication.RMStatus;
import com.mollatech.rewardme.nucleus.commons.GlobalStatus;
import com.mollatech.rewardme.nucleus.db.RmOperators;
import com.mollatech.rewardme.nucleus.db.RmRoles;
import com.mollatech.rewardme.nucleus.db.RmSessions;
import com.mollatech.rewardme.nucleus.db.connector.OperatorUtils;
import com.mollatech.rewardme.nucleus.db.connector.RolesUtils;
import com.mollatech.rewardme.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.rewardme.nucleus.db.connector.SessionUtil;
import com.mollatech.rewardme.nucleus.db.operation.REWARDMEOperator;
import com.mollatech.rewardme.nucleus.settings.SendNotification;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.bouncycastle.util.encoders.Base64;
import org.hibernate.Session;

/**
 *
 * @author abhishekingle
 */
public class OperatorsManagement {
 
    static final Logger logger = Logger.getLogger(OperatorsManagement.class);
    
    public static final int LOGIN_INACTIVE = -1;

    public static final int LOGIN_ACTIVE = 1;

    public static final int EMAIL = 4;

    public static final int SMS = 1;

    public static final int VOICE = 3;

    public static final int USSD = 2;

    public static final String channels = "channels";

    public static final String operators = "operators";

    public static final String userpins = "userpins";

    public static final String otptokens = "otptokens";

    public static final String pkitokens = "pkitokens";

    public static final String settings = "settings";

    public static final String configurations = "configurations";

    public static final String reports = "reports";

    public static final String licenses = "licenses";

    public static final String sysadmin = "sysadmin";

    public static final String admin = "admin";

    public static final String helpdesk = "helpdesk";

    public static final String reporter = "reporter";

    private byte[] SHA1(String message) {
        try {
            MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
            byte[] array = sha1.digest(message.getBytes());
            return array;
        } catch (Exception e) {
            logger.error("Exception at OperatorsManagement ", e);
            return null;
        }
    }

    public int AddOperator(String sessionId, String name, String password, String phone, String emailid, int role, int status) {
        Date d = new Date();
        byte[] SHA1hash = SHA1(name + password + emailid + phone + d.toString());
        String operatorId = new String(Base64.encode(SHA1hash));
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        RmSessions sessions = sUtil.getSession(sessionId);
        SessionFactoryUtil suOperator = new SessionFactoryUtil(SessionFactoryUtil.operators);
        Session sOperator = suOperator.openSession();
        try {
            if (res == GlobalStatus.ACTIVE) {
                int update = sManagement.UpdateSession(sessionId);
                OperatorUtils oUtil = new OperatorUtils(suOperator, sOperator);
                int result = oUtil.addOperator(operatorId, name, phone, emailid, password, role, status, 0, new Date(), new Date());
                return result;
            } else {
                return -2;
            }
        } finally {
            sOperator.close();
            suOperator.close();
            sSession.close();
            suSession.close();
        }
    }

    public RmOperators GetOperatorInner(String sessionId, String name) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionFactoryUtil suOperator = new SessionFactoryUtil(SessionFactoryUtil.operators);
        Session sOperator = suOperator.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        RmSessions sessions = sUtil.getSession(sessionId);
        try {
            if (res == GlobalStatus.ACTIVE) {
                int update = sManagement.UpdateSession(sessionId);
                OperatorUtils oUtil = new OperatorUtils(suOperator, sOperator);
                RmOperators oprObj = oUtil.GetOperatorsByname(name);
                return oprObj;
            } else {
                return null;
            }
        } finally {
            sSession.close();
            suSession.close();
            sOperator.close();
            suOperator.close();
        }
    }

    public int ChangeStatus(String sessionId, String operatorid, int iStatus) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionFactoryUtil suOperator = new SessionFactoryUtil(SessionFactoryUtil.operators);
        Session sOperator = suOperator.openSession();
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        try {
            if (res == GlobalStatus.ACTIVE) {
                sManagement.UpdateSession(sessionId);
                OperatorUtils oUtil = new OperatorUtils(suOperator, sOperator);
                int result = oUtil.changeOperatorStatus(operatorid, iStatus);
                return result;
            } else {
                return -2;
            }
        } finally {
            sSession.close();
            suSession.close();
            sOperator.close();
            suOperator.close();
        }
    }

    public int EditOperator(String sessionId,String operatorid, RmOperators oldOperator, RmOperators newOperator) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionFactoryUtil suOperator = new SessionFactoryUtil(SessionFactoryUtil.operators);
        Session sOperator = suOperator.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        RmSessions sessions = sUtil.getSession(sessionId);
        try {
            RmOperators op = checkIsUniqueInOperators(newOperator.getOperatorid(), newOperator.getName(), newOperator.getEmailid(), newOperator.getPhone());
            if (op == null) {
            } else {
                return -4;
            }
            if (sessions.getStatus() == GlobalStatus.ACTIVE) {
                OperatorUtils oUtil = new OperatorUtils(suOperator, sOperator);
                int result = oUtil.changeOperators(operatorid, newOperator);
                return result;
            } else {
                return -2;
            }
        } finally {
            sSession.close();
            suSession.close();
            sOperator.close();
            suOperator.close();
        }
    }

    public RmOperators VerifyCredential(String sessionId, String name, String password) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionFactoryUtil suOperator = new SessionFactoryUtil(SessionFactoryUtil.operators);
        Session sOperator = suOperator.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        RmSessions sessions = sUtil.getSession(sessionId);
        try {
            if (res == GlobalStatus.ACTIVE) {
                int update = sManagement.UpdateSession(sessionId);
                OperatorUtils oUtil = new OperatorUtils(suOperator, sOperator);
                RmOperators operators = oUtil.VerifyCredential(name, password);
                return operators;
            }
            return null;
        } finally {
            sSession.close();
            suSession.close();
            sOperator.close();
            suOperator.close();
        }
    }

    public int SetPassword(String sessionId, String operatorId, String password, boolean bSend) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionFactoryUtil suOperator = new SessionFactoryUtil(SessionFactoryUtil.operators);
        Session sOperator = suOperator.openSession();
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        String msgid = null;
        String email = null;
        byte[] SHA1hash = null;
        try {
            RmOperators operator = null;
            if (res == GlobalStatus.ACTIVE) {
                sManagement.UpdateSession(sessionId);
                OperatorUtils oUtil = new OperatorUtils(suOperator, sOperator);
                operator = oUtil.getOperator(operatorId);
                if (bSend == true) {
                    email = operator.getEmailid();
                    oUtil.changePassword(operatorId, password);
                    String message = LoadSettings.g_templateSettings.getProperty("email.operator.password");
                    String subject = LoadSettings.g_subjecttemplateSettings.getProperty("email.operator.password.subject");                    
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    if (message != null) {
                        Date d = new Date();
                        message = message.replaceAll("#name#", operator.getName());
                        
                        message = message.replaceAll("#email#", operator.getEmailid());
                        message = message.replaceAll("#datetime#", sdf.format(d));
                        message = message.replaceAll("#password#", password);
                    }
                    if (subject != null) {
                        Date d = new Date();                        
                        subject = subject.replaceAll("#datetime#", sdf.format(d));
                    }
                    int status = -1;
                    try {
                        SendNotification notification = new SendNotification();
                        RMStatus axStatus1 = notification.SendEmail(email, subject, message, null, null, null, null, 1);
                        status = axStatus1.iStatus;
                    } catch (Exception ex) {
                        logger.error("Exception at OperatorsManagement ", ex);
                    }
                    if (status != 0) {
                        return status;
                    }
                    return 0;
                } else if (bSend == false) {
                    if ((oUtil.changePassword(operatorId, password)) != 0) {
                        return -1;
                    }
                    return 0;
                }
            }
            return -1;
        } finally {
            sSession.close();
            suSession.close();
            sOperator.close();
            suOperator.close();
        }
    }

    public int ResendPassword(String sessionId, String operatorId, boolean bSend) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionFactoryUtil suOperator = new SessionFactoryUtil(SessionFactoryUtil.operators);
        Session sOperator = suOperator.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        String msgid = null;
        RmSessions sessions = sUtil.getSession(sessionId);
        String email = null;
        String password = null;
        RmOperators operator = null;
        try {
            if (res == GlobalStatus.ACTIVE) {
                int update = sManagement.UpdateSession(sessionId);
                OperatorUtils oUtil = new OperatorUtils(suOperator, sOperator);
                operator = oUtil.getOperator(operatorId);
                if (bSend == true) {
                    email = operator.getEmailid();
                    password = operator.getPasssword();
                    String message = LoadSettings.g_templateSettings.getProperty("email.operator.password");
                    String subject = LoadSettings.g_subjecttemplateSettings.getProperty("email.operator.password.subject");                                        
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    if (message != null) {
                        Date d = new Date();
                        message = message.replaceAll("#name#", operator.getName());                       
                        message = message.replaceAll("#email#", operator.getEmailid());
                        message = message.replaceAll("#datetime#", sdf.format(d));
                        message = message.replaceAll("#password#", password);
                    }
                    if (subject != null) {
                        Date d = new Date();                        
                        subject = subject.replaceAll("#datetime#", sdf.format(d));
                    }
                    int status = -1;
                    try {
                        SendNotification notification = new SendNotification();
                        RMStatus axStatus1 = notification.SendEmail(email, subject, message, null, null, null, null, 1);
                        status = axStatus1.iStatus;
                    } catch (Exception ex) {
                        logger.error("Exception at OperatorsManagement ", ex);
                    }
                    if (status != 0) {
                        return status;
                    }
                    return 0;
                } else if (bSend == false) {
                    return -1;
                }
            }
            return -1;
        } finally {
            sSession.close();
            suSession.close();
            sOperator.close();
            suOperator.close();
        }
    }

    public int ChangeLoginStatus(String operatorid, int loginFlag) {
        SessionFactoryUtil suOperator = new SessionFactoryUtil(SessionFactoryUtil.operators);
        Session sOperator = suOperator.openSession();
        try {
            OperatorUtils oUtil = new OperatorUtils(suOperator, sOperator);
            int result = oUtil.ChangeLoginStatus(operatorid, loginFlag);
            return result;
        } finally {
            sOperator.close();
            suOperator.close();
        }
    }

    public String GetAuditTrail(String sessionId, String operatorId, int iDuration, Date startDate, int iFileType) {
        return null;
    }

    public int CreateRoles(String channelId) {
        SessionFactoryUtil suRoles = new SessionFactoryUtil(SessionFactoryUtil.roles);
        Session sRoles = suRoles.openSession();
        int roleId;
        RolesUtils roUtil = new RolesUtils(suRoles, sRoles);
        try {
            roUtil.addRole(sysadmin, new Date(), new Date());
            roUtil.addRole(helpdesk, new Date(), new Date());
        } finally {
            suRoles.close();
            sRoles.close();
        }
        return 0;
    }

    public int CreateRolesAll(String channelId) {
        SessionFactoryUtil suRoles = new SessionFactoryUtil(SessionFactoryUtil.roles);
        Session sRoles = suRoles.openSession();
        int roleId;
        RolesUtils roUtil = new RolesUtils(suRoles, sRoles);
        try {
            roUtil.addRole(sysadmin, new Date(), new Date());
            roUtil.addRole(helpdesk, new Date(), new Date());
        } finally {
            suRoles.close();
            sRoles.close();
        }
        return 0;
    }
    
    public RmRoles[] getAllRoles() {
        SessionFactoryUtil suRoles = new SessionFactoryUtil(SessionFactoryUtil.roles);
        Session sRoles = suRoles.openSession();
        RolesUtils roUtil = new RolesUtils(suRoles, sRoles);
        try {
            return roUtil.getRoles();
        } finally {
            suRoles.close();
            sRoles.close();
        }
    }

    public int getCount() {
        SessionFactoryUtil suRoles = new SessionFactoryUtil(SessionFactoryUtil.roles);
        Session sRoles = suRoles.openSession();
        RolesUtils roUtil = new RolesUtils(suRoles, sRoles);
        try {
            return roUtil.getCount();
        } catch (Exception ex) {
            logger.error("Exception at OperatorsManagement ", ex);
            return -2;
        } finally {
            suRoles.close();
            sRoles.close();
        }
    }

    public int getOperatorsCount() {
        SessionFactoryUtil suRoles = new SessionFactoryUtil(SessionFactoryUtil.operators);
        Session sRoles = suRoles.openSession();
        OperatorUtils roUtil = new OperatorUtils(suRoles, sRoles);
        try {
            return roUtil.getCount();
        } catch (Exception ex) {
            logger.error("Exception at OperatorsManagement ", ex);
            return -2;
        } finally {
            suRoles.close();
            sRoles.close();
        }
    }

    public RmRoles getRoleByRoleId(String channelId, int roleId) {
        SessionFactoryUtil suRoles = new SessionFactoryUtil(SessionFactoryUtil.roles);
        Session sRoles = suRoles.openSession();
        RolesUtils roUtil = new RolesUtils(suRoles, sRoles);
        try {
            return roUtil.getRoleById(channelId, roleId);
        } finally {
            suRoles.close();
            sRoles.close();
        }
    }

    public int checkIsUnique(String name, String emailid, String phone) {
        SessionFactoryUtil suOperator = new SessionFactoryUtil(SessionFactoryUtil.operators);
        Session sOperator = suOperator.openSession();
        OperatorUtils oUtil = new OperatorUtils(suOperator, sOperator);
        try {
            RmOperators check = oUtil.checkOperatorName(name);
            if (check == null) {
                check = oUtil.checkOperatorEmail(emailid);
            }
            if (check == null) {
                check = oUtil.checkOperatorPhone(phone);
            }
            if (check == null) {
                return 0;
            } else {
                return -1;
            }
        } finally {
            sOperator.close();
            suOperator.close();
        }
    }

    public int checkIsUniqueInChannel(String name, String emailid, String phone) {
        SessionFactoryUtil suOperator = new SessionFactoryUtil(SessionFactoryUtil.operators);
        Session sOperator = suOperator.openSession();
        OperatorUtils oUtil = new OperatorUtils(suOperator, sOperator);
        try {
            RmOperators check = oUtil.checkOperatorName( name);
            if (check == null) {
                check = oUtil.checkOperatorEmail(emailid);
            }
            if (check == null) {
                check = oUtil.checkOperatorPhone(phone);
            }
            if (check == null) {
                return 0;
            } else {
                return -1;
            }
        } finally {
            sOperator.close();
            suOperator.close();
        }
    }

    public RmOperators checkIsUniqueInOperators(String operatorId, String name, String emailid, String phone) {
        SessionFactoryUtil suOperator = new SessionFactoryUtil(SessionFactoryUtil.operators);
        Session sOperator = suOperator.openSession();
        OperatorUtils oUtil = new OperatorUtils(suOperator, sOperator);
        try {
            RmOperators check = oUtil.checkOperatorName( name);
            if (check != null) {
                if (operatorId.compareTo(check.getOperatorid()) == 0) {
                    check = null;
                } else {
                    return check;
                }
            }
            if (check == null) {
                check = oUtil.checkOperatorEmail(emailid);
            }
            if (check != null) {
                if (operatorId.compareTo(check.getOperatorid()) == 0) {
                    check = null;
                } else {
                    return check;
                }
            }
            if (check == null) {
                check = oUtil.checkOperatorPhone(phone);
            }
            if (check != null) {
                if (operatorId.compareTo(check.getOperatorid()) == 0) {
                    check = null;
                } else {
                    return check;
                }
            }
            if (check == null) {
                return check;
            } else {
                return null;
            }
        } finally {
            sOperator.close();
            suOperator.close();
        }
    }

    public int changerole(String sessionId, String operatorId, int roleId) {
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        SessionFactoryUtil suOperator = new SessionFactoryUtil(SessionFactoryUtil.operators);
        Session sOperator = suOperator.openSession();
        try {
            if (res == GlobalStatus.ACTIVE) {
                int update = sManagement.UpdateSession(sessionId);
                OperatorUtils oUtil = new OperatorUtils(suOperator, sOperator);
                int result = oUtil.changeRole(operatorId, roleId);
                return result;
            }
        } finally {
            sOperator.close();
            suOperator.close();
        }
        return 0;
    }

    public RmOperators getOperatorById(String operatorId) {
        SessionFactoryUtil suOperator = new SessionFactoryUtil(SessionFactoryUtil.operators);
        Session sOperator = suOperator.openSession();
        OperatorUtils oUtil = new OperatorUtils(suOperator, sOperator);
        RmOperators operatorObj = oUtil.GetOperatorsById(operatorId);
        sOperator.close();
        suOperator.close();
        return operatorObj;
    }

    public int UnlockPassword(String sessionId, String operatorid) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionFactoryUtil suOperator = new SessionFactoryUtil(SessionFactoryUtil.operators);
        Session sOperator = suOperator.openSession();
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        try {
            if (res == GlobalStatus.ACTIVE) {
                sManagement.UpdateSession(sessionId);
                OperatorUtils oUtil = new OperatorUtils(suOperator, sOperator);
                int result = oUtil.UnlockOperatorPassword(operatorid);
                return result;
            } else {
                return -1;
            }
        } finally {
            sSession.close();
            suSession.close();
            sOperator.close();
            suOperator.close();
        }
    }

    public int SetPassword(String operatorId, String password) {
        SessionFactoryUtil suOperator = new SessionFactoryUtil(SessionFactoryUtil.operators);
        Session sOperator = suOperator.openSession();
        try {
            OperatorUtils oUtil = new OperatorUtils(suOperator, sOperator);
            //RmOperators operator = oUtil.getOperator(operatorId);
            int iResult = oUtil.changePassword(operatorId, password);
            if (iResult != 0) {
                return -iResult;
            }
            return 0;
        } finally {
            sOperator.close();
            suOperator.close();
        }
    }

    public RmOperators[] getAdminOperator() {
        SessionFactoryUtil suOperator = new SessionFactoryUtil(SessionFactoryUtil.operators);
        Session sOperator = suOperator.openSession();
        OperatorUtils oUtil = new OperatorUtils(suOperator, sOperator);
        RmRoles[] roles = this.getAllRoles();
        int roleid = 1;
        List<RmOperators> opList = new ArrayList<RmOperators>();
        for (int i = 0; i < roles.length; i++) {
            if (roles[i].getName().equals("sysadmin")) {
                roleid = roles[i].getRoleid();
                RmOperators operatorObj = oUtil.GetAdminOperators(roleid);
                if (operatorObj != null) {
                    opList.add(operatorObj);
                }
            }
        }
        RmOperators[] operList = new RmOperators[opList.size()];
        for (int i = 0; i < operList.length; i++) {
            operList[i] = opList.get(i);
        }
        sOperator.close();
        suOperator.close();
        return operList;
    }

    public int EditOperatorPassword(String sessionId,String operatorid, RmOperators oldOperator, RmOperators newOperator) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionFactoryUtil suOperator = new SessionFactoryUtil(SessionFactoryUtil.operators);
        Session sOperator = suOperator.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        RmSessions sessions = sUtil.getSession(sessionId);
        try {
            RmOperators op = checkIsUniqueInOperators(newOperator.getOperatorid(), newOperator.getName(), newOperator.getEmailid(), newOperator.getPhone());
            if (op == null) {
            } else {
                return -4;
            }
            if (sessions.getStatus() == GlobalStatus.ACTIVE) {
                OperatorUtils oUtil = new OperatorUtils(suOperator, sOperator);
                int result = oUtil.changeOperatorsPassword(operatorid, newOperator);
                return result;
            } else {
                return -2;
            }
        } finally {
            sSession.close();
            suSession.close();
            sOperator.close();
            suOperator.close();
        }
    }

    public RmOperators[] getAllOperators() {
        SessionFactoryUtil suOperator = new SessionFactoryUtil(SessionFactoryUtil.operators);
        Session sOperator = suOperator.openSession();
        try {
            OperatorUtils oUtil = new OperatorUtils(suOperator, sOperator);
            RmOperators[] operList = oUtil.getNewAllOperators();
            return operList;
        } catch (Exception ex) {
            logger.error("Exception at OperatorsManagement ", ex);
        } finally {
            sOperator.close();
            suOperator.close();
        }
        return null;
    }

    public REWARDMEOperator[] ListOperators(String sessionId) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionFactoryUtil suOperator = new SessionFactoryUtil(SessionFactoryUtil.operators);
        Session sOperator = suOperator.openSession();
        SessionFactoryUtil suRoles = new SessionFactoryUtil(SessionFactoryUtil.roles);
        Session sRoles = suRoles.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        RmSessions sessions = sUtil.getSession(sessionId);
        try {
            if (res == GlobalStatus.ACTIVE) {
                int update = sManagement.UpdateSession(sessionId);
                OperatorUtils oUtil = new OperatorUtils(suOperator, sOperator);
                RmOperators[] operators = oUtil.getAllOperators();
                REWARDMEOperator[] axiomOperartor = new REWARDMEOperator[operators.length];
                for (int i = 0; i < operators.length; i++) {
                    RolesUtils rUtil = new RolesUtils(suRoles, sRoles);
                    RmRoles roles = rUtil.getRole(operators[i].getRoleid());
                    axiomOperartor[i] = new REWARDMEOperator();                    
                    axiomOperartor[i].setStrEmail(operators[i].getEmailid());
                    axiomOperartor[i].setStrName(operators[i].getName());
                    axiomOperartor[i].setStrOperatorid(operators[i].getOperatorid());
                    axiomOperartor[i].setStrPhone(operators[i].getPhone());
                    axiomOperartor[i].setiWrongAttempts(operators[i].getCurrentAttempts());
                    axiomOperartor[i].setLastUpdateOn(operators[i].getLastAccessOn().getTime());
                    axiomOperartor[i].setUtcCreatedOn(operators[i].getCreatedOn().getTime());
                    axiomOperartor[i].setStrRoleName(roles.getName());
                    axiomOperartor[i].setStrRoleid(String.valueOf(operators[i].getRoleid()));
                }
                return axiomOperartor;
            } else {
                return null;
            }
        } finally {
            sSession.close();
            suSession.close();
            sRoles.close();
            suRoles.close();
            sOperator.close();
            suOperator.close();
        }
    }

    public REWARDMEOperator[] ListOperators() {
        SessionFactoryUtil suOperator = new SessionFactoryUtil(SessionFactoryUtil.operators);
        Session sOperator = suOperator.openSession();
        try {
            OperatorUtils oUtil = new OperatorUtils(suOperator, sOperator);
            RmOperators[] operators = oUtil.getAllOperators();
            REWARDMEOperator[] axiomOperartor = new REWARDMEOperator[operators.length];
            for (int i = 0; i < operators.length; i++) {
                axiomOperartor[i] = new REWARDMEOperator();                
                axiomOperartor[i].setStrEmail(operators[i].getEmailid());
                axiomOperartor[i].setStrName(operators[i].getName());
                axiomOperartor[i].setStrOperatorid(operators[i].getOperatorid());
                axiomOperartor[i].setStrPhone(operators[i].getPhone());
                axiomOperartor[i].setiWrongAttempts(operators[i].getCurrentAttempts());
                axiomOperartor[i].setLastUpdateOn(operators[i].getLastAccessOn().getTime());
                axiomOperartor[i].setUtcCreatedOn(operators[i].getCreatedOn().getTime());
                axiomOperartor[i].setStrRoleid(String.valueOf(operators[i].getRoleid()));
            }
            return axiomOperartor;
        } finally {
            sOperator.close();
            suOperator.close();
        }
    }

    public REWARDMEOperator[] ListOperatorsInternal() {
        SessionFactoryUtil suOperator = new SessionFactoryUtil(SessionFactoryUtil.operators);
        Session sOperator = suOperator.openSession();
        SessionFactoryUtil suRoles = new SessionFactoryUtil(SessionFactoryUtil.roles);
        Session sRoles = suRoles.openSession();
        try {
            OperatorUtils oUtil = new OperatorUtils(suOperator, sOperator);
            RmOperators[] operatorsObj = oUtil.getAllOperators();
            REWARDMEOperator[] axiomOperartor = new REWARDMEOperator[operatorsObj.length];
            RolesUtils rUtil = new RolesUtils(suRoles, sRoles);
            for (int i = 0; i < operatorsObj.length; i++) {
                RmRoles roles = rUtil.getRole(operatorsObj[i].getRoleid());
                axiomOperartor[i] = new REWARDMEOperator();                
                axiomOperartor[i].setStrEmail(operatorsObj[i].getEmailid());
                axiomOperartor[i].setStrName(operatorsObj[i].getName());
                axiomOperartor[i].setiStatus(operatorsObj[i].getStatus());
                axiomOperartor[i].setStrOperatorid(operatorsObj[i].getOperatorid());
                axiomOperartor[i].setStrPhone(operatorsObj[i].getPhone());
                axiomOperartor[i].setiWrongAttempts(operatorsObj[i].getCurrentAttempts());
                axiomOperartor[i].setLastUpdateOn(operatorsObj[i].getLastAccessOn().getTime());
                axiomOperartor[i].setUtcCreatedOn(operatorsObj[i].getCreatedOn().getTime());
                axiomOperartor[i].setStrRoleName(roles.getName());
                axiomOperartor[i].setStrRoleid(String.valueOf(operatorsObj[i].getRoleid()));
            }
            return axiomOperartor;
        } finally {
            sRoles.close();
            suRoles.close();
            sOperator.close();
            suOperator.close();
        }
    }

    public REWARDMEOperator GetOperatorByName(String sessionId, String name) {
        REWARDMEOperator axiomOperartor = new REWARDMEOperator();
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionFactoryUtil suOperator = new SessionFactoryUtil(SessionFactoryUtil.operators);
        Session sOperator = suOperator.openSession();
        SessionFactoryUtil suRoles = new SessionFactoryUtil(SessionFactoryUtil.roles);
        Session sRoles = suRoles.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        RmSessions sessions = sUtil.getSession(sessionId);
        try {
            if (res == GlobalStatus.ACTIVE) {
                int update = sManagement.UpdateSession(sessionId);
                OperatorUtils oUtil = new OperatorUtils(suOperator, sOperator);
                RmOperators oprObj = oUtil.GetByName(name);
                RolesUtils rUtil = new RolesUtils(suRoles, sRoles);
                RmRoles roles = rUtil.getRole(oprObj.getRoleid());                
                axiomOperartor.setStrEmail(oprObj.getEmailid());
                axiomOperartor.setStrName(name);
                axiomOperartor.setStrOperatorid(oprObj.getOperatorid());
                axiomOperartor.setStrPhone(oprObj.getPhone());
                axiomOperartor.setiWrongAttempts(oprObj.getCurrentAttempts());
                axiomOperartor.setLastUpdateOn(oprObj.getLastAccessOn().getTime());
                axiomOperartor.setUtcCreatedOn(oprObj.getCreatedOn().getTime());
                axiomOperartor.setStrRoleName(roles.getName());
                axiomOperartor.setStrRoleid(String.valueOf(oprObj.getRoleid()));
                return axiomOperartor;
            } else {
                return null;
            }
        } finally {
            sSession.close();
            suSession.close();
            sOperator.close();
            suOperator.close();
            sRoles.close();
            suRoles.close();
        }
    }

    public REWARDMEOperator[] GetOperatorByRoleId(int roleId) {
        REWARDMEOperator[] axiomOperartor = null;
        SessionFactoryUtil suOperator = new SessionFactoryUtil(SessionFactoryUtil.operators);
        Session sOperator = suOperator.openSession();
        try {
            OperatorUtils oUtil = new OperatorUtils(suOperator, sOperator);
            RmOperators[] oprObj = oUtil.GetByRoleID(roleId);
            axiomOperartor = new REWARDMEOperator[oprObj.length];
            for (int i = 0; i < axiomOperartor.length; i++) {
                axiomOperartor[i] = new REWARDMEOperator();                
                axiomOperartor[i].setStrEmail(oprObj[i].getEmailid());
                axiomOperartor[i].setStrName(oprObj[i].getName());
                axiomOperartor[i].setStrOperatorid(oprObj[i].getOperatorid());
                axiomOperartor[i].setStrPhone(oprObj[i].getPhone());
                axiomOperartor[i].setiWrongAttempts(oprObj[i].getCurrentAttempts());
                axiomOperartor[i].setLastUpdateOn(oprObj[i].getLastAccessOn().getTime());
                axiomOperartor[i].setUtcCreatedOn(oprObj[i].getCreatedOn().getTime());
            }
            return axiomOperartor;
        } finally {
            sOperator.close();
            suOperator.close();
        }
    }

    public RmOperators initializeResetPassword(String sessionId, String name, String email, String randomString, Date expiry) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionFactoryUtil suOperator = new SessionFactoryUtil(SessionFactoryUtil.operators);
        Session sOperator = suOperator.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        RmSessions sessions = sUtil.getSession(sessionId);
        try {
            if (res == GlobalStatus.ACTIVE) {
                int update = sManagement.UpdateSession(sessionId);
                OperatorUtils oUtil = new OperatorUtils(suOperator, sOperator);
                RmOperators operators = oUtil.initializeResetPassword(name, email, randomString, expiry);
                return operators;
            }
            return null;
        } finally {
            sSession.close();
            suSession.close();
            sOperator.close();
            suOperator.close();
        }
    }

    public RmOperators resetPassword(String sessionId, String name, String email, String randomString, String password) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionFactoryUtil suOperator = new SessionFactoryUtil(SessionFactoryUtil.operators);
        Session sOperator = suOperator.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        RmSessions sessions = sUtil.getSession(sessionId);
        try {
            if (res == GlobalStatus.ACTIVE) {
                int update = sManagement.UpdateSession(sessionId);
                OperatorUtils oUtil = new OperatorUtils(suOperator, sOperator);
                RmOperators operators = oUtil.resetPassword(name, email, randomString, password);
                return operators;
            }
            return null;
        } finally {
            sSession.close();
            suSession.close();
            sOperator.close();
            suOperator.close();
        }
    }
}
