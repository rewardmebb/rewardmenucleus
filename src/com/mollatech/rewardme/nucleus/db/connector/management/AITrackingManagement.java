/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector.management;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.mollatech.rewardme.nucleus.db.RmAiTrackingCampaignDetails;
import com.mollatech.rewardme.nucleus.db.RmCampaigndetails;
import com.mollatech.rewardme.nucleus.db.connector.AITrackingUtil;
import com.mollatech.rewardme.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
public class AITrackingManagement {
    
    static final Logger logger = Logger.getLogger(AITrackingManagement.class);
    
    public int addTracking(RmAiTrackingCampaignDetails x) {
        int result = -1;
        try {
            SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.aiTrackingCampaignDetails);
            Session sSession = suSession.openSession();
            AITrackingUtil prtUtil = new AITrackingUtil(suSession, sSession);
            try {
                result = prtUtil.addTracking(x);
            } catch (Exception ex) {
                logger.error("Exception at AITrackingManagement ", ex);
            }
            sSession.close();
            suSession.close();
        } finally {
        }

        return result;
    }    

    public RmAiTrackingCampaignDetails[] getrequesttracking() {
        RmAiTrackingCampaignDetails[] obj = null;
        SessionFactoryUtil guSession = new SessionFactoryUtil(SessionFactoryUtil.aiTrackingCampaignDetails);
        Session gSession = guSession.openSession();        
        try {
            AITrackingUtil otptrail = new AITrackingUtil(guSession, gSession);
            obj = otptrail.getaitracking();
        } finally {
            gSession.close();
            guSession.close();
        }
        return obj;
    }

    public int getCount() {
        SessionFactoryUtil guSession = new SessionFactoryUtil(SessionFactoryUtil.aiTrackingCampaignDetails);
        Session gSession = guSession.openSession();       
        try {
            AITrackingUtil otptrail = new AITrackingUtil(guSession, gSession);
            return otptrail.getCount();
        } catch (Exception ex) {
            logger.error("Exception at AITrackingManagement ", ex);
            return -2;
        } finally {
            gSession.close();
            guSession.close();
        }       
    }
    
    public RmAiTrackingCampaignDetails[] getTxDetails(Integer ownerId, String startDate, String endDate, String stime, String etime) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.aiTrackingCampaignDetails);
        Session sSession = suSession.openSession();
        try {
            RmAiTrackingCampaignDetails[] sgTranscationdetailses = new AITrackingUtil(suSession, sSession).getTxDetails(ownerId, startDate, endDate, stime, etime);
            return sgTranscationdetailses;
        } catch (Exception ex) {
            logger.error("Exception at AITrackingManagement ", ex);
        } finally {
            sSession.close();
            suSession.close();
        }
        return null;
    }
    
    public RmAiTrackingCampaignDetails[] getTxDetails(Integer ownerId, String startDate, String endDate, String stime, String etime,String campExeName, Integer campaignId) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.aiTrackingCampaignDetails);
        Session sSession = suSession.openSession();
        try {
            RmAiTrackingCampaignDetails[] sgTranscationdetailses = new AITrackingUtil(suSession, sSession).getTxDetails(ownerId, startDate, endDate, stime, etime,campExeName,campaignId);
            return sgTranscationdetailses;
        } catch (Exception ex) {
            logger.error("Exception at AITrackingManagement ", ex);
        } finally {
            sSession.close();
            suSession.close();
        }
        return null;
    }
    
    public RmAiTrackingCampaignDetails[] getTxDetails(Integer ownerId, Date startDate, Date endDate) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.aiTrackingCampaignDetails);
        Session sSession = suSession.openSession();
        try {
            RmAiTrackingCampaignDetails[] sgTranscationdetailses = new AITrackingUtil(suSession, sSession).getTxDetails(ownerId,startDate, endDate);
            return sgTranscationdetailses;
        } catch (Exception ex) {
            logger.error("Exception at AITrackingManagement ", ex);
        } finally {
            sSession.close();
            suSession.close();
        }
        return null;
    }

    public RmAiTrackingCampaignDetails[] getTxDetailsByAdvatisorId(Integer ownerId, Date startDate, Date endDate) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.aiTrackingCampaignDetails);
        Session sSession = suSession.openSession();
        try {
            RmAiTrackingCampaignDetails[] sgTranscationdetailses = new AITrackingUtil(suSession, sSession).getTxDetailsByAdvertisorId(ownerId,startDate, endDate);
            return sgTranscationdetailses;
        } catch (Exception ex) {
            logger.error("Exception at AITrackingManagement ", ex);
        } finally {
            sSession.close();
            suSession.close();
        }
        return null;
    }
    
    public RmAiTrackingCampaignDetails[] getTxDetailsByOwnerIdAndCampaignId(Integer ownerId, Date startDate, Date endDate, Integer campaignId) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.aiTrackingCampaignDetails);
        Session sSession = suSession.openSession();
        try {
            RmAiTrackingCampaignDetails[] sgTranscationdetailses = new AITrackingUtil(suSession, sSession).getTxDetailsByOwnerIdAndCampaignId(ownerId,startDate, endDate,campaignId);
            return sgTranscationdetailses;
        } catch (Exception ex) {
            logger.error("Exception at AITrackingManagement ", ex);
        } finally {
            sSession.close();
            suSession.close();
        }
        return null;
    }
    
    public List<Object[]> getCountUsingHQLForRealTimeUsingEXEName(Integer ownerId, Date startDate, Date endDate, String exeName, boolean equal) {
        long tpd = 0L;

        SessionFactoryUtil guSession = new SessionFactoryUtil(SessionFactoryUtil.aiTrackingCampaignDetails);
        Session gSession = guSession.openSession();
        try {
            AITrackingUtil aiTracking = new AITrackingUtil(guSession, gSession);
            return aiTracking.getCountUsingHQLForRealTimeUsingEXEName(ownerId, startDate, endDate,exeName,equal);

        } finally {
            gSession.close();
            guSession.close();
        }
    }
    
    public List<Object[]> getCountUsingHQL(int ownerId, Date sDate, Date eDate, boolean equal) {
        long tpd = 0L;

        SessionFactoryUtil guSession = new SessionFactoryUtil(SessionFactoryUtil.aiTrackingCampaignDetails);
        Session gSession = guSession.openSession();
        try {
            AITrackingUtil otptrail = new AITrackingUtil(guSession, gSession);
            return otptrail.getCountUsingHQL(ownerId, sDate, eDate, equal);

        } finally {
            gSession.close();
            guSession.close();
        }
    }
    
    public List<Object[]> getCountUsingHQLForRealTime(int ownerId, Date sDate, Date eDate, boolean equal) {
        long tpd = 0L;

        SessionFactoryUtil guSession = new SessionFactoryUtil(SessionFactoryUtil.aiTrackingCampaignDetails);
        Session gSession = guSession.openSession();
        try {
            AITrackingUtil otptrail = new AITrackingUtil(guSession, gSession);
            return otptrail.getCountUsingHQLForRealTime(ownerId, sDate, eDate, equal);

        } finally {
            gSession.close();
            guSession.close();
        }

    }
    
    public List<Object[]> getCampaignRunCountUsingHQL(int ownerId, Date sDate, Date eDate, boolean equal) {
        long tpd = 0L;

        SessionFactoryUtil guSession = new SessionFactoryUtil(SessionFactoryUtil.aiTrackingCampaignDetails);
        Session gSession = guSession.openSession();
        try {
            AITrackingUtil otptrail = new AITrackingUtil(guSession, gSession);
            return otptrail.getCampaignRunCountUsingHQL(ownerId, sDate, eDate, equal);

        } finally {
            gSession.close();
            guSession.close();
        }

    }
    
    public List<Object[]> getTrendingCountUsingHQL(int advertisorId, Date sDate, Date eDate, boolean equal) {
        long tpd = 0L;

        SessionFactoryUtil guSession = new SessionFactoryUtil(SessionFactoryUtil.aiTrackingCampaignDetails);
        Session gSession = guSession.openSession();
        try {
            AITrackingUtil otptrail = new AITrackingUtil(guSession, gSession);
            return otptrail.getTesdingCountUsingHQL(advertisorId, sDate, eDate, equal);

        } finally {
            gSession.close();
            guSession.close();
        }

    }
    private Document document;
    private String filePath;
    public static final String DATE_FORMAT_NOW = "yyyy-MM-dd hh:mm:ss";
    SimpleDateFormat tmDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
    private final String[] aREPORTHEADERCOL = { "Sr.No.", "Date", "Campaign", "User Name", "Screen Name", "Follower Count", "Location"};
    private File generateTempFile(String strExtension) throws IOException {
        return File.createTempFile("RewardMe", strExtension);
    }
    public PrintWriter createCSV() {
        try {
            FileWriter fw = null;
            PrintWriter out = null;
            File f = generateTempFile(".csv");
            filePath = f.getAbsolutePath();
            fw = new FileWriter(filePath);
            Font f1 = new Font();
            f1.setSize(15);
            out = new PrintWriter(fw);
            out.print("                                            Report Generated by RewardMe for " + ",\n");
            for (int a = 0; a < 1; ++a) {
                for (int j = 0; j < aREPORTHEADERCOL.length; ++j) {
                    out.print(aREPORTHEADERCOL[j] + ",");
                }
            }
            return out;
        } catch (IOException ex) {
            logger.error("Exception at AITrackingManagement ", ex);
        }
        return null;
    }
    public String generateReport(int iReportType, RmAiTrackingCampaignDetails[] aiTacking) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
        filePath = "";
        try {
            PdfPTable table = null;
            PrintWriter out = null;
            if (iReportType == 0) {
                //table = createPdf(duration);
            } else {
                out = createCSV();
            }
            Font f1 = new Font();
            f1.setSize(8);
            if (aiTacking != null) {
                int count = 0;
                for (int i = 0; i < aiTacking.length; i++) {
                    String campName = "NA";
                    String user = "NA";
                    String followerCount = "NA";
                    String date = null;
                    String location = "NA";
                    String twitterPostURL = null;
                    String screenName = "NA";
                    if (aiTacking[i].getTwitterReturnData() != null) {
                        JSONObject data = new JSONObject(aiTacking[i].getTwitterReturnData());
                        RmCampaigndetails campDetails = new CampaignManagement().getCampaignByIdDetails(aiTacking[i].getCampaignId());
                        if (campDetails != null) {
                            campName = campDetails.getRewardTitle();
                        }
                        if (data.has("user")) {
                            JSONObject userJson = data.getJSONObject("user");
                            if (userJson.has("followers_count")) {
                                followerCount = userJson.getString("followers_count");
                            }
                            if (userJson.has("name")) {
                                user = userJson.getString("name");
                            }
                            if (userJson.has("location")) {
                                location = userJson.getString("location");
                                if (location != null && !location.equalsIgnoreCase("null")) {
                                } else {
                                    location = "NA";
                                }
                            }
                            if (userJson.has("screen_name")) {
                                screenName = userJson.getString("screen_name");
                            }
                        }
                        if (data.has("created_at")) {
                            date = data.getString("created_at");
                        }
                        count++;
                    if (iReportType == 0) {
//                        PdfPCell auditId1 = new PdfPCell(new Phrase(String.valueOf((i + 1)), f1));
//                        table.addCell(auditId1);
//                        PdfPCell sessionId1 = new PdfPCell(new Phrase(audit[i].getSessionId(), f1));
//                        table.addCell(sessionId1);
//                        PdfPCell remoteAccessLogin = new PdfPCell(new Phrase(audit[i].getRemoteaccesslogin(), f1));
//                        table.addCell(remoteAccessLogin);
//                        PdfPCell ipAddress = new PdfPCell(new Phrase(audit[i].getIpaddress(), f1));
//                        table.addCell(ipAddress);
//                        PdfPCell operatorname1 = new PdfPCell(new Phrase(audit[i].getOperatorname(), f1));
//                        table.addCell(operatorname1);
//                        PdfPCell category1 = new PdfPCell(new Phrase(audit[i].getCategory(), f1));
//                        table.addCell(category1);
//                        PdfPCell itemtype1 = new PdfPCell(new Phrase(audit[i].getItemtype(), f1));
//                        table.addCell(itemtype1);
//                        PdfPCell action1 = new PdfPCell(new Phrase(audit[i].getAction(), f1));
//                        table.addCell(action1);
//                        PdfPCell result1 = new PdfPCell(new Phrase(audit[i].getResult(), f1));
//                        table.addCell(result1);
//                        PdfPCell newvalue1 = new PdfPCell(new Phrase(audit[i].getNewvalue(), f1));
//                        table.addCell(newvalue1);
//                        PdfPCell oldvalue1 = new PdfPCell(new Phrase(audit[i].getOldvalue(), f1));
//                        table.addCell(oldvalue1);
//                        PdfPCell signature1 = new PdfPCell(new Phrase(audit[i].getIntegritycheck(), f1));
//                        table.addCell(signature1);
//                        Date date = audit[i].getAuditedon();
//                        Calendar calStart = new GregorianCalendar();
//                        calStart.setTime(date);
//                        calStart.set(Calendar.MILLISECOND, 0);
//                        Date d = calStart.getTime();
//                        PdfPCell auditedon1 = new PdfPCell(new Phrase(String.valueOf(d), f1));
//                        table.addCell(auditedon1);
                    } else {
                        out.println();
                        out.print(String.valueOf((count)) + ",");
                        out.print(tmDateFormat.format(aiTacking[i].getCreationDate()) + ",");
                        out.print(campName + ",");
                        out.print(user+ ",");
                        out.print(screenName + ",");
                        out.print(followerCount + ",");
                        out.print(location + ",");                        
                        out.println();
                        out.flush();
                    }
                }
            }
            }else if (aiTacking == null) {
                if (iReportType == 0) {
                    PdfPCell cell = new PdfPCell(new Phrase("No Records Found"));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(13);
                    table.addCell(cell);
                } else {
                    out.println();
                    out.print("                                                No Record Found");
                    out.println();
                    out.flush();
                }
            }
            if (iReportType == 0) {
                Date date1 = new Date();
                SimpleDateFormat sd1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                String da1 = sd1.format(date1);
                PdfPCell cell1 = new PdfPCell(new Phrase("Date " + da1));
                cell1.setBackgroundColor(BaseColor.GRAY);
                cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell1.setColspan(16);
                table.addCell(cell1);
                table.setSkipLastFooter(true);
                String author = LoadSettings.g_sSettings.getProperty("report.author");
                String title = LoadSettings.g_sSettings.getProperty("report.title");
                document.addAuthor(author);
                document.addTitle(title);
                document.add(table);
                document.close();
            } else {
                Date date1 = new Date();
                SimpleDateFormat sd1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                String da1 = sd1.format(date1);
                out.print("                                                               Date  :" + da1);
                out.println();
                out.flush();
            }
            if (out != null) {
                out.close();
            }
        
        }catch (Exception e) {
            logger.error("Exception at AITrackingManagement ", e);
        }
        if (document != null) {
            if (document.isOpen()) {
                document.close();
            }
        }
        return filePath;
    }

}
