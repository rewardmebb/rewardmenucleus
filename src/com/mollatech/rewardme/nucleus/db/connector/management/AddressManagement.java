/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector.management;

import com.mollatech.rewardme.nucleus.commons.GlobalStatus;
import com.mollatech.rewardme.nucleus.db.RmCities;
import com.mollatech.rewardme.nucleus.db.RmCountries;
import com.mollatech.rewardme.nucleus.db.RmStates;
import com.mollatech.rewardme.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.rewardme.nucleus.db.connector.SessionUtil;
import org.hibernate.Session;
import com.mollatech.rewardme.nucleus.db.connector.AdressUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author bluebricks
 */
public class AddressManagement {

    public static final int ACTIVE_STATUS = 1;

    static final Logger logger = Logger.getLogger(AddressManagement.class);

    public RmCountries[] getAllCountries() {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.country);
        Session pSession = puSession.openSession();
        AdressUtils pUtil = new AdressUtils(puSession, pSession);
        RmCountries[] groups = null;
        try {
                groups = pUtil.getAllCity();
                return groups;
            
        } catch (Exception ex) {
            logger.error("Exception at AddressManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
            sSession.close();
            suSession.close();
        }
        return null;
    }
    
     public RmCities[] getAllCityByStateId(String sessionid,int stateId) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionid);
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.country);
        Session pSession = puSession.openSession();
        RmCities[] cityDetails = null;
        AdressUtils cityUtils = new AdressUtils(suSession, sSession);
        try {
            if (res == GlobalStatus.ACTIVE) {
                cityDetails = cityUtils.getAllCityByStateid(stateId);
                return cityDetails;
            } else {
                return null;
            }
        } catch (Exception ex) {
            logger.error("Exception at AddressManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
            sSession.close();
            suSession.close();
        }
        return null;
    }
     
      public RmStates[] getAllstateByCountryid(String sessionid,int cityID) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionid);
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.country);
        Session pSession = puSession.openSession();
        RmStates [] cityDetails = null;
        AdressUtils cityUtils = new AdressUtils(suSession, sSession);
        try {
            if (res == GlobalStatus.ACTIVE) {
                cityDetails = cityUtils.getAllstateByCountry(cityID);
                return cityDetails;
            } else {
                return null;
            }
        } catch (Exception ex) {
            logger.error("Exception at AddressManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
            sSession.close();
            suSession.close();
        }
        return null;
    }
     
     
    
}
