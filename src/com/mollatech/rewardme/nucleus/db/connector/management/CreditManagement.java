/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector.management;

import com.mollatech.rewardme.nucleus.db.RmCreditinfo;
import com.mollatech.rewardme.nucleus.db.connector.CreditUtils;
import com.mollatech.rewardme.nucleus.db.connector.SessionFactoryUtil;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author abhishekingle
 */
public class CreditManagement {
    
    static final Logger logger = Logger.getLogger(CreditManagement.class);

    public void addDetails(RmCreditinfo creditInfo) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.creditInfo);
        Session sSession = suSession.openSession();
        CreditUtils sUtil = new CreditUtils(suSession, sSession);
        try {
            sUtil.addDetails(creditInfo);
        } finally {
            sSession.close();
            suSession.close();
        }
    }

    public RmCreditinfo getDetailsByOwnerId(Integer ownerId) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.creditInfo);
        Session sSession = suSession.openSession();
        CreditUtils sUtil = new CreditUtils(suSession, sSession);
        try {
            return sUtil.getDetailsByOwnerId(ownerId);
        } finally {
            sSession.close();
            suSession.close();
        }
    }

    public int updateDetails(RmCreditinfo creditInfo) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.creditInfo);
        Session sSession = suSession.openSession();
        CreditUtils sUtil = new CreditUtils(suSession, sSession);
        try {
            return sUtil.updateDetails(creditInfo);
        } finally {
            sSession.close();
            suSession.close();
        }
    }
}
