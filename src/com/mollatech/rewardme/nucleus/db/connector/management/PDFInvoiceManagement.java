/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector.management;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.html.WebColors;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.draw.LineSeparator;
import com.itextpdf.text.pdf.draw.VerticalPositionMark;
import com.mollatech.rewardme.nucleus.commons.TaxCalculation;
import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
import com.mollatech.rewardme.nucleus.db.RmSubscriptionpackagedetails;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
public class PDFInvoiceManagement {
    
    static final Logger logger = Logger.getLogger(PDFInvoiceManagement.class);
    private String filePath;
    double totalPayment = 0.0f;
    Double totalAmount = 0.0;
    DecimalFormat df = new DecimalFormat("#0.00");

    String apName = "NA";
    String resName = "NA";
    String envt = "NA";
    String apiName = "NA";
    int version = 0;
    int callCount = 0;
    float amount = 0;
    float totalCharge = 0.0f;
    Map<String, String> taxMap = new LinkedHashMap<String, String>();
    Map<String, Double> packageamount = new LinkedHashMap<String, Double>();
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
    SimpleDateFormat dailyTxFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
        
//    public static void main(String[] args) {
//         RmBrandownerdetails partnerObj = new BrandOwnerManagement().getBrandOwnerDetails(7);
//        RmSubscriptionpackagedetails subscriptionObject = new SubscriptionManagement().getSubscriptionbyOwnerId(partnerObj.getOwnerId());
//        try{
//        new PDFInvoiceManagement().createInvoicePDF(110d, partnerObj,"1121212", subscriptionObject);
//        }catch(Exception e){
//            
//        }
//    }
    private File generateURLFile(RmSubscriptionpackagedetails subcriptionObj, String strExtension, String invoiceId, String url) throws IOException{
        String filePath = url + subcriptionObj.getPackageName() + invoiceId + new SimpleDateFormat("dd-MM-yyyy").format(new Date())+strExtension;
        File file = new File(filePath);
        return file;
    }
    
    public void addCompanyLogo(Document document, PdfWriter writer) {
        try {
            Image companyLogo = Image.getInstance(LoadSettings.g_strPath + System.getProperty("file.separator") + "readyAPILogo.png");
            companyLogo.setWidthPercentage(100);
            PdfPCell cell = new PdfPCell();
            cell.addElement(companyLogo);
            companyLogo.setAlignment(Element.ALIGN_LEFT);

            PdfPTable tabletmp = new PdfPTable(1);
            tabletmp.setHorizontalAlignment(Element.ALIGN_LEFT);
            tabletmp.getDefaultCell().setBorder(Rectangle.NO_BORDER);
            tabletmp.setWidthPercentage(25);
            float[] colWidths = {45, 55};
            cell.setBorder(Rectangle.NO_BORDER);
            tabletmp.addCell(companyLogo);

            PdfContentByte canvas = writer.getDirectContent();
            canvas.saveState();
            document.add(tabletmp);
            canvas.restoreState();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addLineSeperator(Document document) {
        try {
            Paragraph p = new Paragraph();
            LineSeparator line = new LineSeparator();
            line.setOffset(-1);
            p.add(line);
            document.add(p);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }    
    
    public PdfPCell getCell(String text, int alignment) {
        PdfPCell cell = new PdfPCell(new Phrase(text));
        cell.setPadding(0);
        cell.setHorizontalAlignment(alignment);
        cell.setMinimumHeight(20);
        cell.setBorder(PdfPCell.NO_BORDER);
        return cell;
    }
    
    public String createInvoicePDF(Double packageAmount, RmBrandownerdetails partnerObj, String invoiceNo, RmSubscriptionpackagedetails subcriptionObj) throws IOException {
        df.setMaximumFractionDigits(2);
        Font bold = new Font(Font.FontFamily.HELVETICA, 14f, Font.BOLD);
        Font normalBold = new Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD);
        //Font normal1 = new Font(Font.FontFamily.HELVETICA, 8f, Font.NORMAL);
        Double totalPaid = new Double(0);
        Document document = new Document();
        //String tmWellFormbenefitDiscount = "NA";
        String gstNumber = "NA";
            String strgstNum = (String) LoadSettings.g_sSettings.getProperty("gst.number");
            if(strgstNum != null){
                gstNumber = strgstNum;
            }
        try {
//            filePath = generateTempFile(subcriptionObj, ".pdf", invoiceNo).getAbsolutePath();
            String urPath = LoadSettings.pdfSavePath;
            filePath = generateURLFile(subcriptionObj, ".pdf", invoiceNo,urPath).toString();
            FileOutputStream fout = new FileOutputStream(filePath);
            PdfWriter writer = PdfWriter.getInstance(document, fout);
            boolean pdfSignEnable = false;
                                    
            document.open();
            addCompanyLogo(document, writer);
            addLineSeperator(document);

            document.add(new Phrase("\n"));
            document.add(new Phrase("\n"));
            Chunk glue = new Chunk(new VerticalPositionMark());
            Paragraph p = new Paragraph("Invoice No. " + invoiceNo, bold);
            p.add(new Chunk(glue));
            p.add("To");
            document.add(p);
            //document.add(blankCell);
            PdfPTable table = new PdfPTable(3);
            table.setWidthPercentage(100);
            table.addCell(getCell(" ", PdfPCell.ALIGN_LEFT));
            table.addCell(getCell(" ", PdfPCell.ALIGN_CENTER));
            table.addCell(getCell(" ", PdfPCell.ALIGN_RIGHT));

            table.addCell(getCell("Blue Bricks Pty. Ltd.", PdfPCell.ALIGN_LEFT));
            table.addCell(getCell(" ", PdfPCell.ALIGN_CENTER));
            table.addCell(getCell(partnerObj.getBrandName(), PdfPCell.ALIGN_RIGHT));

            table.addCell(getCell("Unit 39, 118 Adderton Road,", PdfPCell.ALIGN_LEFT));
            table.addCell(getCell(" ", PdfPCell.ALIGN_CENTER));
            table.addCell(getCell("E : " + partnerObj.getEmail(), PdfPCell.ALIGN_RIGHT));

            table.addCell(getCell("Carlingford 2118 NSW,", PdfPCell.ALIGN_LEFT));
            table.addCell(getCell(" ", PdfPCell.ALIGN_CENTER));
//            table.addCell(getCell("Package : "+subcriptionObj.getBucketName(), PdfPCell.ALIGN_RIGHT));
            table.addCell(getCell("Date : " + dailyTxFormat.format(subcriptionObj.getCreationDate()), PdfPCell.ALIGN_RIGHT));

            table.addCell(getCell("Australia", PdfPCell.ALIGN_LEFT));
            table.addCell(getCell(" ", PdfPCell.ALIGN_CENTER));
//            table.addCell(getCell("Date : "+dailyTxFormat.format(subcriptionObj.getCreationDate()), PdfPCell.ALIGN_RIGHT));
            table.addCell(getCell("", PdfPCell.ALIGN_RIGHT));
            if(!gstNumber.equalsIgnoreCase("na")){
                table.addCell(getCell("GST No.  "+gstNumber, PdfPCell.ALIGN_LEFT));
                table.addCell(getCell(" ", PdfPCell.ALIGN_CENTER));
                table.addCell(getCell("", PdfPCell.ALIGN_RIGHT));
            }
            document.add(table);

            PdfPTable pdfTable = new PdfPTable(3);
            pdfTable.setWidthPercentage(100);
            document.add(new Phrase("\n"));
            document.add(new Phrase("\n"));
            PdfPCell headerCell = new PdfPCell(new Phrase("Item List", normalBold));
            //headerCell.setBackgroundColor(BaseColor.ORANGE);
            headerCell.setBorder(Rectangle.NO_BORDER);
            headerCell.setMinimumHeight(30);
            headerCell.setHorizontalAlignment(Element.ALIGN_LEFT);

            pdfTable.addCell(headerCell);
            pdfTable.setHeaderRows(1);

            headerCell = new PdfPCell(new Phrase("Details", normalBold));
            //headerCell.setBackgroundColor(BaseColor.ORANGE);
            headerCell.setBorder(Rectangle.NO_BORDER);
            headerCell.setMinimumHeight(30);
            headerCell.setHorizontalAlignment(Element.ALIGN_LEFT);
            pdfTable.addCell(headerCell);
            pdfTable.setHeaderRows(1);

            headerCell = new PdfPCell(new Phrase("Total Price", normalBold));
            //headerCell.setBackgroundColor(BaseColor.ORANGE);
            headerCell.setBorder(Rectangle.NO_BORDER);
            headerCell.setMinimumHeight(30);
            headerCell.setHorizontalAlignment(Element.ALIGN_LEFT);
            pdfTable.addCell(headerCell);
            pdfTable.setHeaderRows(1);
//            addLineSeperator(document);            

            BaseColor myColor = WebColors.getRGBColor("#ddd");
            boolean backgroudColourSet = false;
            if (packageAmount != null) {
                String packageName = subcriptionObj.getPackageName();
                String bucketName = packageName.toLowerCase();
                if (bucketName.contains("basic") && bucketName.contains("month")) {
                    packageName = "Basic";
                } else if (bucketName.contains("basic") && bucketName.contains("year")) {
                    packageName = "Basic";
                } else if (bucketName.contains("student") && bucketName.contains("month")) {
                    packageName = "Student";
                } else if (bucketName.contains("student") && bucketName.contains("year")) {
                    packageName = "Student";
                } else if (bucketName.contains("standard") && bucketName.contains("month")) {
                    packageName = "Startup";
                } else if (bucketName.contains("standard") && bucketName.contains("year")) {
                    packageName = "Startup";
                } else if (bucketName.contains("enterprise") && bucketName.contains("month")) {
                    packageName = "Enterprise";
                } else if (bucketName.contains("enterprise") && bucketName.contains("year")) {
                    packageName = "Enterprise";
                }
                PdfPCell packageAmountCell = new PdfPCell(new Phrase(packageName));
                packageAmountCell.setBorder(Rectangle.TOP);
                packageAmountCell.setBackgroundColor(myColor);
                packageAmountCell.setMinimumHeight(40);
                packageAmountCell.setPaddingTop(13);
                packageAmountCell.setHorizontalAlignment(Element.ALIGN_LEFT);
                pdfTable.addCell(packageAmountCell);

                PdfPCell packageAmountDetailsCell = new PdfPCell(new Phrase(" "));
                packageAmountDetailsCell.setBorder(Rectangle.TOP);
                packageAmountDetailsCell.setBackgroundColor(myColor);
                packageAmountDetailsCell.setMinimumHeight(40);
                packageAmountDetailsCell.setPaddingTop(13);
                packageAmountDetailsCell.setHorizontalAlignment(Element.ALIGN_LEFT);
                pdfTable.addCell(packageAmountDetailsCell);

                PdfPCell packageAmountTotalPriceCell = new PdfPCell(new Phrase("AUD " + df.format(packageAmount)));
                packageAmountTotalPriceCell.setBorder(Rectangle.TOP);
                packageAmountTotalPriceCell.setBackgroundColor(myColor);
                packageAmountTotalPriceCell.setMinimumHeight(40);
                packageAmountTotalPriceCell.setPaddingTop(13);
                packageAmountTotalPriceCell.setHorizontalAlignment(Element.ALIGN_LEFT);
                pdfTable.addCell(packageAmountTotalPriceCell);
                backgroudColourSet = true;
                totalPaid += Double.valueOf(packageAmount);
            }                        
            taxMap = TaxCalculation.calculateTax(subcriptionObj.getTax(), totalPaid);
            if (!taxMap.isEmpty()) {
                for (Map.Entry<String, String> entry : taxMap.entrySet()) {
                    if (entry.getKey().equalsIgnoreCase("GST Tax")) {
                        String[] taxArr = entry.getValue().split(":");
                        PdfPCell packageAmountCell = new PdfPCell(new Phrase(entry.getKey()));
                        packageAmountCell.setBorder(Rectangle.TOP);
                        if (!backgroudColourSet) {
                            packageAmountCell.setBackgroundColor(myColor);
                        }
                        packageAmountCell.setMinimumHeight(40);
                        packageAmountCell.setPaddingTop(13);
                        packageAmountCell.setHorizontalAlignment(Element.ALIGN_LEFT);
                        pdfTable.addCell(packageAmountCell);

                        PdfPCell packageAmountDetailsCell = new PdfPCell(new Phrase("" + taxArr[0] + "%"));
                        packageAmountDetailsCell.setBorder(Rectangle.TOP);
                        if (!backgroudColourSet) {
                            packageAmountDetailsCell.setBackgroundColor(myColor);
                        }
                        packageAmountDetailsCell.setMinimumHeight(40);
                        packageAmountDetailsCell.setPaddingTop(13);
                        packageAmountDetailsCell.setHorizontalAlignment(Element.ALIGN_LEFT);
                        pdfTable.addCell(packageAmountDetailsCell);

                        PdfPCell packageAmountTotalPriceCell = new PdfPCell(new Phrase("AUD " + df.format(Double.parseDouble(taxArr[1]))));
                        packageAmountTotalPriceCell.setBorder(Rectangle.TOP);
                        if (!backgroudColourSet) {
                            packageAmountTotalPriceCell.setBackgroundColor(myColor);
                        }
                        packageAmountTotalPriceCell.setMinimumHeight(40);
                        packageAmountTotalPriceCell.setPaddingTop(13);
                        packageAmountTotalPriceCell.setHorizontalAlignment(Element.ALIGN_LEFT);
                        pdfTable.addCell(packageAmountTotalPriceCell);
                        backgroudColourSet = true;

                        totalPaid += Double.parseDouble(taxArr[1]);
                    }
                }
            }

            PdfPCell packageAmountCell = new PdfPCell(new Phrase(" "));
            packageAmountCell.setBorder(Rectangle.TOP);
            if (!backgroudColourSet) {
                packageAmountCell.setBackgroundColor(myColor);
            }
            packageAmountCell.setMinimumHeight(40);
            packageAmountCell.setPaddingTop(13);
            packageAmountCell.setHorizontalAlignment(Element.ALIGN_LEFT);
            pdfTable.addCell(packageAmountCell);

            PdfPCell packageAmountitemPriceCell = new PdfPCell(new Phrase("Total "));
            packageAmountitemPriceCell.setBorder(Rectangle.TOP);
            if (!backgroudColourSet) {
                packageAmountitemPriceCell.setBackgroundColor(myColor);
            }
            packageAmountitemPriceCell.setMinimumHeight(40);
            packageAmountitemPriceCell.setPaddingTop(13);
            packageAmountitemPriceCell.setHorizontalAlignment(Element.ALIGN_LEFT);
            pdfTable.addCell(packageAmountitemPriceCell);

            PdfPCell packageAmountTotalPriceCell;
            
                //pdfTable.addCell("USD " + tmWellFormbenefitDiscount);
               packageAmountTotalPriceCell = new PdfPCell(new Phrase("AUD " + df.format(totalPaid)));
            packageAmountTotalPriceCell.setBorder(Rectangle.TOP);
            packageAmountTotalPriceCell.setMinimumHeight(40);
            packageAmountTotalPriceCell.setPaddingTop(13);
            if (!backgroudColourSet) {
                packageAmountTotalPriceCell.setBackgroundColor(myColor);
            }
            packageAmountTotalPriceCell.setHorizontalAlignment(Element.ALIGN_LEFT);
            pdfTable.addCell(packageAmountTotalPriceCell);

//            document.add(new Phrase("\n"));
//            document.add(new Phrase("\n"));
//            document.add(new Phrase("\n"));
//            document.add(new Phrase("\n"));
            PdfPTable TermCondition = new PdfPTable(1);
            PdfPCell privacyStatementBlankSpace = new PdfPCell(new Phrase(" "));
            privacyStatementBlankSpace.setBorder(Rectangle.NO_BORDER);
            if(pdfSignEnable){
               privacyStatementBlankSpace.setMinimumHeight(80);
            }else{
               privacyStatementBlankSpace.setMinimumHeight(140); 
            }
            TermCondition.addCell(privacyStatementBlankSpace);

            PdfPCell privacyStatementHeader = new PdfPCell(new Phrase("PRIVACY STATEMENT", normalBold));
            privacyStatementHeader.setBorder(Rectangle.NO_BORDER);
            privacyStatementHeader.setMinimumHeight(20);
            TermCondition.addCell(privacyStatementHeader);

            document.add(new Phrase("\n"));
            PdfPCell privacyStatement = new PdfPCell(new Phrase("Blue Bricks'S PRIVACY STATEMENT In its effort to ensure compliance to the Personal Data Protection Act 2010 (PDPA), Blue Bricks has put in place a personal data protection policy which shall govern the use and protection of your personal data as Blue Bricks's customer. For details of the policy, please refer to Blue Bricks's Privacy Statement at http://www.blue-bricks.com, which may be reviewed by Blue Bricks from time to time.   "));
//            TermCondition.addCell("\nPRIVACY STATEMENT"
//                    + " \n "                    
//                    + "Blue Bricks'S PRIVACY STATEMENT In its effort to ensure compliance to the Personal Data Protection Act 2010 (PDPA), Blue Bricks has put in place a personal data protection policy which shall govern the use and protection of your personal data as Blue Bricks's customer. For details of the policy, please refer to Blue Bricks's Privacy Statement at http://www.blue-bricks.com, which may be reviewed by Blue Bricks from time to time.   ");
            privacyStatement.setBorder(Rectangle.NO_BORDER);
            TermCondition.setWidthPercentage(100);
            TermCondition.addCell(privacyStatement);
            PdfPCell cell5 = new PdfPCell();
            cell5.setBorder(Rectangle.NO_BORDER);
            TermCondition.addCell(cell5);
            document.add(pdfTable);
            document.add(TermCondition);
            document.add(new Phrase("\n"));
//            document.add(new Phrase("\n"));
//            document.add(new Phrase("\n"));
//            document.add(new Phrase("\n"));

//            Paragraph par = new Paragraph("Blue Bricks Pty. Ltd. ", bold);
//
//            Paragraph par1 = new Paragraph("Unit 39, 118 Adderton Road, Carlingford 2118 NSW, Australia", normal1);
//            //Paragraph par2 = new Paragraph("Tel: +61 423 394 252 / +61 478 665 482", normal1);
//            Paragraph par3 = new Paragraph("WWW.blue-bricks.com", normal1);
            //document.add(par);
            Paragraph par1 = new Paragraph(" Note: This is system generated invoice and does not require stamp.", normalBold);
            document.add(par1);
            //document.add(par2);
            //document.add(par3);                                   
            document.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return filePath;
    }
    
    
}
