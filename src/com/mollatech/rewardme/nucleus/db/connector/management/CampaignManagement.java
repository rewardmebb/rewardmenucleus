/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector.management;

import com.mollatech.rewardme.nucleus.commons.GlobalStatus;
import com.mollatech.rewardme.nucleus.db.RmCampaigndetails;
import com.mollatech.rewardme.nucleus.db.connector.CampaignUtils;
import com.mollatech.rewardme.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.rewardme.nucleus.db.connector.SessionUtil;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author abhishekingle
 */
public class CampaignManagement {
    
    static final Logger logger = Logger.getLogger(CampaignManagement.class);

    public static final int ACTIVE_STATUS = 1;

    public static final int SUSPEND_STATUS = 0;

    public static final int APPROVED_STATUS = 9;

    public static final int UPDATED_REQUEST_STATUS = 2;

    public static final int REJECTED_STATUS = -1;

    public static final int REQUEST_ACTIVE_STATUS = 1;

    public static final int REQUEST_SUSPENDED_STATUS = 0;
    
    public int CreateCampaign(String sessionId, RmCampaigndetails campaignObj) {
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.campaignDetails);
        Session pSession = puSession.openSession();
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        CampaignUtils prtUtil = new CampaignUtils(puSession, pSession);
        int result = -1;
        try {
            if (res == ACTIVE_STATUS) {
                int update = sManagement.UpdateSession(sessionId);
                result = prtUtil.addCampaignDetails(campaignObj);
                return result;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Exception at CampaignManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
            sSession.close();
            suSession.close();
        }
        return result;
    }
            
    public RmCampaigndetails getCampaignByIdDetails(int campaignId) {
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.campaignDetails);
        Session pSession = puSession.openSession();
        CampaignUtils partUtil = new CampaignUtils(puSession, pSession);
        int result = -1;
        RmCampaigndetails pdetails = null;
        try {
            pdetails = partUtil.getCampaignById(campaignId);
            return pdetails;
        } catch (Exception ex) {
            logger.error("Exception at CampaignManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
        }
        return null;
    }
    
    public RmCampaigndetails getCampaignByUniqueIdDetails(String campaignUniqueId) {
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.campaignDetails);
        Session pSession = puSession.openSession();
        CampaignUtils partUtil = new CampaignUtils(puSession, pSession);
        int result = -1;
        RmCampaigndetails pdetails = null;
        try {
            pdetails = partUtil.getCampaignByUniqueId(campaignUniqueId);
            return pdetails;
        } catch (Exception ex) {
            logger.error("Exception at CampaignManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
        }
        return null;
    }
    
    public RmCampaigndetails[] getCampaignByOwnerId(int ownerId) {
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.campaignDetails);
        Session pSession = puSession.openSession();
        CampaignUtils partUtil = new CampaignUtils(puSession, pSession);
        int result = -1;
        RmCampaigndetails[] pdetails = null;
        try {
            pdetails = partUtil.getCampaignDetailsByOwnerId(ownerId);
            return pdetails;
        } catch (Exception ex) {
            logger.error("Exception at CampaignManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
        }
        return null;
    }
    
    public RmCampaigndetails[] getAllCampaignBySocialMediaUniqueId(String socialMediaUniqueId) {
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.campaignDetails);
        Session pSession = puSession.openSession();
        CampaignUtils partUtil = new CampaignUtils(puSession, pSession);
        int result = -1;
        RmCampaigndetails[] pdetails = null;
        try {
            pdetails = partUtil.getAllCampaignBySocialMediaUniqueId(socialMediaUniqueId);
            return pdetails;
        } catch (Exception ex) {
            logger.error("Exception at CampaignManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
        }
        return null;
    }
    
    public int updateDetails(RmCampaigndetails campaignDetails) {
        SessionFactoryUtil guSession = new SessionFactoryUtil(SessionFactoryUtil.campaignDetails);
        Session gSession = guSession.openSession();
        CampaignUtils campaignUtil = new CampaignUtils(guSession, gSession);
        int result = -1;
        try {
            result = campaignUtil.updateDetails(campaignDetails);
            return result;
        } catch (Exception ex) {
            logger.error("Exception at CampaignManagement ", ex);
        } finally {
            gSession.close();
            guSession.close();
        }
        return result;
    }
    
    
     public RmCampaigndetails[] getAllCampaignDetails(String sessionId) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.campaignDetails);
        Session pSession = puSession.openSession();
        CampaignUtils pUtil = new CampaignUtils(puSession, pSession);       
        RmCampaigndetails[] groups = null;
        try {
            if (res == GlobalStatus.ACTIVE) {
                int update = sManagement.UpdateSession(sessionId);
                groups = pUtil.getAllCampaignDetails();
                return groups;
            } else {
                return null;
            }
        } catch (Exception ex) {
            logger.error("Exception at CampaignManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
            sSession.close();
            suSession.close();
        }
        return null;
    }
}
