/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector.management;

import com.mollatech.rewardme.connector.communication.RMStatus;
import com.mollatech.rewardme.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.rewardme.nucleus.db.connector.SettingsUtil;
import com.mollatech.rewardme.nucleus.settings.SendNotification;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author abhishekingle
 */
public class SettingsManagement {
    
    static final Logger logger = Logger.getLogger(SettingsManagement.class);

    public static final int SMS = 1;

    public static final int USSD = 2;

    public static final int VOICE = 3;

    public static final int EMAIL = 4;

    public static final int PASSWORD_POLICY_SETTING = 5;

    public static final int MATERSLAVE = 6;

    public static final int MAKERCHAKER = 7;

    public static final int GlobalSettings = 8;

    public static final int ACTIVE_STATUS = 1;

    public static final int SUSPENDED_STATUS = 0;

    public static final int PREFERENCE_ONE = 1;

    public static final int PREFERENCE_TWO = 2;

    public int passwordType;

    public final int ENCRYPTED = 1;

    public final int HASHED = 2;

    public final int PLAIN = 3;

    public int algoType;

    public String algoAttrReserve1;

    public String algoAttrReserve2;

    public String algoAttrReserve3;

    public String algoAttrReserve4;

    public final int AES_ENCRYPTION = 1;

    public final int BLOWFISH_ENCRYPTION = 2;

    public final int DES_ENCRYPTION = 3;

    public final int SHA1_HASH = 4;

    public final int MD5_HASH = 5;

    public final int SHA256_HASH = 6;

    public int addSetting(int iType, int iPreference, Object objSetting) {
        SessionFactoryUtil suSettings = new SessionFactoryUtil(SessionFactoryUtil.settings);
        Session sSettings = suSettings.openSession();
        try {
            SettingsUtil setUtil = new SettingsUtil(suSettings, sSettings);
            int result = setUtil.addSetting(iType, iPreference, objSetting);
            return result;
        } finally {
            sSettings.close();
            suSettings.close();
        }
    }

    public int addSetting(String sessionId,int iType, int iPreference, Object objSetting) {
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        SessionFactoryUtil suSettings = new SessionFactoryUtil(SessionFactoryUtil.settings);
        Session sSettings = suSettings.openSession();
        try {
            if (res == ACTIVE_STATUS) {
                int update = sManagement.UpdateSession(sessionId);
                SettingsUtil setUtil = new SettingsUtil(suSettings, sSettings);
                int result = setUtil.addSetting(iType, iPreference, objSetting);
                return result;
            } else {
                return -2;
            }
        } finally {
            sSettings.close();
            suSettings.close();
        }
    }

    public Object getSetting(String sessionId, int iType, int iPreference) {
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        SessionFactoryUtil suSettings = new SessionFactoryUtil(SessionFactoryUtil.settings);
        Session sSettings = suSettings.openSession();
        try {
            if (res == ACTIVE_STATUS) {
                int update = sManagement.UpdateSession(sessionId);
                SettingsUtil setUtil = new SettingsUtil(suSettings, sSettings);
                Object obj = setUtil.getSetting(iType, iPreference);
                return obj;
            } else {
                return null;
            }
        } finally {
            sSettings.close();
            suSettings.close();
        }
    }

    public int changeSetting(String sessionId, int iType, int iPreference, Object objOldSetting, Object objNewSetting) {
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        SessionFactoryUtil suSettings = new SessionFactoryUtil(SessionFactoryUtil.settings);
        Session sSettings = suSettings.openSession();
        try {
            if (res == ACTIVE_STATUS) {
                int update = sManagement.UpdateSession(sessionId);
                SettingsUtil setUtil = new SettingsUtil(suSettings, sSettings);
                int result = setUtil.changeSetting(iType, iPreference, objOldSetting, objNewSetting);
                return result;
            } else {
                return -2;
            }
        } finally {
            sSettings.close();
            suSettings.close();
        }
    }

    public Object[] getSettings(String sessionId) {
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        SessionFactoryUtil suSettings = new SessionFactoryUtil(SessionFactoryUtil.settings);
        Session sSettings = suSettings.openSession();
        try {
            if (res == ACTIVE_STATUS) {
                int update = sManagement.UpdateSession(sessionId);
                SettingsUtil setUtil = new SettingsUtil(suSettings, sSettings);
                Object[] objList = setUtil.getSettings();
                return objList;
            } else {
                return null;
            }
        } finally {
            sSettings.close();
            suSettings.close();
        }
    }

    public Object[] getSettings() {
        SessionFactoryUtil suSettings = new SessionFactoryUtil(SessionFactoryUtil.settings);
        Session sSettings = suSettings.openSession();
        try {
            SettingsUtil setUtil = new SettingsUtil(suSettings, sSettings);
            Object[] objList = setUtil.getSettings();
            return objList;
        } finally {
            sSettings.close();
            suSettings.close();
        }
    }

    public int getCount() {
        SessionFactoryUtil suSettings = new SessionFactoryUtil(SessionFactoryUtil.settings);
        Session sSettings = suSettings.openSession();
        try {
            SettingsUtil setUtil = new SettingsUtil(suSettings, sSettings);
            return setUtil.getCount();
        } catch (Exception ex) {
            logger.error("Exception at SettingsManagement ", ex);
            return -2;
        } finally {
            sSettings.close();
            suSettings.close();
        }
    }

    public Object getSettingInner(int iType, int iPreference) {
        SessionFactoryUtil suSettings = new SessionFactoryUtil(SessionFactoryUtil.settings);
        Session sSettings = suSettings.openSession();
        try {
            SettingsUtil setUtil = new SettingsUtil(suSettings, sSettings);
            Object obj = setUtil.getSetting(iType, iPreference);
            return obj;
        } catch (Exception e) {
            sSettings.close();
            suSettings.close();
            logger.error("Exception at SettingsManagement ", e);
            return null;
        }
    }

    public Object getSetting(int iType, int iPreference) {
        SessionFactoryUtil suSettings = new SessionFactoryUtil(SessionFactoryUtil.settings);
        Session sSettings = suSettings.openSession();
        try {
            SettingsUtil setUtil = new SettingsUtil(suSettings, sSettings);
            Object obj = setUtil.getSetting(iType, iPreference);
            return obj;
        } finally {
            sSettings.close();
            suSettings.close();
        }
    }
   

    public int updateSetting(int iType, int iPreference, Object objOldSetting, Object objNewSetting) {
        SessionFactoryUtil suSettings = new SessionFactoryUtil(SessionFactoryUtil.settings);
        Session sSettings = suSettings.openSession();
        try {
            SettingsUtil setUtil = new SettingsUtil(suSettings, sSettings);
            int result = setUtil.changeSetting(iType, iPreference, objOldSetting, objNewSetting);
            return result;
        } finally {
            sSettings.close();
            suSettings.close();
        }
    }

    public int testSetting(String sessionId, int iType, int iPreference, String sendto, String message) {
        SendNotification send = new SendNotification();
        if (iType == EMAIL) {
            RMStatus axStatus = send.SendOnEmailByPreference(sendto, "Test Email Message ", message, null, null, null, null, iType, iPreference, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
            if (axStatus.iStatus == 0) {
                return 0;
            } else if (axStatus.iStatus == -21) {
                return -4;
            } else {
                return -1;
            }
        } else if (iType == SMS) {
            RMStatus axStatus = send.SendOnMobileByPreference(sendto, message, iType, iPreference, Integer.parseInt(LoadSettings.g_sSettings.getProperty("product.type")));
            if (axStatus.iStatus == 0) {
                return 0;
            } else if (axStatus.iStatus == -22) {
                return -3;
            } else {
                return -1;
            }
        } else {
            return -2;
        }
    }    
}
