/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector.management;

import com.mollatech.rewardme.nucleus.commons.GlobalStatus;
import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
import com.mollatech.rewardme.nucleus.db.connector.BrandOwnerUtils;
import com.mollatech.rewardme.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.rewardme.nucleus.db.connector.SessionUtil;
import java.util.Date;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author abhishekingle
 */
public class BrandOwnerManagement {
    
    static final Logger logger = Logger.getLogger(BrandOwnerManagement.class);

    public static final int ACTIVE_STATUS = 1;

    public static final int SUSPEND_STATUS = 0;

    public static final int APPROVED_STATUS = 9;

    public static final int UPDATED_REQUEST_STATUS = 2;

    public static final int REJECTED_STATUS = -1;

    public static final int REQUEST_ACTIVE_STATUS = 1;

    public static final int REQUEST_SUSPENDED_STATUS = 0;
    
    public int CreateBrandOwner(String sessionId, RmBrandownerdetails brandownerObj) {
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.brandOwnerDetails);
        Session pSession = puSession.openSession();
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        BrandOwnerUtils prtUtil = new BrandOwnerUtils(puSession, pSession);
        int result = -1;
        try {
            if (res == ACTIVE_STATUS) {
                int update = sManagement.UpdateSession(sessionId);
                result = prtUtil.addBrandOwnerDetails(brandownerObj);
                return result;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Exception at BrandOwnerManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
            sSession.close();
            suSession.close();
        }
        return result;
    }
    
    public int checkIsUniqueInBrandOwner(String sessionId, String name, String mail, String phone) {
        int result = 0;
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        SessionFactoryUtil suOperator = new SessionFactoryUtil(SessionFactoryUtil.brandOwnerDetails);
        Session sOperator = suOperator.openSession();
        BrandOwnerUtils pUtil = new BrandOwnerUtils(suOperator, sOperator);
        try {
            if (res == GlobalStatus.ACTIVE) {
                int update = sManagement.UpdateSession(sessionId);
                result = 0;
                if (result == 0) {
                    result = pUtil.checkDuplicate(mail, 2);
                    if (result == 0) {
                        result = pUtil.checkDuplicate(phone, 3);
                    }
                }
            }
        } finally {
            sOperator.close();
            suOperator.close();
        }
        return result;
    }
    
    public RmBrandownerdetails getBrandOwnerDetails(int ownerid) {
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.brandOwnerDetails);
        Session pSession = puSession.openSession();
        BrandOwnerUtils partUtil = new BrandOwnerUtils(puSession, pSession);
        int result = -1;
        RmBrandownerdetails pdetails = null;
        try {
            pdetails = partUtil.getBrandOwnerbyId(ownerid);
            return pdetails;
        } catch (Exception ex) {
            logger.error("Exception at BrandOwnerManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
        }
        return null;
    }
    
    public int updateDetails(RmBrandownerdetails owner) {
        SessionFactoryUtil guSession = new SessionFactoryUtil(SessionFactoryUtil.brandOwnerDetails);
        Session gSession = guSession.openSession();
        BrandOwnerUtils otptrail = new BrandOwnerUtils(guSession, gSession);
        int result = -1;
        try {
            result = otptrail.updateDetails(owner);
            return result;
        } catch (Exception ex) {
            logger.error("Exception at BrandOwnerManagement ", ex);
        } finally {
            gSession.close();
            guSession.close();
        }
        return result;
    }
    
    public RmBrandownerdetails VerifyCredential(String sessionId,String name, String password) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionFactoryUtil suUser = new SessionFactoryUtil(SessionFactoryUtil.brandOwnerDetails);
        Session sUser = suUser.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);       
        try {
            if (res == ACTIVE_STATUS) {
                int update = sManagement.UpdateSession(sessionId);
                BrandOwnerUtils oUtil = new BrandOwnerUtils(suUser, sUser);
                RmBrandownerdetails SgUsers = oUtil.VerifyCredential(name, password);
                return SgUsers;
            }
            return null;
        } finally {
            sSession.close();
            suSession.close();
            sUser.close();
            suUser.close();
        }
    }
    
     public RmBrandownerdetails[] getAllBrandOwnerDetails(String sessionId) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.brandOwnerDetails);
        Session pSession = puSession.openSession();
        BrandOwnerUtils pUtil = new BrandOwnerUtils(puSession, pSession);       
        RmBrandownerdetails[] groups = null;
        try {
            if (res == GlobalStatus.ACTIVE) {
                int update = sManagement.UpdateSession(sessionId);
                groups = pUtil.getAllBrandOwnerDetails();
                return groups;
            } else {
                return null;
            }
        } catch (Exception ex) {
            logger.error("Exception at BrandOwnerManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
            sSession.close();
            suSession.close();
        }
        return null;
    }
   
    public int ChangebrandStatus(String sessionId, int _brandId, int status) {
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.brandOwnerDetails);
        Session pSession = puSession.openSession();
        BrandOwnerUtils pUtil = new BrandOwnerUtils(puSession, pSession);
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        int result = -1;
        try {
            if (res == GlobalStatus.ACTIVE) {
                int update = sManagement.UpdateSession(sessionId);
                result = pUtil.ChangeBrandStatus(_brandId, status);
                return result;
            } else {
                return -2;
            }
        } catch (Exception ex) {
            logger.error("Exception at BrandOwnerManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
            sSession.close();
            suSession.close();
        }
        return -1;
    } 
    
    public int SetPassword(String userId, String password) {
        SessionFactoryUtil suUser = new SessionFactoryUtil(SessionFactoryUtil.brandOwnerDetails);
        Session sUser = suUser.openSession();
        try {
            BrandOwnerUtils oUtil = new BrandOwnerUtils(suUser, sUser);
            int iResult = oUtil.changePassword(userId, password);
            if (iResult != 0) {
                return -iResult;
            }
            return 0;
        } finally {
            sUser.close();
            suUser.close();
        }
    }
    
    public RmBrandownerdetails initializeResetPassword(String sessionId, String name, String email, Date expiry) {
        SessionFactoryUtil suSession = new SessionFactoryUtil(SessionFactoryUtil.sessions);
        Session sSession = suSession.openSession();
        SessionFactoryUtil suOperator = new SessionFactoryUtil(SessionFactoryUtil.brandOwnerDetails);
        Session sOperator = suOperator.openSession();
        SessionUtil sUtil = new SessionUtil(suSession, sSession);
        SessionManagement sManagement = new SessionManagement();
        int res = sManagement.GetSessionStatus(sessionId);
        Session pSession = suOperator.openSession();
        try {
            if (res == GlobalStatus.ACTIVE) {
                int update = sManagement.UpdateSession(sessionId);
                BrandOwnerUtils oUtil = new BrandOwnerUtils(suOperator, sOperator);
                RmBrandownerdetails operators = oUtil.initializeResetPassword(name, email, expiry);
                return operators;
            }
            return null;
        } finally {
            sSession.close();
            suSession.close();
            sOperator.close();
            suOperator.close();
        }
    }
    
    public RmBrandownerdetails getOwnerDetailsByStripeCustomerId(String customerId) {
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.brandOwnerDetails);
        Session pSession = puSession.openSession();
        BrandOwnerUtils partUtil = new BrandOwnerUtils(puSession, pSession);       
        RmBrandownerdetails pdetails = null;
        try {
            pdetails = partUtil.getOwnerbyCustomerId(customerId);
            return pdetails;
        } catch (Exception ex) {
            logger.error("Exception at BrandOwnerManagement ", ex);
        } finally {
            pSession.close();
            puSession.close();
        }
        return null;
    }
}
