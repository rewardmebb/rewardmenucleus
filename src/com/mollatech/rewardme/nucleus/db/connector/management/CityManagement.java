/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector.management;

import com.mollatech.rewardme.nucleus.db.RmCities;
import com.mollatech.rewardme.nucleus.db.connector.CityUtils;
import com.mollatech.rewardme.nucleus.db.connector.SessionFactoryUtil;
import org.apache.log4j.Logger;
import org.hibernate.Session;

/**
 *
 * @author abhishekingle
 */
public class CityManagement {
    
    static final Logger logger = Logger.getLogger(CityManagement.class);
    
    public RmCities getCityDetailsById(int stateId) {
        SessionFactoryUtil puSession = new SessionFactoryUtil(SessionFactoryUtil.city);
        Session pSession = puSession.openSession();
        CityUtils partUtil = new CityUtils(puSession, pSession);       
        RmCities pdetails = null;
        try {
            pdetails = partUtil.getCityById(stateId);
            return pdetails;
        } catch (Exception ex) {
            logger.error("Exception at CityManagement", ex);
        } finally {
            pSession.close();
            puSession.close();
        }
        return null;
    }
}
