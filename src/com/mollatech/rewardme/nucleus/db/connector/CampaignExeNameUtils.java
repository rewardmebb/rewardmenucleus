/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector;

import com.mollatech.rewardme.nucleus.db.RmCampaignExecutionName;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author abhishekingle
 */
public class CampaignExeNameUtils {
    
    private SessionFactoryUtil m_su;

    private Session m_session;

    static final Logger logger = Logger.getLogger(CampaignExeNameUtils.class);

    public CampaignExeNameUtils(SessionFactoryUtil su, Session session) {
        m_su = su;
        m_session = session;
    }

    public int addCampaignExeNameDetails(RmCampaignExecutionName campaignObj) {
        Transaction tx = null;
        try {
            tx = m_session.beginTransaction();
            Serializable s = m_session.save(campaignObj);
            tx.commit();
            return (Integer) s;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception at CampaignExeNameUtils ", e);
            return -1;
        }
    }        
        
    public int updateDetails(RmCampaignExecutionName campaignDetails) {
        try {
            Transaction tx = m_session.beginTransaction();
            m_session.update(campaignDetails);
            tx.commit();
            return 0;
        } catch (Exception ex) {
            logger.error("Exception at CampaignExeNameUtils ", ex);
            return -1;
        }
    }        
        
    public RmCampaignExecutionName[] getCampaignDetailsByOwnerId(int ownerId) {
        RmCampaignExecutionName[] pObj = null;
        Criteria criteria = m_session.createCriteria(RmCampaignExecutionName.class);        
        //criteria.add(Restrictions.ne("status", GlobalStatus.DELETED));
        criteria.add(Restrictions.eq("ownerId", ownerId));
        List list = criteria.list();
        if (list.isEmpty() != true) {
            try {
                pObj = new RmCampaignExecutionName[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    pObj[i] = (RmCampaignExecutionName) list.get(i);
                }
                return pObj;
            } catch (Exception e) {
                logger.error("Exception at CampaignExeNameUtils ", e);
                return null;
            }
        }
        return null;
    }
    
    public RmCampaignExecutionName getCampaignDetailsByExename(int ownerId, String exeName) {
        RmCampaignExecutionName pObj = null;
        Criteria criteria = m_session.createCriteria(RmCampaignExecutionName.class);                
        criteria.add(Restrictions.eq("campaignExeName", exeName));
        criteria.add(Restrictions.eq("ownerId", ownerId));
        List list = criteria.list();
        if (list.isEmpty() != true) {
            try {
                pObj = (RmCampaignExecutionName) list.get(0);                
                return pObj;
            } catch (Exception e) {
                logger.error("Exception at CampaignExeNameUtils ", e);
                return null;
            }
        }
        return null;
    }
    
    public RmCampaignExecutionName[] getTxDetails(Integer ownerId, String startdate, String enddate, String stime, String etime) {
        try {
            Calendar current = Calendar.getInstance();
            Calendar currenttime = Calendar.getInstance();
            DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            DateFormat timeformat = new SimpleDateFormat("hh:mm a");
            Date sttime = null;
            Criteria criteria = m_session.createCriteria(RmCampaignExecutionName.class);
            if (startdate != null && !startdate.isEmpty()) {
                sttime = (Date) timeformat.parse(stime);
            }
            Date entime = null;
            if (startdate != null && !startdate.isEmpty()) {
                entime = (Date) timeformat.parse(etime);
            }
            Date startDate = null;
            if (startdate != null && !startdate.isEmpty()) {
                startDate = (Date) formatter.parse(startdate);
            }
            Date endDate = null;
            if (enddate != null && !enddate.isEmpty()) {
                endDate = (Date) formatter.parse(enddate);
            }
            if (startdate != null || enddate != null) {
                current.setTime(endDate);
                currenttime.setTime(entime);
                current.set(Calendar.AM_PM, currenttime.get(Calendar.AM_PM));
                current.set(Calendar.HOUR, currenttime.get(Calendar.HOUR));
                current.set(Calendar.MINUTE, currenttime.get(Calendar.MINUTE));
                Date endDates = current.getTime();
                current.setTime(startDate);
                currenttime.setTime(sttime);
                current.set(Calendar.AM_PM, currenttime.get(Calendar.AM_PM));
                current.set(Calendar.HOUR, currenttime.get(Calendar.HOUR));
                current.set(Calendar.MINUTE, currenttime.get(Calendar.MINUTE));
                Date startDates = current.getTime();
//                criteria.add(Restrictions.gt("creationDate", startDates));
//                criteria.add(Restrictions.lt("creationDate", endDates));
                criteria.add(Restrictions.between("creationDate",startDates, endDates));
            }
            criteria.add(Restrictions.eq("ownerId", ownerId));
            List list = criteria.list();
            if (list != null && !list.isEmpty()) {
                RmCampaignExecutionName[] transcationdetailses = new RmCampaignExecutionName[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    transcationdetailses[i] = (RmCampaignExecutionName) list.get(i);
                }
                return transcationdetailses;
            } else {
                return null;
            }
        } catch (Exception ex) {
            logger.error("Exception at CampaignExeNameUtils ", ex);
            return null;
        }
    }
}
