/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector;

import com.mollatech.rewardme.nucleus.db.RmSubscriptionpackagedetails;
import java.io.Serializable;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author abhishekingle
 */
public class SubscriptionUtils {
    
    private SessionFactoryUtil m_su;

    private Session m_session;

    static final Logger logger = Logger.getLogger(SubscriptionUtils.class);

    public SubscriptionUtils(SessionFactoryUtil su, Session session) {
        m_su = su;
        m_session = session;
    }
    
    int count = 0;

    public int addSubscriptionDetails(RmSubscriptionpackagedetails subObj) {
        if (count >= 5) {
            return -1;
        }
        Transaction tx = null;
        try {
            tx = m_session.beginTransaction();
            Serializable s = m_session.save(subObj);
            tx.commit();
            return (Integer) s;
        } catch (org.hibernate.exception.ConstraintViolationException e) {
            count++;
            int i = addSubscriptionDetails(subObj);
            return i;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception at SubscriptionUtils ", e);
            return -1;
        }
    }
    
    public int updateDetails(RmSubscriptionpackagedetails subObj) {
        Transaction tx = null;
        try {
            tx = m_session.beginTransaction();
            m_session.update(subObj);
            tx.commit();
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            tx.rollback();
            logger.error("Exception at SubscriptionUtils ", e);
        }
        return -1;
    }
    
    public RmSubscriptionpackagedetails getSubscriptionbyOwnerId(int ownerid) {
        RmSubscriptionpackagedetails pdetails;
        try {
            Criteria criteria = m_session.createCriteria(RmSubscriptionpackagedetails.class).addOrder(Order.desc("creationDate"));
            criteria.add(Restrictions.eq("ownerId", ownerid));
            List list = criteria.list();
            if (list.isEmpty() == true) {
                return null;
            } else {
                pdetails = (RmSubscriptionpackagedetails) list.get(0);
                return pdetails;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception at SubscriptionUtils ", e);
        }
        return null;
    }
    
    public RmSubscriptionpackagedetails getSubscriptionbySubscriptionId(int subscriptionid) {
        RmSubscriptionpackagedetails pdetails;
        try {
            Criteria criteria = m_session.createCriteria(RmSubscriptionpackagedetails.class).addOrder(Order.desc("creationDate"));
            criteria.add(Restrictions.eq("subscriptionId", subscriptionid));
            List list = criteria.list();
            if (list.isEmpty() == true) {
                return null;
            } else {
                pdetails = (RmSubscriptionpackagedetails) list.get(0);
                return pdetails;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception at SubscriptionUtils ", e);
        }
        return null;
    }
    
    public RmSubscriptionpackagedetails[] listOfPackageSubscripedbyOwnerId(int ownerid) {
        RmSubscriptionpackagedetails[] pdetails = null;
        try {
            Criteria criteria = m_session.createCriteria(RmSubscriptionpackagedetails.class).addOrder(Order.desc("creationDate"));
            criteria.add(Restrictions.eq("ownerId", ownerid));
            List list = criteria.list();
            if (list.isEmpty() == true) {
                return null;
            } else {
                pdetails = new RmSubscriptionpackagedetails[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    pdetails[i] = (RmSubscriptionpackagedetails) list.get(i);
                }
                return pdetails;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception at SubscriptionUtils ", e);
        }
        return null;
    } 
}
