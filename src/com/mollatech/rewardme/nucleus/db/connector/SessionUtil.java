/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector;

import com.mollatech.rewardme.nucleus.db.RmSessions;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author abhishekingle
 */
public class SessionUtil {
    
    private SessionFactoryUtil m_su;

    private Session m_session;

    static final int ACTIVE_STATUS = 1;

    static final Logger logger = Logger.getLogger(SessionUtil.class);

    public SessionUtil(SessionFactoryUtil su, Session session) {
        m_su = su;
        m_session = session;
    }

    public int addSession(String sessionid, String loginId, int status, Date startedOn, Integer expiryInMins, Date lastUpdateOn) {
        RmSessions sObj = new RmSessions();       
        sObj.setSessionid(sessionid);
        sObj.setLoginid(loginId);
        sObj.setStatus(status);
        sObj.setStartedOn(startedOn);
        sObj.setExpiryInMins(expiryInMins);
        sObj.setLastUpdateOn(lastUpdateOn);
        Transaction tx = null;
        try {
            tx = m_session.beginTransaction();
            m_session.save(sObj);
            tx.commit();
            return 0;
        } catch (Exception e) {
            logger.error("Exception at SessionUtil ", e);
        }
        return -1;
    }

    public RmSessions getSession(String sessionId) {
        try {
            Criteria criteria = m_session.createCriteria(RmSessions.class);
            criteria.add(Restrictions.eq("sessionid", sessionId));           
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmSessions sObj = (RmSessions) list.get(0);
                return sObj;
            }
        } catch (Exception e) {
            logger.error("Exception at SessionUtil ", e);
        }
        return null;
    }

    public int updateSession(String sessionId) {
        try {
            int status = 1;
            RmSessions sObj = new RmSessions();
            Criteria criteria = m_session.createCriteria(RmSessions.class);
            criteria.add(Restrictions.eq("sessionid", sessionId));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return -2;
            } else {
                sObj = (RmSessions) list.get(0);
                sObj.setStatus(status);
                sObj.setLastUpdateOn(new Date());
                Transaction tx = null;
                try {
                    tx = m_session.beginTransaction();
                    m_session.save(sObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    logger.error("Exception at SessionUtil ", e);
                }
            }
        } catch (Exception e) {
            logger.error("Exception at SessionUtil ", e);
        }
        return -2;
    }

    public int updateLastAccessOn(String sessionId, Date lastAccessOnDate) {
        try {
            RmSessions sObj = new RmSessions();
            Criteria criteria = m_session.createCriteria(RmSessions.class);
            criteria.add(Restrictions.eq("sessionid", sessionId));            
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return -2;
            } else if (lastAccessOnDate != null) {
                sObj = (RmSessions) list.get(0);
                sObj.setLastUpdateOn(lastAccessOnDate);
                Transaction tx = null;
                try {
                    tx = m_session.beginTransaction();
                    m_session.save(sObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    logger.error("Exception at SessionUtil ", e);
                }
            } else {
                return -1;
            }
        } catch (Exception e) {
            logger.error("Exception at SessionUtil ", e);
        }
        return -2;
    }

    public RmSessions[] getAllRmSessions(String sessionId) {
        try {
            Criteria criteria = m_session.createCriteria(RmSessions.class);           
            criteria.add(Restrictions.eq("sessionId", sessionId));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmSessions[] sessionsList = new RmSessions[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    sessionsList[i] = (RmSessions) list.get(0);
                }
                return sessionsList;
            }
        } catch (Exception e) {
            logger.error("Exception at SessionUtil ", e);
        }
        return null;
    }

    public int deleteSession(String sessionId) {
        try {
            Criteria criteria = m_session.createCriteria(RmSessions.class);
            criteria.add(Restrictions.eq("sessionid", sessionId));            
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return -1;
            } else {
                RmSessions sObj = (RmSessions) list.get(0);
                Transaction tx = null;
                try {
                    tx = m_session.beginTransaction();
                    m_session.delete(tx);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    logger.error("Exception at SessionUtil ", e);
                }
            }
        } catch (Exception e) {
            logger.error("Exception at SessionUtil ", e);
        }
        return -2;
    }

//    public RmSessions getSession(String sessionId) {
//        try {
//            Criteria criteria = m_session.createCriteria(RmSessions.class);
//            criteria.add(Restrictions.eq("sessionid", sessionId));
//            List list = criteria.list();
//            if (list == null || list.isEmpty() == true) {
//                return null;
//            } else {
//                RmSessions sObj = (RmSessions) list.get(0);
//                return sObj;
//            }
//        } catch (Exception e) {
//            logger.error("Exception at SessionUtil ", e);
//        }
//        return null;
//    }

    public int updateStatus(String sessionId, int iStatus) {
        try {
            RmSessions sObj = new RmSessions();
            Criteria criteria = m_session.createCriteria(RmSessions.class);
            criteria.add(Restrictions.eq("sessionid", sessionId));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return -2;
            } else {
                sObj = (RmSessions) list.get(0);
                sObj.setStatus(iStatus);
                Transaction tx = null;
                try {
                    tx = m_session.beginTransaction();
                    m_session.save(sObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    logger.error("Exception at SessionUtil ", e);
                }
            }
        } catch (Exception e) {
            logger.error("Exception at SessionUtil ", e);
        }
        return -1;
    }

    public RmSessions[] getRmSessions() {
        try {
            RmSessions sObj[] = null;
            Criteria criteria = m_session.createCriteria(RmSessions.class);
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                sObj = new RmSessions[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    sObj[i] = (RmSessions) list.get(i);
                }
                return sObj;
            }
        } catch (Exception e) {
            logger.error("Exception at SessionUtil ", e);
        }
        return null;
    }

    public int getCount() {
        try {
            Criteria criteria = m_session.createCriteria(RmSessions.class);
            Integer totalResult = ((Number) criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
            return totalResult;
        } catch (Exception e) {
            logger.error("Exception at SessionUtil ", e);
            return -2;
        }
    }    

    public int getTotalSessions() {
        try {
            Criteria criteria = m_session.createCriteria(RmSessions.class);            
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return 0;
            } else {
                return list.size();
            }
        } catch (Exception e) {
            logger.error("Exception at SessionUtil ", e);
        }
        return -1;
    }

    public RmSessions getValidSession(String sessionId) {
        try {
            Criteria criteria = m_session.createCriteria(RmSessions.class);
            criteria.add(Restrictions.eq("sessionid", sessionId));
            criteria.add(Restrictions.eq("status", ACTIVE_STATUS));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmSessions sObj = (RmSessions) list.get(0);
                return sObj;
            }
        } catch (Exception e) {
            logger.error("Exception at SessionUtil ", e);
        }
        return null;
    }
    
    public RmSessions[] getSessions() {
        try {
            RmSessions sObj[] = null;
            Criteria criteria = m_session.createCriteria(RmSessions.class);
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                sObj = new RmSessions[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    sObj[i] = (RmSessions) list.get(i);
                }
                return sObj;
            }
        } catch (Exception e) {
            logger.error("Exception at SessionUtil ", e);
        }
        return null;
    }
}
