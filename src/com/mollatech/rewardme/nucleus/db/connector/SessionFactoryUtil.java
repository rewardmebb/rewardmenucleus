/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector;

import java.util.Iterator;
import java.util.Set;
import org.hibernate.Session;
import org.hibernate.cfg.AnnotationConfiguration;

/**
 *
 * @author abhishekingle
 */
public class SessionFactoryUtil extends SessionFactory{
    
    public static org.hibernate.SessionFactory sessionFactory;
    
//    public static Properties stat_Properties = null;
    
    public static final int aiTrackingCampaignDetails = 1;

    public static final int approvedPackageDetails = 2;

    public static final int brandOwnerDetails = 3;

    public static final int campaignDetails = 4;

    public static final int creditInfo = 5;

    public static final int operators = 6;

    public static final int paymentDetails = 7;

    public static final int remoteaccess = 8;   

    public static final int roles = 9;

    public static final int sessions = 10;

    public static final int settings = 11;

    public static final int socialMediaDetails = 12;
    
    public static final int subscriptionPackageDetails = 13;
    
    public static final int unApprovalPackageDetails = 14;
    
    public static final int channellogs = 15;
    
    public static final int country = 16;
    
    public static final int campaignExecutionName = 17;
    
    public static final int state = 18;
    
    public static final int city = 19;
    
    public org.hibernate.Session m_session = null;

    public int m_iType = -1;
    
    public SessionFactoryUtil(int iType) {
        if (sessionFactory != null) {
            return;
        }
        m_iType = iType;
        try {
            AnnotationConfiguration annotationConfig = new AnnotationConfiguration();
            Set set = stat_Properties.keySet();
            Iterator itr = set.iterator();
            while (itr.hasNext()) {
                String key = (String) itr.next();
                String value = (String) stat_Properties.get(key);
                annotationConfig.setProperty(key, value);
            }
            annotationConfig.setProperty("javax.persistence.validation.mode", "NONE");
            annotationConfig.addPackage("com/mollatech/rewardme/nucleus/db/");
            annotationConfig.addResource("com/mollatech/rewardme/nucleus/db/RmAiTrackingCampaignDetails.hbm.xml");
            annotationConfig.addResource("com/mollatech/rewardme/nucleus/db/RmApprovedpackagedetails.hbm.xml");
            annotationConfig.addResource("com/mollatech/rewardme/nucleus/db/RmBrandownerdetails.hbm.xml");
            annotationConfig.addResource("com/mollatech/rewardme/nucleus/db/RmCampaigndetails.hbm.xml");
            annotationConfig.addResource("com/mollatech/rewardme/nucleus/db/RmCreditinfo.hbm.xml");
            annotationConfig.addResource("com/mollatech/rewardme/nucleus/db/RmOperators.hbm.xml");
            annotationConfig.addResource("com/mollatech/rewardme/nucleus/db/RmPaymentdetails.hbm.xml");
            annotationConfig.addResource("com/mollatech/rewardme/nucleus/db/RmRemoteaccess.hbm.xml");
            annotationConfig.addResource("com/mollatech/rewardme/nucleus/db/RmRoles.hbm.xml");
            annotationConfig.addResource("com/mollatech/rewardme/nucleus/db/RmSessions.hbm.xml"); 
            annotationConfig.addResource("com/mollatech/rewardme/nucleus/db/RmSettings.hbm.xml"); 
            annotationConfig.addResource("com/mollatech/rewardme/nucleus/db/RmSocialmediadetails.hbm.xml");
            annotationConfig.addResource("com/mollatech/rewardme/nucleus/db/RmSubscriptionpackagedetails.hbm.xml");            
            annotationConfig.addResource("com/mollatech/rewardme/nucleus/db/RmUnapprovalpackagedetails.hbm.xml");
            annotationConfig.addResource("com/mollatech/rewardme/nucleus/db/RmChannellogs.hbm.xml");
            annotationConfig.addResource("com/mollatech/rewardme/nucleus/db/RmCountries.hbm.xml");
            annotationConfig.addResource("com/mollatech/rewardme/nucleus/db/RmStates.hbm.xml");
            annotationConfig.addResource("com/mollatech/rewardme/nucleus/db/RmCities.hbm.xml");
            annotationConfig.addResource("com/mollatech/rewardme/nucleus/db/RmCampaignExecutionName.hbm.xml");
            sessionFactory = annotationConfig.buildSessionFactory();
        } catch (Exception e) {
            logger.error("Exception at SessionFactoryUtil For Creating Session ", e);
            e.printStackTrace();
        }
    }
    
    public Session openSession() {
        try {
            m_session = sessionFactory.openSession();
            if (m_session.isConnected()) {
                return m_session;
            } else {
                throw new Exception("Connection Closed.");
            }
        } catch (Exception ex) {
            logger.error("Exception at SessionFactoryUtil ", ex);
            synchronized (this) {
                try {
                    m_session = sessionFactory.openSession();
                } catch (Exception e) {
                    logger.error("Exception at SessionFactoryUtil For Obtaining Session ", e);
                    sessionFactory = null;
                    new SessionFactoryUtil(1);
                    m_session = sessionFactory.openSession();
                }
            }
        }
        return m_session;
    }
    
    public void close() {
        try {
            m_session.disconnect();
//            m_session.close();
        } catch (Exception e) {
            m_session = null;
        }
    }
}
