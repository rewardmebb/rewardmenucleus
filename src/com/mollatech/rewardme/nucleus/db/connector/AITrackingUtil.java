/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector;

import com.mollatech.rewardme.nucleus.commons.GlobalStatus;
import com.mollatech.rewardme.nucleus.db.RmAiTrackingCampaignDetails;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author abhishekingle
 */
public class AITrackingUtil {
    
    private SessionFactoryUtil m_su;

    private Session m_session;

    static final Logger logger = Logger.getLogger(AITrackingUtil.class);

    public AITrackingUtil(SessionFactoryUtil su, Session session) {
        m_su = su;
        m_session = session;
    }

    public int addTracking(RmAiTrackingCampaignDetails x) {
        Transaction tx = null;
        try {
            tx = m_session.beginTransaction();
            m_session.save(x);
            tx.commit();
            return 0;
        } catch (Exception e) {
            logger.error("Exception at AITrackingUtil ", e);
        }
        return -1;
    }

    public int UpdateTracking(RmAiTrackingCampaignDetails x) {
        Transaction tx = null;
        try {

            tx = m_session.beginTransaction();
            m_session.update(x);

            tx.commit();
            return 0;
        } catch (Exception e) {
            logger.error("Exception at AITrackingUtil ", e);
        }
        return -1;
    }    

    public RmAiTrackingCampaignDetails[] getaitracking() {
        try {
            Criteria criteria = null;
            criteria = m_session.createCriteria(RmAiTrackingCampaignDetails.class);
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmAiTrackingCampaignDetails[] mList = new RmAiTrackingCampaignDetails[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    mList[i] = (RmAiTrackingCampaignDetails) list.get(i);
                }
                return mList;
            }
        } catch (Exception e) {
            logger.error("Exception at AITrackingUtil ", e);
        }
        return null;
    }

    public int getCount() {
        try {
            Criteria criteria = null;
            criteria = m_session.createCriteria(RmAiTrackingCampaignDetails.class);
            Integer totalResult = ((Number) criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
            return totalResult;
        } catch (Exception e) {
            logger.error("Exception at AITrackingUtil ", e);
            return -2;
        }
    }
    
    public RmAiTrackingCampaignDetails[] getTxDetails(Integer ownerId, String startdate, String enddate, String stime, String etime) {
        try {
            Calendar current = Calendar.getInstance();
            Calendar currenttime = Calendar.getInstance();
            DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            DateFormat timeformat = new SimpleDateFormat("hh:mm a");
            Date sttime = null;
            Criteria criteria = m_session.createCriteria(RmAiTrackingCampaignDetails.class);
            if (startdate != null && !startdate.isEmpty()) {
                sttime = (Date) timeformat.parse(stime);
            }
            Date entime = null;
            if (startdate != null && !startdate.isEmpty()) {
                entime = (Date) timeformat.parse(etime);
            }
            Date startDate = null;
            if (startdate != null && !startdate.isEmpty()) {
                startDate = (Date) formatter.parse(startdate);
            }
            Date endDate = null;
            if (enddate != null && !enddate.isEmpty()) {
                endDate = (Date) formatter.parse(enddate);
            }
            if (startdate != null || enddate != null) {
                current.setTime(endDate);
                currenttime.setTime(entime);
                current.set(Calendar.AM_PM, currenttime.get(Calendar.AM_PM));
                current.set(Calendar.HOUR, currenttime.get(Calendar.HOUR));
                current.set(Calendar.MINUTE, currenttime.get(Calendar.MINUTE));
                Date endDates = current.getTime();
                current.setTime(startDate);
                currenttime.setTime(sttime);
                current.set(Calendar.AM_PM, currenttime.get(Calendar.AM_PM));
                current.set(Calendar.HOUR, currenttime.get(Calendar.HOUR));
                current.set(Calendar.MINUTE, currenttime.get(Calendar.MINUTE));
                Date startDates = current.getTime();
                criteria.add(Restrictions.gt("creationDate", startDates));
                criteria.add(Restrictions.lt("creationDate", endDates));
            }
            criteria.add(Restrictions.eq("ownerId", ownerId));
            List list = criteria.list();
            if (list != null && !list.isEmpty()) {
                RmAiTrackingCampaignDetails[] transcationdetailses = new RmAiTrackingCampaignDetails[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    transcationdetailses[i] = (RmAiTrackingCampaignDetails) list.get(i);
                }
                return transcationdetailses;
            } else {
                return null;
            }
        } catch (Exception ex) {
            logger.error("Exception at AITrackingUtil ", ex);
            return null;
        }
    }
    
    public RmAiTrackingCampaignDetails[] getTxDetails(Integer ownerId, String startdate, String enddate, String stime, String etime,String campExeName, Integer campId) {
        try {
            Calendar current = Calendar.getInstance();
            Calendar currenttime = Calendar.getInstance();
            DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            DateFormat timeformat = new SimpleDateFormat("hh:mm a");
            Date sttime = null;
            Criteria criteria = m_session.createCriteria(RmAiTrackingCampaignDetails.class);
            if (startdate != null && !startdate.isEmpty()) {
                sttime = (Date) timeformat.parse(stime);
            }
            Date entime = null;
            if (startdate != null && !startdate.isEmpty()) {
                entime = (Date) timeformat.parse(etime);
            }
            Date startDate = null;
            if (startdate != null && !startdate.isEmpty()) {
                startDate = (Date) formatter.parse(startdate);
            }
            Date endDate = null;
            if (enddate != null && !enddate.isEmpty()) {
                endDate = (Date) formatter.parse(enddate);
            }
            if (startdate != null || enddate != null) {
                current.setTime(endDate);
                currenttime.setTime(entime);
                current.set(Calendar.AM_PM, currenttime.get(Calendar.AM_PM));
                current.set(Calendar.HOUR, currenttime.get(Calendar.HOUR));
                current.set(Calendar.MINUTE, currenttime.get(Calendar.MINUTE));
                Date endDates = current.getTime();
                current.setTime(startDate);
                currenttime.setTime(sttime);
                current.set(Calendar.AM_PM, currenttime.get(Calendar.AM_PM));
                current.set(Calendar.HOUR, currenttime.get(Calendar.HOUR));
                current.set(Calendar.MINUTE, currenttime.get(Calendar.MINUTE));
                Date startDates = current.getTime();
                criteria.add(Restrictions.gt("creationDate", startDates));
                criteria.add(Restrictions.lt("creationDate", endDates));
            }
            criteria.add(Restrictions.eq("ownerId", ownerId));
            criteria.add(Restrictions.eq("campaignExecutedId", campExeName));
            criteria.add(Restrictions.eq("campaignId", campId));
            List list = criteria.list();
            if (list != null && !list.isEmpty()) {
                RmAiTrackingCampaignDetails[] transcationdetailses = new RmAiTrackingCampaignDetails[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    transcationdetailses[i] = (RmAiTrackingCampaignDetails) list.get(i);
                }
                return transcationdetailses;
            } else {
                return null;
            }
        } catch (Exception ex) {
            logger.error("Exception at AITrackingUtil ", ex);
            return null;
        }
    }
    
    
    public RmAiTrackingCampaignDetails[] getTxDetails(Integer advertisorAdId, Date startDate, Date endDate) {
        try {
            Criteria criteria = m_session.createCriteria(RmAiTrackingCampaignDetails.class);
            if (startDate != null) {
                criteria.add(Restrictions.between("creationDate", startDate, endDate));
            } else {
                criteria.add(Restrictions.le("creationDate", endDate));
            }
            criteria.add(Restrictions.eq("ownerId", advertisorAdId));            
            List list = criteria.list();
            if (list != null && !list.isEmpty()) {
                RmAiTrackingCampaignDetails[] transcationdetailses = new RmAiTrackingCampaignDetails[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    transcationdetailses[i] = (RmAiTrackingCampaignDetails) list.get(i);
                }
                return transcationdetailses;
            } else {
                return null;
            }
        } catch (Exception ex) {
            logger.error("Exception at AITrackingUtil ", ex);
            return null;
        }
    }
    
    public RmAiTrackingCampaignDetails[] getTxDetailsByAdvertisorId(Integer ownerId, Date startDate, Date endDate) {
        try {
            Criteria criteria = m_session.createCriteria(RmAiTrackingCampaignDetails.class);
            if (startDate != null) {
                criteria.add(Restrictions.between("creationDate", startDate, endDate));
            } else {
                criteria.add(Restrictions.le("creationDate", endDate));
            }
            criteria.add(Restrictions.eq("ownerId", ownerId));
            
            List list = criteria.list();
            if (list != null && !list.isEmpty()) {
                RmAiTrackingCampaignDetails[] transcationdetailses = new RmAiTrackingCampaignDetails[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    transcationdetailses[i] = (RmAiTrackingCampaignDetails) list.get(i);
                }
                return transcationdetailses;
            } else {
                return null;
            }
        } catch (Exception ex) {
            logger.error("Exception at AITrackingUtil ", ex);
            return null;
        }
    }
    
    public RmAiTrackingCampaignDetails[] getTxDetailsByOwnerIdAndCampaignId(Integer ownerId, Date startDate, Date endDate, Integer campaignId) {
        try {
            Criteria criteria = m_session.createCriteria(RmAiTrackingCampaignDetails.class);
            if (startDate != null) {
                criteria.add(Restrictions.between("creationDate", startDate, endDate));
            } else {
                criteria.add(Restrictions.le("creationDate", endDate));
            }
            criteria.add(Restrictions.eq("ownerId", ownerId));
            criteria.add(Restrictions.eq("campaignId", campaignId));
            List list = criteria.list();
            if (list != null && !list.isEmpty()) {
                RmAiTrackingCampaignDetails[] transcationdetailses = new RmAiTrackingCampaignDetails[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    transcationdetailses[i] = (RmAiTrackingCampaignDetails) list.get(i);
                }
                return transcationdetailses;
            } else {
                return null;
            }
        } catch (Exception ex) {
            logger.error("Exception at AITrackingUtil ", ex);
            return null;
        }
    }
    
    public List<Object[]> getCountUsingHQL(Integer advertiserIds, Date sDate, Date eDate, boolean equal) {
        Query query = null;
        
            if (!equal) {
                query = m_session.createQuery("SELECT count(e.campaignId),e.campaignId FROM RmAiTrackingCampaignDetails e where e.ownerId != :ownerId and e.creationDate BETWEEN :stDate AND :edDate GROUP BY e.campaignId , e.ownerId ORDER BY  count(e.campaignId) DESC");
            } else {
                query = m_session.createQuery("SELECT count(e.campaignId),e.campaignId FROM RmAiTrackingCampaignDetails e where e.ownerId = :ownerId and e.creationDate BETWEEN :stDate AND :edDate GROUP BY e.campaignId , e.ownerId ORDER BY  count(e.campaignId) DESC");
            }
        
        query.setParameter("stDate", sDate)
                .setParameter("edDate", eDate).setParameter("ownerId", advertiserIds);
        List<Object[]> list = query.list();
        return list;
    }
    
    public List<Object[]> getCountUsingHQLForRealTime(Integer ownerIds, Date sDate, Date eDate, boolean equal) {
        Query query = null;
        Calendar current = Calendar.getInstance();
        current.setTime(sDate);
        Date startDate = current.getTime();
        current.setTime(eDate);
        Date endDate = current.getTime();
            if (!equal) {
                query = m_session.createQuery("SELECT count(e.campaignId),e.campaignId FROM RmAiTrackingCampaignDetails e where e.ownerId != :ownerId and e.creationDate BETWEEN :stDate AND :edDate and e.campaignRunFlag != 1 GROUP BY e.campaignId , e.ownerId ORDER BY  count(e.campaignId) DESC");
            } else {
                query = m_session.createQuery("SELECT count(e.campaignId),e.campaignId FROM RmAiTrackingCampaignDetails e where e.ownerId = :ownerId and e.creationDate BETWEEN :stDate AND :edDate and e.campaignRunFlag != 1 GROUP BY e.campaignId , e.ownerId ORDER BY  count(e.campaignId) DESC");
            }
        
        query.setParameter("stDate", startDate)
                .setParameter("edDate", endDate).setParameter("ownerId", ownerIds);
        List<Object[]> list = query.list();
        return list;
    }
    
    public List<Object[]> getCountUsingHQLForRealTimeUsingEXEName(Integer ownerIds, Date sDate, Date eDate, String exeName, boolean equal) {
        Query query = null;
        Calendar current = Calendar.getInstance();
        current.setTime(sDate);
        Date startDate = current.getTime();
        current.setTime(eDate);
        Date endDate = current.getTime();
            if (!equal) {
                query = m_session.createQuery("SELECT count(e.campaignId),e.campaignId FROM RmAiTrackingCampaignDetails e where e.ownerId != :ownerId and e.creationDate BETWEEN :stDate AND :edDate and e.campaignRunFlag != 1 and e.campaignExecutedId= :campaignExecutedId GROUP BY e.campaignId , e.ownerId ORDER BY  count(e.campaignId) DESC");
            } else {
                query = m_session.createQuery("SELECT count(e.campaignId),e.campaignId FROM RmAiTrackingCampaignDetails e where e.ownerId = :ownerId and e.creationDate BETWEEN :stDate AND :edDate and e.campaignRunFlag != 1 and e.campaignExecutedId= :campaignExecutedId GROUP BY e.campaignId , e.ownerId ORDER BY  count(e.campaignId) DESC");
            }
        
        query.setParameter("stDate", startDate)
                .setParameter("edDate", endDate).setParameter("ownerId", ownerIds)
                .setParameter("campaignExecutedId", exeName);
        List<Object[]> list = query.list();
        return list;
    }
    
    public List<Object[]> getCampaignRunCountUsingHQL(Integer ownerId, Date sDate, Date eDate, boolean equal) {
        Query query = null;
        
            if (!equal) {
                query = m_session.createQuery("SELECT count(e.campaignId),e.campaignId FROM RmAiTrackingCampaignDetails e where e.ownerId != :ownerId and e.campaignRunFlag = :campaignRunFlag and e.creationDate BETWEEN :stDate AND :edDate GROUP BY e.campaignId , e.ownerId ORDER BY  count(e.campaignId) DESC");
            } else {
                query = m_session.createQuery("SELECT count(e.campaignId),e.campaignId FROM RmAiTrackingCampaignDetails e where e.ownerId = :ownerId and e.campaignRunFlag = :campaignRunFlag and e.creationDate BETWEEN :stDate AND :edDate GROUP BY e.campaignId , e.ownerId ORDER BY  count(e.campaignId) DESC");
            }
        
        query.setParameter("stDate", sDate)
                .setParameter("edDate", eDate).setParameter("ownerId", ownerId)
                .setParameter("campaignRunFlag", GlobalStatus.ACTIVE);
        List<Object[]> list = query.list();
        return list;
    }
    
    
    public List<Object[]> getTesdingCountUsingHQL(Integer advertisorIds, Date sDate, Date eDate, boolean equal) {
        Query query = null;
        
            if (!equal) {
                query = m_session.createQuery("SELECT count(e.campaignId),e.campaignId FROM RmAiTrackingCampaignDetails e where e.creationDate BETWEEN :stDate AND :edDate GROUP BY e.campaignId  ORDER BY  count(e.campaignId) DESC");
            } else {
                query = m_session.createQuery("SELECT count(e.campaignId),e.campaignId FROM RmAiTrackingCampaignDetails e where e.creationDate BETWEEN :stDate AND :edDate GROUP BY e.campaignId  ORDER BY  count(e.campaignId) DESC");
            }
      
        query.setParameter("stDate", sDate)
                .setParameter("edDate", eDate);
        List<Object[]> list = query.list();
        return list;
    }
}
