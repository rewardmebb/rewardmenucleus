/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector;

import com.mollatech.rewardme.nucleus.db.RmCountries;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author abhishekingle
 */
public class CountryUtils {
    
    private SessionFactoryUtil m_su;

    private Session m_session;

    static final Logger logger = Logger.getLogger(CountryUtils.class);

    public CountryUtils(SessionFactoryUtil su, Session session) {
        m_su = su;
        m_session = session;
    }
    
    public RmCountries getCountryById(int countryid) {
        RmCountries pdetails;
        try {
            Criteria criteria = m_session.createCriteria(RmCountries.class);
            criteria.add(Restrictions.eq("id", countryid));
            List list = criteria.list();
            if (list.isEmpty() == true) {
                return null;
            } else {
                pdetails = (RmCountries) list.get(0);
                return pdetails;
            }
        } catch (Exception e) {
            logger.error("Exception at CountryUtils ", e);
        }
        return null;
    }
}
