/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector;

import com.mollatech.rewardme.nucleus.commons.GlobalStatus;
import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
import com.mollatech.rewardme.nucleus.db.RmCities;
import com.mollatech.rewardme.nucleus.db.RmCountries;
import com.mollatech.rewardme.nucleus.db.RmStates;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author bluebricks
 */
public class AdressUtils {

    private SessionFactoryUtil m_su;

    private Session m_session;

    static final Logger logger = Logger.getLogger(AdressUtils.class);

    public AdressUtils(SessionFactoryUtil su, Session session) {
        m_su = su;
        m_session = session;
    }

    public RmCountries[] getAllCity() {
        RmCountries[] pObj = null;
        Criteria criteria = m_session.createCriteria(RmCountries.class);
        criteria.add(Restrictions.ne("id", 0));
        List list = criteria.list();
        if (list.isEmpty() != true) {
            try {
                pObj = new RmCountries[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    pObj[i] = (RmCountries) list.get(i);
                }
                return pObj;
            } catch (Exception e) {
                logger.error("Exception at AdressUtils ", e);
                return null;
            }
        }
        return null;
    }

    public RmCities []  getAllCityByStateid(int stateId) {
        
        try {
            Criteria criteria = m_session.createCriteria(RmCities.class);
            criteria.add(Restrictions.eq("stateId", stateId));
            List list = criteria.list();
            RmCities [] pdetails = new RmCities[list.size()];
            if (list.isEmpty() == true) {
                return null;
            } else {
                for(int i = 0; i < list.size(); i++ ){
                pdetails [i] =  (RmCities) list.get(i);
                }
                return pdetails;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception at AdressUtils ", e);
        }
        return null;

    }
    
    public RmStates []  getAllstateByCountry(int countryId) {
        
        try {
            Criteria criteria = m_session.createCriteria(RmStates.class);
            criteria.add(Restrictions.eq("countryId", countryId));
            List list = criteria.list();
            RmStates [] pdetails = new RmStates[list.size()];
            if (list.isEmpty() == true) {
                return null;
            } else {
                for(int i = 0; i < list.size(); i++ ){
                pdetails[i] =  (RmStates) list.get(i);
                }
                return pdetails;
            }
        } catch (Exception e) {
            logger.error("Exception at AdressUtils ", e);
        }
        return null;

    }

}
