/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector;

import com.mollatech.rewardme.nucleus.commons.GlobalStatus;
import com.mollatech.rewardme.nucleus.db.RmUnapprovalpackagedetails;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author abhishekingle
 */
public class UnapprovalPackageUtil {
    
    private SessionFactoryUtil m_su;

    private Session m_session;

    static final Logger logger = Logger.getLogger(UnapprovalPackageUtil.class);

    public UnapprovalPackageUtil(SessionFactoryUtil su, Session session) {
        m_su = su;
        m_session = session;
    }

    public int addUnApprovalPackage(RmUnapprovalpackagedetails bucketObj) {
        Transaction tx = null;
        try {
            tx = m_session.beginTransaction();
            Serializable s = m_session.save(bucketObj);
            tx.commit();
            return (Integer) s;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception at UnapprovalPackageUtil ", e);
            return -1;
        }
    }

    public RmUnapprovalpackagedetails[] listUnapprovalPackage() {
        RmUnapprovalpackagedetails[] pdetails = null;
        try {
            Criteria criteria = m_session.createCriteria(RmUnapprovalpackagedetails.class);
            List list = criteria.list();
            if (list.isEmpty() == true) {
                return null;
            } else {
                pdetails = new RmUnapprovalpackagedetails[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    pdetails[i] = (RmUnapprovalpackagedetails) list.get(i);
                }
                return pdetails;
            }
        } catch (Exception e) {
            logger.error("Exception at UnapprovalPackageUtil ", e);
        }
        return null;
    }

    public RmUnapprovalpackagedetails getUnApprovalPackagebyId( int packageid) {
        RmUnapprovalpackagedetails pdetails;
        try {
            Criteria criteria = m_session.createCriteria(RmUnapprovalpackagedetails.class);            
            criteria.add(Restrictions.eq("pckId", packageid));
            List list = criteria.list();
            if (list.isEmpty() == true) {
                return null;
            } else {
                pdetails = (RmUnapprovalpackagedetails) list.get(0);
                return pdetails;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception at UnapprovalPackageUtil ", e);
        }
        return null;
    }

    public int editUnApprovalPackage(RmUnapprovalpackagedetails details) {
        try {
            Criteria criteria = m_session.createCriteria(RmUnapprovalpackagedetails.class);            
            criteria.add(Restrictions.eq("pckId", details.getPckId()));
            List list = criteria.list();
            RmUnapprovalpackagedetails detailses = null;
            if (list != null && !list.isEmpty()) {
                detailses = (RmUnapprovalpackagedetails) list.get(0);
            } else {
                return -1;
            }
            
            detailses.setPckId(details.getPckId());
            detailses.setUpdationDate(new Date());            
            detailses.setPackageName(details.getPackageName());
            detailses.setPlanAmount(details.getPlanAmount());
            detailses.setPaymentMode(details.getPaymentMode());
            detailses.setPackageDuration(details.getPackageDuration());
            detailses.setStatus(details.getStatus());            
            detailses.setTax(details.getTax());
            detailses.setCreationDate(details.getCreationDate());                   
            detailses.setMainCredits(details.getMainCredits());
            detailses.setRecurrenceBillingPlanId(detailses.getRecurrenceBillingPlanId());
            detailses.setPackageDescription(details.getPackageDescription());
            detailses.setBrandOwnerVisibility(details.getBrandOwnerVisibility());
            detailses.setCreditDeductionConfiguration(details.getCreditDeductionConfiguration());
            detailses.setMakerflag(details.getMakerflag());
            detailses.setReasonForRejection(details.getReasonForRejection());
            
            Transaction tx = m_session.beginTransaction();
            m_session.save(detailses);
            tx.commit();
            return 0;
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Exception at UnapprovalPackageUtil ", ex);
            return -1;
        }
    }

    public int changeUnApprovalRequestStatus(int pid, int status) {
        try {
            Criteria criteria = m_session.createCriteria(RmUnapprovalpackagedetails.class);
            criteria.add(Restrictions.eq("pckId", pid));            
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return -2;
            } else {
                RmUnapprovalpackagedetails rObj = (RmUnapprovalpackagedetails) list.get(0);
                if (status == rObj.getMakerflag()) {
                    return 3;
                }
                Transaction tx = null;
                try {
                    rObj.setMakerflag(status);
                    tx = m_session.beginTransaction();
                    m_session.save(rObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return -1;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception at UnapprovalPackageUtil ", e);
        }
        return -1;
    }

    public int changeUnApprovalPackageStatus(int pid, int status) {
        try {
            Criteria criteria = m_session.createCriteria(RmUnapprovalpackagedetails.class);
            criteria.add(Restrictions.eq("pckId", pid));            
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return -2;
            } else {
                RmUnapprovalpackagedetails rObj = (RmUnapprovalpackagedetails) list.get(0);
                Transaction tx = null;
                try {
                    rObj.setStatus(status);
                    tx = m_session.beginTransaction();
                    m_session.save(rObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return -1;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception at UnapprovalPackageUtil ", e);
        }
        return -1;
    }

    public RmUnapprovalpackagedetails[] listUnApprovalPackageByStatus(int status) {
        try {
            Criteria criteria = m_session.createCriteria(RmUnapprovalpackagedetails.class);
            criteria.add(Restrictions.eq("makerflag", status));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmUnapprovalpackagedetails[] SgBucketRequestList = new RmUnapprovalpackagedetails[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    SgBucketRequestList[i] = (RmUnapprovalpackagedetails) list.get(i);
                }
                return SgBucketRequestList;
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception at UnapprovalPackageUtil ", e);
        }
        return null;
    }

    public RmUnapprovalpackagedetails getUnApprovalPackageByName(String packageName) {
        try {
            Criteria criteria = m_session.createCriteria(RmUnapprovalpackagedetails.class);            
            criteria.add(Restrictions.eq("packageName", packageName));
            criteria.add(Restrictions.ne("status", GlobalStatus.DELETED));
            List list = criteria.list();
            if (list != null && !list.isEmpty()) {
                RmUnapprovalpackagedetails packageObj = (RmUnapprovalpackagedetails) list.get(0);
                return packageObj;
            } else {
                return null;
            }
        } catch (Exception ex) {
            logger.error("Exception at UnapprovalPackageUtil ", ex);
            return null;
        }
    }

    public RmUnapprovalpackagedetails getUnApprovalPackageById(int id) {
        try {
            Criteria criteria = m_session.createCriteria(RmUnapprovalpackagedetails.class);            
            criteria.add(Restrictions.eq("pckId", id));
            criteria.add(Restrictions.ne("status", GlobalStatus.DELETED));
            List list = criteria.list();
            if (list != null && !list.isEmpty()) {
                RmUnapprovalpackagedetails packageObj = (RmUnapprovalpackagedetails) list.get(0);
                return packageObj;
            } else {
                return null;
            }
        } catch (Exception ex) {
            logger.error("Exception at UnapprovalPackageUtil ", ex);
            return null;
        }
    }

    public RmUnapprovalpackagedetails getUnApprovalPackageByDefault() {
        try {
            Criteria criteria = m_session.createCriteria(RmUnapprovalpackagedetails.class);            
            criteria.add(Restrictions.eq("markAsDefault", GlobalStatus.DEFAULT));
            criteria.add(Restrictions.ne("status", GlobalStatus.DELETED));
            List list = criteria.list();
            if (list != null && !list.isEmpty()) {
                RmUnapprovalpackagedetails packageObj = (RmUnapprovalpackagedetails) list.get(0);
                return packageObj;
            } else {
                return null;
            }
        } catch (Exception ex) {
            logger.error("Exception at UnapprovalPackageUtil ", ex);
            return null;
        }
    }

    public int updateUnApprovalDetails(RmUnapprovalpackagedetails bucketObj) {
        Transaction tx = null;
        try {
            tx = m_session.beginTransaction();
            m_session.save(bucketObj);
            tx.commit();
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            tx.rollback();
            logger.error("Exception at UnapprovalPackageUtil ", e);
        }
        return -1;
    }

    public RmUnapprovalpackagedetails unApprovalPackageStatus(int bucketId) {
        RmUnapprovalpackagedetails pdetails = null;
        try {
            Criteria criteria = m_session.createCriteria(RmUnapprovalpackagedetails.class);
            criteria.add(Restrictions.eq("pckId", bucketId));
            List list = criteria.list();
            if (list.isEmpty() == true) {
                return null;
            } else {
                pdetails = (RmUnapprovalpackagedetails) list.get(0);
                return pdetails;
            }
        } catch (Exception e) {
            logger.error("Exception at UnapprovalPackageUtil ", e);
        }
        return null;
    }
}
