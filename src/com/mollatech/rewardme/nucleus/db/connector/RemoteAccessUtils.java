/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector;

import com.mollatech.rewardme.nucleus.db.RmRemoteaccess;
import com.mollatech.service.nucleus.crypto.AxiomProtect;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author abhishekingle
 */
public class RemoteAccessUtils {
    
    private SessionFactoryUtil m_su;

    private Session m_session;

    public RemoteAccessUtils(SessionFactoryUtil su, Session session) {
        m_su = su;
        m_session = session;
    }

    static final Logger logger = Logger.getLogger(RemoteAccessUtils.class);

    public int addRemoteaccess(int remoteaccessId, Boolean authenabled, String loginid, String password, Integer expiryInMins, Date createdOn, Date lastUpdateOn) {
        RmRemoteaccess rObj = new RmRemoteaccess();        
        rObj.setAuthenabled((byte) (authenabled ? 1 : 0));
        rObj.setCreatedOn(createdOn);
        rObj.setExpiryInMins(expiryInMins);
        rObj.setLastUpdateOn(lastUpdateOn);
        rObj.setLoginid(loginid);
        String securePassword = AxiomProtect.ProtectData(password);
        rObj.setPassword(securePassword);
        rObj.setRemoteaccessid(remoteaccessId);
        Transaction tx = null;
        try {
            tx = m_session.beginTransaction();
            m_session.save(rObj);
            tx.commit();
            return 0;
        } catch (Exception e) {
            logger.error("Exception at RemoteAccessUtils ", e);
        }
        return -1;
    }

    public RmRemoteaccess getRemoteaccess() {
        try {
            Criteria criteria = m_session.createCriteria(RmRemoteaccess.class);            
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmRemoteaccess rObj = (RmRemoteaccess) list.get(0);
                String pass = rObj.getPassword();
                rObj.setPassword(AxiomProtect.AccessData(pass));
                return rObj;
            }
        } catch (Exception e) {
            logger.error("Exception at RemoteAccessUtils ", e);
        }
        return null;
    }

    public RmRemoteaccess getRemoteaccess4Channel() {
        try {
            Criteria criteria = m_session.createCriteria(RmRemoteaccess.class);            
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmRemoteaccess rObj = (RmRemoteaccess) list.get(0);
                String pass = rObj.getPassword();
                rObj.setPassword(AxiomProtect.AccessData(pass));
                return rObj;
            }
        } catch (Exception e) {
            logger.error("Exception at RemoteAccessUtils ", e);
        }
        return null;
    }

    public int changeRemoteaccesss(int remoteaccessId, RmRemoteaccess remoteaccess) {
        try {
            RmRemoteaccess rObj = new RmRemoteaccess();
            Criteria criteria = m_session.createCriteria(RmRemoteaccess.class);            
            criteria.add(Restrictions.eq("remoteaccessid", remoteaccessId));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return -2;
            } else if (remoteaccess != null) {
                String securePassword = AxiomProtect.ProtectData(remoteaccess.getPassword());
                remoteaccess.setPassword(securePassword);
                rObj = remoteaccess;
                Transaction tx = null;
                try {
                    tx = m_session.beginTransaction();
                    m_session.save(rObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    logger.error("Exception at RemoteAccessUtils ", e);
                }
            } else {
                return -1;
            }
        } catch (Exception e) {
            logger.error("Exception at RemoteAccessUtils ", e);
        }
        return -2;
    }

    public RmRemoteaccess[] getAllRemoteaccess() {
        try {
            Criteria criteria = m_session.createCriteria(RmRemoteaccess.class);            
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmRemoteaccess[] objectList = new RmRemoteaccess[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    objectList[i] = (RmRemoteaccess) list.get(i);
                    String pass = objectList[i].getPassword();
                    objectList[i].setPassword(AxiomProtect.AccessData(pass));
                }
                return objectList;
            }
        } catch (Exception e) {
            logger.error("Exception at RemoteAccessUtils ", e);
        }
        return null;
    }

    public int updateLastUpdatedOn(int remoteAccessId, Date lastAccessOnDate) {
        try {
            RmRemoteaccess rObj = new RmRemoteaccess();
            Criteria criteria = m_session.createCriteria(RmRemoteaccess.class);
            criteria.add(Restrictions.eq("remoteaccessid", remoteAccessId));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return -2;
            } else if (lastAccessOnDate != null) {
                rObj = (RmRemoteaccess) list.get(0);
                rObj.setLastUpdateOn(lastAccessOnDate);
                Transaction tx = null;
                try {
                    tx = m_session.beginTransaction();
                    m_session.save(rObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    logger.error("Exception at RemoteAccessUtils ", e);
                }
            } else {
                return -1;
            }
        } catch (Exception e) {
            logger.error("Exception at RemoteAccessUtils ", e);
        }
        return -2;
    }

    public int enableRemoteAccess(boolean bEnabled) {
        try {
            RmRemoteaccess rObj = new RmRemoteaccess();
            Criteria criteria = m_session.createCriteria(RmRemoteaccess.class);            
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return -1;
            } else {
                rObj = (RmRemoteaccess) list.get(0);
                rObj.setAuthenabled((byte) (bEnabled ? 1 : 0));
                Date dd = new Date();
                rObj.setLastUpdateOn(dd);
                Transaction tx = null;
                try {
                    tx = m_session.beginTransaction();
                    m_session.save(rObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    logger.error("Exception at RemoteAccessUtils ", e);
                }
            }
        } catch (Exception e) {
            logger.error("Exception at RemoteAccessUtils ", e);
        }
        return -2;
    }

    public int getCount() {
        try {
            Criteria criteria = m_session.createCriteria(RmRemoteaccess.class);
            Integer totalResult = ((Number) criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
            return totalResult;
        } catch (Exception e) {
            logger.error("Exception at RemoteAccessUtils ", e);
            return -2;
        }
    }

    public RmRemoteaccess[] getRemoteAccess() {
        try {
            RmRemoteaccess[] rObj = null;
            Criteria criteria = m_session.createCriteria(RmRemoteaccess.class);
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                rObj = new RmRemoteaccess[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    rObj[i] = (RmRemoteaccess) list.get(i);
                }
                return rObj;
            }
        } catch (Exception e) {
            logger.error("Exception at RemoteAccessUtils ", e);
        }
        return null;
    }

    public int SetRemoteAccessCredentials(String login, String password) {
        try {
            Date date = new Date();
            RmRemoteaccess rObj = new RmRemoteaccess();
            Criteria criteria = m_session.createCriteria(RmRemoteaccess.class);            
            criteria.add(Restrictions.eq("loginid", login));
            String securePassword = AxiomProtect.ProtectData(password);
            criteria.add(Restrictions.eq("password", securePassword));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                Date d = new Date();
                Long lRemoteaccessId = d.getTime();
                int iRemoteaccessId = lRemoteaccessId.intValue();
                if (iRemoteaccessId < 0) {
                    iRemoteaccessId = iRemoteaccessId * (-1);
                }
                int iRetVal = addRemoteaccess(iRemoteaccessId, Boolean.FALSE, login, password, 15, d, d);
                if (iRetVal == 0) {
                    return 0;
                }
            } else {
                rObj = (RmRemoteaccess) list.get(0);
                boolean b = true;
                rObj.setAuthenabled((byte) (b ? 1 : 0));
                rObj.setLastUpdateOn(date);
                Transaction tx = null;
                try {
                    tx = m_session.beginTransaction();
                    m_session.save(rObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    logger.error("Exception at RemoteAccessUtils ", e);
                }
            }
        } catch (Exception e) {
            logger.error("Exception at RemoteAccessUtils ", e);
        }
        return -1;
    }

    public String[] GetRemoteAccessCredentials() {
        try {
            Criteria criteria = m_session.createCriteria(RmRemoteaccess.class);            
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmRemoteaccess rObj = (RmRemoteaccess) list.get(0);
                String[] arr = new String[3];
                arr[0] = rObj.getLoginid();
                String pass = rObj.getPassword();
                arr[1] = AxiomProtect.AccessData(pass);
                arr[2] = String.valueOf(rObj.getExpiryInMins().intValue());
                return arr;
            }
        } catch (Exception e) {
            logger.error("Exception at RemoteAccessUtils ", e);
        }
        return null;
    }

    public boolean IsRemoteAccessEnabled() {
        try {
            Criteria criteria = m_session.createCriteria(RmRemoteaccess.class);            
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return false;
            } else {
                RmRemoteaccess rObj = (RmRemoteaccess) list.get(0);
                Byte b = rObj.getAuthenabled();
                return b != 0;
            }
        } catch (Exception e) {
            logger.error("Exception at RemoteAccessUtils ", e);
        }
        return false;
    }
}
