/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.connector;

import com.mollatech.rewardme.nucleus.commons.GlobalStatus;
import com.mollatech.rewardme.nucleus.db.RmBrandownerdetails;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author abhishekingle
 */
public class BrandOwnerUtils {
    
    private SessionFactoryUtil m_su;

    private Session m_session;

    static final Logger logger = Logger.getLogger(BrandOwnerUtils.class);

    public BrandOwnerUtils(SessionFactoryUtil su, Session session) {
        m_su = su;
        m_session = session;
    }

    public int addBrandOwnerDetails(RmBrandownerdetails brandOwnerObj) {
        Transaction tx = null;
        try {
            tx = m_session.beginTransaction();
            Serializable s = m_session.save(brandOwnerObj);
            tx.commit();
            return (Integer) s;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Exception at BrandOwnerUtils ", e);
            return -1;
        }
    }
    
    public int checkDuplicate(String name, int type) {
        int result = 0;
        RmBrandownerdetails gdetails;
        try {
            Criteria criteria = m_session.createCriteria(RmBrandownerdetails.class);
            List list = null;
            if (type == 1) {                
                criteria.add(Restrictions.eq("brandName", name));
                criteria.add(Restrictions.ne("status", GlobalStatus.DELETED));
                list = criteria.list();
                if (list != null && !list.isEmpty()) {
                    return -1;
                }
            }
            if (type == 2) {                
                criteria.add(Restrictions.eq("email", name));
                criteria.add(Restrictions.ne("status", GlobalStatus.DELETED));
                list = criteria.list();
                if (list != null && !list.isEmpty()) {
                    return -2;
                }
            }
            if (type == 3) {                
                criteria.add(Restrictions.eq("phone", name));
                criteria.add(Restrictions.ne("status", GlobalStatus.DELETED));
                list = criteria.list();
                if (list != null && !list.isEmpty()) {
                    return -3;
                }
            }
        } catch (Exception e) {
            logger.error("Exception at BrandOwnerUtils ", e);
        }
        return result;
    }
    
    public RmBrandownerdetails getBrandOwnerbyId(int ownerid) {
        RmBrandownerdetails pdetails;
        try {
            Criteria criteria = m_session.createCriteria(RmBrandownerdetails.class);
            criteria.add(Restrictions.eq("ownerId", ownerid));
            List list = criteria.list();
            if (list.isEmpty() == true) {
                return null;
            } else {
                pdetails = (RmBrandownerdetails) list.get(0);
                return pdetails;
            }
        } catch (Exception e) {
            logger.error("Exception at BrandOwnerUtils ", e);
        }
        return null;
    }
    
    public int updateDetails(RmBrandownerdetails brandOwner) {
        try {
            Transaction tx = m_session.beginTransaction();
            m_session.update(brandOwner);
            tx.commit();
            return 0;
        } catch (Exception ex) {
            logger.error("Exception at BrandOwnerUtils ", ex);
            return -1;
        }
    }
    
    public RmBrandownerdetails VerifyCredential(String username, String password) {
        try {
            Criteria criteria = m_session.createCriteria(RmBrandownerdetails.class);
            criteria.add(Restrictions.eq("email", username));            
            criteria.add(Restrictions.eq("password", password));
            criteria.add(Restrictions.ne("status", GlobalStatus.DELETED));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmBrandownerdetails rObj = (RmBrandownerdetails) list.get(0);
                return rObj;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public RmBrandownerdetails[] getAllBrandOwnerDetails() {
        RmBrandownerdetails[] pObj = null;
        Criteria criteria = m_session.createCriteria(RmBrandownerdetails.class);        
        criteria.add(Restrictions.ne("status", GlobalStatus.DELETED));
        List list = criteria.list();
        if (list.isEmpty() != true) {
            try {
                pObj = new RmBrandownerdetails[list.size()];
                for (int i = 0; i < list.size(); i++) {
                    pObj[i] = (RmBrandownerdetails) list.get(i);
                }
                return pObj;
            } catch (Exception e) {
                logger.error("Exception at BrandOwnerUtils ", e);
                return null;
            }
        }
        return null;
    }
    
    public int ChangeBrandStatus(int _brandId, int status) {
        try {
            RmBrandownerdetails pdetails = null;
            Criteria criteria = m_session.createCriteria(RmBrandownerdetails.class);
            criteria.add(Restrictions.eq("ownerId", _brandId));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return -2;
            } else {
                pdetails = (RmBrandownerdetails) list.get(0);
                pdetails.setStatus(status);
                pdetails.setUpdationDate(new Date());
                Transaction tx = null;
                try {
                    tx = m_session.beginTransaction();
                    m_session.update(pdetails);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    logger.error("Exception at BrandOwnerUtils ", e);
                }
            }
        } catch (Exception e) {
            logger.error("Exception at BrandOwnerUtils ", e);
        }
        return -1;
    }
    
    public int changePassword(String userid, String password) {
        int result = Integer.parseInt(userid);
        try {
            Criteria criteria = m_session.createCriteria(RmBrandownerdetails.class);
            criteria.add(Restrictions.eq("ownerId", result));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return -1;
            } else {
                RmBrandownerdetails rObj = (RmBrandownerdetails) list.get(0);
                Transaction tx = null;
                try {
                    rObj.setPassword(password);
                    rObj.setUpdationDate(new Date());
                    tx = m_session.beginTransaction();
                    m_session.save(rObj);
                    tx.commit();
                    return 0;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }
    
    public RmBrandownerdetails initializeResetPassword(String name, String email, Date expiryTime) {
        try {
            Criteria criteria = m_session.createCriteria(RmBrandownerdetails.class);
            if (name != null) {
                criteria.add(Restrictions.eq("username", name));
            }
            criteria.add(Restrictions.eq("email", email));
            criteria.add(Restrictions.eq("status", GlobalStatus.ACTIVE));
            criteria.add(Restrictions.ne("status", GlobalStatus.DELETED));
            List list = criteria.list();
            if (list == null || list.isEmpty() == true) {
                return null;
            } else {
                RmBrandownerdetails oObj1 = (RmBrandownerdetails) list.get(0);
                if (expiryTime != null) {
                    oObj1.setLinkExpiry(expiryTime);
                    Transaction tx = null;
                    try {
                        tx = m_session.beginTransaction();
                        m_session.save(oObj1);
                        tx.commit();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return oObj1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public RmBrandownerdetails getOwnerbyCustomerId(String customerId) {
        RmBrandownerdetails pdetails;
        try {
            Criteria criteria = m_session.createCriteria(RmBrandownerdetails.class);
            criteria.add(Restrictions.eq("stripeCustomerId", customerId));
            List list = criteria.list();
            if (list.isEmpty() == true) {
                return null;
            } else {
                pdetails = (RmBrandownerdetails) list.get(0);
                return pdetails;
            }
        } catch (Exception e) {
            logger.error("Exception at PartnerUtils ", e);
        }
        return null;
    }
}
