/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.rewardme.nucleus.db.operation;

/**
 *
 * @author abhishekingle
 */
public class REWARDMEOperator {
    
    @Override
    public String toString() {
        return "RewardmeOperartor{" + "strOperatorid=" + strOperatorid + ", strName=" + strName + ", strPhone=" + strPhone + ", strEmail=" + strEmail + ", strRoleName=" + strRoleName + ", strRoleid=" + strRoleid + ", iStatus=" + iStatus + ", iWrongAttempts=" + iWrongAttempts + ", utcCreatedOn=" + utcCreatedOn + ", lastUpdateOn=" + lastUpdateOn + '}';
    }

    

    public String getStrOperatorid() {
        return strOperatorid;
    }

    public void setStrOperatorid(String strOperatorid) {
        this.strOperatorid = strOperatorid;
    }

    public String getStrName() {
        return strName;
    }

    public void setStrName(String strName) {
        this.strName = strName;
    }

    public String getStrPhone() {
        return strPhone;
    }

    public void setStrPhone(String strPhone) {
        this.strPhone = strPhone;
    }

    public String getStrEmail() {
        return strEmail;
    }

    public void setStrEmail(String strEmail) {
        this.strEmail = strEmail;
    }

    public String getStrRoleName() {
        return strRoleName;
    }

    public void setStrRoleName(String strRoleName) {
        this.strRoleName = strRoleName;
    }

    public String getStrRoleid() {
        return strRoleid;
    }

    public void setStrRoleid(String strRoleid) {
        this.strRoleid = strRoleid;
    }

    public int getiStatus() {
        return iStatus;
    }

    public void setiStatus(int iStatus) {
        this.iStatus = iStatus;
    }

    public int getiWrongAttempts() {
        return iWrongAttempts;
    }

    public void setiWrongAttempts(int iWrongAttempts) {
        this.iWrongAttempts = iWrongAttempts;
    }

    public long getUtcCreatedOn() {
        return utcCreatedOn;
    }

    public void setUtcCreatedOn(long utcCreatedOn) {
        this.utcCreatedOn = utcCreatedOn;
    }

    public long getLastUpdateOn() {
        return lastUpdateOn;
    }

    public void setLastUpdateOn(long lastUpdateOn) {
        this.lastUpdateOn = lastUpdateOn;
    }

    public REWARDMEOperator() {
    }

    public REWARDMEOperator(String strOperatorid, String strName, String strPhone, String strEmail, String strRoleName, String strRoleid, int iStatus, int iWrongAttempts, long utcCreatedOn, long lastUpdateOn) {        
        this.strOperatorid = strOperatorid;
        this.strName = strName;
        this.strPhone = strPhone;
        this.strEmail = strEmail;
        this.strRoleName = strRoleName;
        this.strRoleid = strRoleid;
        this.iStatus = iStatus;
        this.iWrongAttempts = iWrongAttempts;
        this.utcCreatedOn = utcCreatedOn;
        this.lastUpdateOn = lastUpdateOn;
    }
    

    public String strOperatorid;

    public String strName;

    public String strPhone;

    public String strEmail;

    public String strRoleName;

    public String strRoleid;

    public int iStatus;

    public int iWrongAttempts;

    public long utcCreatedOn;

    public long lastUpdateOn;
}
