package com.mollatech.rewardme.nucleus.db;
// Generated Jan 11, 2018 6:53:19 PM by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * RmOperators generated by hbm2java
 */
public class RmOperators  implements java.io.Serializable {


     private String operatorid;
     private String name;
     private String phone;
     private String emailid;
     private String passsword;
     private int roleid;
     private int status;
     private int currentAttempts;
     private Date createdOn;
     private Date lastAccessOn;
     private Date passwordupdatedOn;
     private Integer changePassword;
     private Integer loginflag;
     private String randomString;
     private Date linkExpiry;
     private Integer resourceRole;

    public RmOperators() {
    }

	
    public RmOperators(String operatorid, String passsword, int roleid, int status, int currentAttempts) {
        this.operatorid = operatorid;
        this.passsword = passsword;
        this.roleid = roleid;
        this.status = status;
        this.currentAttempts = currentAttempts;
    }
    public RmOperators(String operatorid, String name, String phone, String emailid, String passsword, int roleid, int status, int currentAttempts, Date createdOn, Date lastAccessOn, Date passwordupdatedOn, Integer changePassword, Integer loginflag, String randomString, Date linkExpiry, Integer resourceRole) {
       this.operatorid = operatorid;
       this.name = name;
       this.phone = phone;
       this.emailid = emailid;
       this.passsword = passsword;
       this.roleid = roleid;
       this.status = status;
       this.currentAttempts = currentAttempts;
       this.createdOn = createdOn;
       this.lastAccessOn = lastAccessOn;
       this.passwordupdatedOn = passwordupdatedOn;
       this.changePassword = changePassword;
       this.loginflag = loginflag;
       this.randomString = randomString;
       this.linkExpiry = linkExpiry;
       this.resourceRole = resourceRole;
    }
   
    public String getOperatorid() {
        return this.operatorid;
    }
    
    public void setOperatorid(String operatorid) {
        this.operatorid = operatorid;
    }
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    public String getPhone() {
        return this.phone;
    }
    
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getEmailid() {
        return this.emailid;
    }
    
    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }
    public String getPasssword() {
        return this.passsword;
    }
    
    public void setPasssword(String passsword) {
        this.passsword = passsword;
    }
    public int getRoleid() {
        return this.roleid;
    }
    
    public void setRoleid(int roleid) {
        this.roleid = roleid;
    }
    public int getStatus() {
        return this.status;
    }
    
    public void setStatus(int status) {
        this.status = status;
    }
    public int getCurrentAttempts() {
        return this.currentAttempts;
    }
    
    public void setCurrentAttempts(int currentAttempts) {
        this.currentAttempts = currentAttempts;
    }
    public Date getCreatedOn() {
        return this.createdOn;
    }
    
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }
    public Date getLastAccessOn() {
        return this.lastAccessOn;
    }
    
    public void setLastAccessOn(Date lastAccessOn) {
        this.lastAccessOn = lastAccessOn;
    }
    public Date getPasswordupdatedOn() {
        return this.passwordupdatedOn;
    }
    
    public void setPasswordupdatedOn(Date passwordupdatedOn) {
        this.passwordupdatedOn = passwordupdatedOn;
    }
    public Integer getChangePassword() {
        return this.changePassword;
    }
    
    public void setChangePassword(Integer changePassword) {
        this.changePassword = changePassword;
    }
    public Integer getLoginflag() {
        return this.loginflag;
    }
    
    public void setLoginflag(Integer loginflag) {
        this.loginflag = loginflag;
    }
    public String getRandomString() {
        return this.randomString;
    }
    
    public void setRandomString(String randomString) {
        this.randomString = randomString;
    }
    public Date getLinkExpiry() {
        return this.linkExpiry;
    }
    
    public void setLinkExpiry(Date linkExpiry) {
        this.linkExpiry = linkExpiry;
    }
    public Integer getResourceRole() {
        return this.resourceRole;
    }
    
    public void setResourceRole(Integer resourceRole) {
        this.resourceRole = resourceRole;
    }




}


