package com.mollatech.rewardme.nucleus.db;
// Generated Jan 11, 2018 6:53:19 PM by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * RmRoles generated by hbm2java
 */
public class RmRoles  implements java.io.Serializable {


     private Integer roleid;
     private String name;
     private Date createdOn;
     private Date lastAccessOn;

    public RmRoles() {
    }

	
    public RmRoles(String name) {
        this.name = name;
    }
    public RmRoles(String name, Date createdOn, Date lastAccessOn) {
       this.name = name;
       this.createdOn = createdOn;
       this.lastAccessOn = lastAccessOn;
    }
   
    public Integer getRoleid() {
        return this.roleid;
    }
    
    public void setRoleid(Integer roleid) {
        this.roleid = roleid;
    }
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    public Date getCreatedOn() {
        return this.createdOn;
    }
    
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }
    public Date getLastAccessOn() {
        return this.lastAccessOn;
    }
    
    public void setLastAccessOn(Date lastAccessOn) {
        this.lastAccessOn = lastAccessOn;
    }




}


